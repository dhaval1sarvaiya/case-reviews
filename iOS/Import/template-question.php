<?php
	require_once('template-functions.php');
	head('question');
?>
	<script>
		var correctAnswers = <?php echo json_encode($answers);?>;
	</script>
	<div class='container'>
		<div class='resultCorrect'>Correct</div>
		<div class='resultIncorrect'>Incorrect</div>
		<div class='diagnosis'>
			<h5>Diagnosis:</h5>
			<h1 class='diagnosisText'><?php echo flay($diagnosis);?></h1>
		</div>
		<div class='question'>
			<div><?php echo flay($question);?></div>
		</div>
		<div class='answers'>
		<?php foreach ($dds as $i=>$dd) {?>
			<div class='differentialDiagnosis'><?php echo $dd['answer']; ?></div>
			<div class='differentialDiagnosisRationale <?php if (array_search($i,$answers) === FALSE) {echo "false";} ?>'><?php echo empty($dd['rationale'])?"&nbsp;":$dd['rationale']; ?></div>
		<?php } ?>
		</div>
	</div>
	<div class='submit'>Submit answers</div>
<?php 
	foot('question');
