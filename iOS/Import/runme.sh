#!/bin/bash
PROJECT=`pwd | sed -e 's#^.*/Books/\([^/]*\)/Import#\1#'`
export GDOC CASES_GID IMAGES_GID CONTENTS_GID PROJECT;
cd ../../../Import/
if [ "$?" != "0" -o "$PROJECT" == "" ]; then
	echo "ERROR: You must call this script from the Books/*/Import/ directory.";
	exit 1;
else
	php import-questions.php $PROJECT
fi;
cd ../Books/$PROJECT/
mkdir Zipme
find Yellow -type f -exec cp \{\} Zipme \;
cp -r Blue/* Zipme
cd Zipme
rm ../app.zip
zip ../app -r *
cd ..
rm -Rf Zipme
