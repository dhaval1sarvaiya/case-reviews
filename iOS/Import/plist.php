<?php
function xmlentities($string) {
	return str_replace(array ('&', '"', "'", '<', '>'), array('&amp;', '&quot;', '&apos;', '&lt;', '&gt;'), $string);
}
function is_assoc($array) {
    return (is_array($array) && 0 !== count(array_diff_key($array, array_keys(array_keys($array)))));
}
function is_plainarray($array) {
	return is_array($array) && !is_assoc($array);
}
function plist($obj,$inner = false) {
	$ret = "";
	//var_dump($obj);
	if (!$inner) {
		$ret .= "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">\n<plist version=\"1.0\">\n";
	}
	if ($obj === NULL) {
		$obj = "";
	}
	if (is_plainarray($obj)) {
		$ret .= "<array>\n";
		foreach ($obj as $o) {
			$ret .= plist($o,true)."";
		}
		$ret .= "</array>\n";
	} else if (is_assoc($obj)) {
		$ret .= "<dict>\n";
		foreach ($obj as $k=>$o) {
			$ret .= "<key>".xmlentities($k)."</key>".plist($o,true)."";
		}
		$ret .= "</dict>\n";
	} else if (is_numeric($obj)) {
		$ret .= "<real>".floatval($obj)."</real>\n";
	} else if (is_string($obj)) {
		$ret .= "<string>".xmlentities($obj)."</string>\n";
	} else {
		var_dump($obj);
		die("UNHANDLED VALUE '$obj'\n");
	}
	if (!$inner) {
		$ret .= "</plist>\n";
	}
	return $ret;
}
