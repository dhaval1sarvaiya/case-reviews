<?php
function head(){
	?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link rel="stylesheet" type="text/css" href="style.css"> 
		<title>Question</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=no" />
    <script src="js/question.js"></script>
	</head>
	<body>
	<?php
}
function foot() {
	?>
	</body>
</html>
	<?php
}
	function flay($msg,$class="()*£$)(*£$)%(*$)%(*£)*($)%*)77873907)") {
		$lines = explode("\n",$msg);
		$inUl = false;
		foreach ($lines as $i=>&$line) {
			$line = htmlentities($line,ENT_COMPAT,"UTF-8");
			$line = preg_replace("/\^\{([^\}]+)\}/","<sup>\$1</sup>",$line);
			$line = preg_replace("/\_\{([^\}]+)\}/","<sub>\$1</sub>",$line);
			$line = preg_replace("/i\{([^\}]+)\}/","<em>\$1</em>",$line);
			$line = preg_replace("/b\{([^\}]+)\}/","<strong>\$1</strong>",$line);
			$line = preg_replace("/u\{([^\}]+)\}/","<u>\$1</u>",$line);
			if ($i == 0) {
				$line = preg_replace("/(".preg_quote($class).")/"," <strong>\$1</strong> ",$line);
			}
			if (substr($line,0,1) == "-") {
				$line = substr($line,1);
				$line = ($inUl?"":"<ul>")."<li>$line</li>";
				$inUl = true;
			} else {
				$line = ($inUl?"</ul>":"")."<p>$line</p>";
				$inUl = false;
			}
		}
		return implode("\n",$lines).($inUl?"</ul>":"")."\n"	;
	}
	function youtube($id) {
		return "<iframe width=\"320\" height=\"199\" src=\"http://www.youtube.com/embed/$id\" frameborder=\"0\" allowfullscreen></iframe>";
	}
	function img($link) {
		if (substr($link,0,1) == "<") {
			//Youtube embed
			//<iframe width="560" height="349" src="http://www.youtube.com/embed/JdCoXUcCT6Y" frameborder="0" allowfullscreen></iframe>
			if (preg_match("/<iframe width=\"[0-9]+\" height=\"[0-9]+\" src=\"http://www.youtube.com/embed/([A-Za-z0-9]+)\" frameborder=\"0\" allowfullscreen><\/iframe>/",$link,$matches)) {
				$id = $matches[1];
				return youtube($id);
			}
		} else if (substr($link,0,7) == "http://") {
			//Youtube link
			// http://www.youtube.com/watch?v=JdCoXUcCT6Y
			// http://youtu.be/JdCoXUcCT6Y
			if (preg_match("/^http:\/\/(?:www.)?(?:youtube.com\/watch\?v=|youtu.be\/)([A-Za-z0-9]+)$/",$link,$matches)) {
				$id = $matches[1];
				return youtube($id);
			}
		} else {
			return "<img src=\"images/$link\" />";
		}
		return "UNKNOWN";
	}

