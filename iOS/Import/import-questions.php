<?php
/*
exec("thor xcs:help",$o,$ret);
if ($ret != 0) {
	echo "
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
ERROR: You need to install stuff...

Copy this command into terminal (you will need to enter your password):
	
sudo gem install thor && sudo gem install rb-appscript && thor install https://github.com/gonzoua/xcs/raw/master/xcs.thor






";
	
	
	exit(1);
}
*/
$PROJECT = $_SERVER['argv'][1];
if (empty($PROJECT)) {
	die("ERROR: Project not known!!\n");
}
chdir(dirname(__FILE__));
require_once("gdocs.php");
if (!file_exists('tmp')) {
	mkdir('tmp');
}

$envs = array("GDOC","CASES_GID","IMAGES_GID","CONTENTS_GID");
foreach ($envs as $env) {
	$$env = $_SERVER[$env];
}

$commonSrc = $_SERVER['HOME']."/Dropbox/Antbits-BB/CaseReviews/Common/";
$othersrc = dirname(dirname(__FILE__))."/Books/$PROJECT/";
$src = $_SERVER['HOME']."/Dropbox/Antbits-BB/CaseReviews/$PROJECT/";
$dest = $othersrc;//dirname(dirname(__FILE__))."/current/";
$destBlue = $dest."Blue/";
$destYellow = $dest."Yellow/";


if (!file_exists($destYellow)) {
	mkdir($destYellow);
}
if (!file_exists($destBlue)) {
	mkdir($destBlue);
}

echo "Copying files";

/*if (file_exists("../current")) {
	`rm -Rf ../current`;
}
mkdir("../current/");
*/



//rcopy($commonSrc."UIImages/",$destYellow."UIImages/");
echo ".";
if (!file_exists($destBlue."html/")) {
	mkdir($destBlue."html/");
}
if (!file_exists($destBlue."Plists/")) {
	mkdir($destBlue."Plists/");
}
rcopy($commonSrc."html/",$destBlue."html/",$src."html/");
echo ".";
if (file_exists($src."html/")) {
	rcopy($src."html/",$destBlue."html/");
}
rcopy($src."UIImages/",$destYellow."UIImages/");
echo ".";
rcopy($src."Images/",$destBlue."Images/");
echo ".";
rcopy($src."Thumbs/",$destBlue."Thumbs/");
echo ".";
if (!file_exists($destBlue."Plists/Info.plist")) {
	echo "WARNING: Importing default Info.plist - EDIT ME!\n";
	copy($commonSrc."Info.plist",$destBlue."Plists/Info.plist");
}
echo ".";
echo " done\n";

$gids = array($CASES_GID,$IMAGES_GID,$CONTENTS_GID);
$cols = array("case","diagnosis","question","answers");

if (true) {
	echo "Downloading documents...";
	GetGDocsCSVMulti(array(
		array($GDOC,$gids[0],"tmp/cases.csv"),
		array($GDOC,$gids[1],"tmp/images.csv"),
		array($GDOC,$gids[2],"tmp/contents.csv")
	));
	//file_put_contents("tmp/cases.csv",GetGDocsCSV($GDOC,$gids[0]));
	//echo ".";
	//file_put_contents("tmp/images.csv",GetGDocsCSV($GDOC,$gids[1]));
	//echo ".";
	//file_put_contents("tmp/contents.csv",GetGDocsCSV($GDOC,$gids[2]));
	echo " done\n";
}
echo "Converting... ";

$index = array();

function index($words,$case) {
	global $index;
	$tmp = strtolower($words);
	$tmp = preg_replace("/[^a-z0-9]/"," ",$tmp);
	$tmp = preg_replace("/ +/"," ",$tmp);
	$tmp = trim($tmp);
	$tmp = explode(" ",$tmp);
	foreach ($tmp as $word) {
		if (empty($index[$word])) {
			$index[$word] = array();
		}
		if (array_search("$case",$index["$word"]) === FALSE) {
			$index["$word"][] = "$case";
		}
	}
}

$images = array();
$f = fopen("tmp/images.csv","r");
$r = -1;
while ($arr = fgetcsv($f)) {
	$r++;
	if ($r == 0) continue;
	if (empty($images[$arr[0]])) $images[$arr[0]] = array();
	$legend = $arr[2];
	$images[$arr[0]][] = array("image"=>$arr[1],"legend"=>$legend);
	index($legend,$arr[0]);
}
fclose($f);
foreach ($images as $case=>$imageList) {
	if (!is_array($imageList)) {
		print_r($imageList);
		die("\nERROR: Expected an array ('$case').\n");
	}
	foreach ($imageList as $imageDetails) {
		$image = $imageDetails['image'];
		if (!$image) {
			print_r($imageDetails);
			die("\nInvalid entry!\n");	
		}
		$base = "$case-$image";
		$files = array("$base.jpg","$base.label.png");
		foreach ($files as $i=>$file) {
			if (!file_exists($src."Images/$base.jpg")) {
				echo "\nWARNING: Referenced file '$file' doesn't exist\n";
			}
		}
		if (!file_exists($src."Thumbs/$base.jpg")) {
			echo "\nWARNING: Referenced file '$file' doesn't have a thumbnail\n";
		}
	}
}

$f = fopen("tmp/cases.csv","r");
$cases = array();
$caseNr = 0;
$r = -1;
while ($arr = fgetcsv($f)) {
	
	if (empty($arr[1]) || strtolower($arr[0]) == "case") continue;
	$r++;
	if (!empty($arr[0])) {
		$caseNr = intval($arr[0]);
	}
	$new = false;
	if (!empty($cases[$caseNr])) {
		die("\nERROR: Same case number ($caseNr) twice!\n");
		$case = $cases[$caseNr];
	} else {
		$new = true;
		if (empty($images[$caseNr])) {
			die("\nERROR: No images defined for case ($caseNr)!\n");
		}
		$c = count($images[$caseNr]);
		if ($c <= 0) {
			echo "\nWARNING: Case $caseNr has 0 images!\n";
		}
		$case = array("images"=>$images[$caseNr],"count"=>$c);
		foreach ($cols as $c=>$col) {
			$ok = true;
			if (count($arr)<=$c) {
				break;
				echo "\nWARNING: Incomplete data for case $caseNr\n";
				$ok = false;
			}
			if ($col == "answers") {
				$case[$col] = $ok?array_map('intval',explode(",",$arr[$c])):array();
			} else {
				$case[$col] = $ok?$arr[$c]:"";
			}
		}
		$dd = array_slice($arr,count($cols));
		$dds = array();
		for ($i = 0, $l = ceil(count($dd)/2); $i<$l; $i++) {
			if (empty($dd[$i*2])) {
				break;
			}
			$ans = array("answer"=>$dd[$i*2]);
			if (!empty($dd[$i*2+1])) $ans["rationale"]=$dd[$i*2+1];
			$dds[$i+1] = $ans;
		}
		$case['dds'] = $dds;
	}
	$cases[$caseNr] = $case;
	index($case['diagnosis'],$caseNr);
	index($case['question'],$caseNr);
}
ksort($cases,SORT_NUMERIC);
fclose($f);

function render($template,$data,$file) {
	global $PROJECT,$destBlue;
	ob_start();
	extract($data);
	include($template);
	$html = ob_get_clean();
	file_put_contents($destBlue."html/$file",$html);
}
function walk($dir) {
	if (!file_exists($dir)) {
		echo "\nWARNING: $dir doesn't exist!\n";
		walk(dirname($dir));
		return false;
	}
	return true;
}
foreach ($cases as $case) {
	if (!empty($case['answers'])) {
		render("template-question.php",$case,"question-{$case['case']}.html");
	}
}
file_put_contents($destBlue."Plists/cases.plist",plist($cases));


$f = fopen("tmp/contents.csv","r");
$contents = array();
$cols = array("image","title","type","data");
$caseNr = 0;
$r = -1;

$files = array("Default.png","Default@2x.png","Default-Portrait~ipad.png","Default-Landscape~ipad.png","icon.png");
foreach ($files as $file) {
	if (file_exists($src."UIImages/".$file)) {
		/*if (!copy($src.$file,$dest.$file)) {
			echo "FAILED copying '{$src}{$file}' to '{$dest}{$file}'\n";
		}*/
	} else {
		echo "\nWARNING: Highly recommended file '$file' not found!\n";
	}
}
function rcopy($src,$dest,$altSrc=NULL) {
	if (!walk($src)) return;
	if (!file_exists($dest)) {
		if (!mkdir($dest)) {
			die("\nERROR: Could not create '".$dest."'!");
		}
	}
	if (!walk($dest)) return;
	if ($handle = opendir($src)) {
	    /* This is the correct way to loop over the directory. */
	    while (false !== ($file = readdir($handle))) {
	    	$ext = substr($file,-4);
	    	if (substr($file,0,1) == '.') {
	    		continue;
	    	}
	    	if (($ext != '.png' && $ext != '.jpg' && $ext != 'work' && $ext != '.css' && substr($ext,1) != '.js' && $ext != 'html') || is_dir($src.$file)) { /*iTunesArtwork*/
	    		if (is_dir($src.$file)) {
	    			rcopy($src.$file."/",$dest.$file."/",($altSrc?$altSrc.$file."/":NULL));
	    		} else {
	    			echo "\nWARNING: Skipping file '$file'\n";
	    		}
	    		continue;
	    	}
	    	if (!$altSrc || !file_exists($altSrc.$file)) {
				if (!file_exists($dest.$file) || (abs(filemtime($dest.$file)-filemtime($src.$file)) > 2)) {
					if (!copy($src.$file,$dest.$file)) {
						echo "\nFAILED copying '{$src}{$file}' to '{$dest}{$file}'\n";
					}
					touch($dest.$file,filemtime($src.$file));
					echo "_";
				} else {
				}
			}
	    }
	    closedir($handle);
	}
}
while ($arr = fgetcsv($f)) {
	$r++;
	if ($r == 0) continue;
	if (empty($arr[1])) break;
	$content = array();
	foreach ($cols as $i => $col) {
		$content[$col] = !empty($arr[$i])?$arr[$i]:"";
	}
	if (!empty($content['image'])) {
		//See if we can copy the file from Dropbox
		$path = $src."UIImages/".$content['image'];
		if (file_exists($path)) {
			//copy($path,$dest."UIImages/".$content['image']);
		} else {
			$path = $commonSrc."UIImages/".$content['image'];
			if (file_exists($path)) {
				//copy($path,$dest."UIImages/".$content['image']);
			} else {
				echo "\nWARNING: '$path' could not be found\n";
			}
		}
	}
	$contents[] = $content;
}
file_put_contents($destBlue."Plists/contents.plist",plist($contents));


ksort($index);
file_put_contents($destBlue."Plists/search.plist",plist($index));
echo " done\n";
/*
if (substr($othersrc,0,-1) != substr($destBlue,0,strlen($othersrc)-1)) {
	copy("{$othersrc}Plists/Info.plist", "$destBlue/Plists/Info.plist");
}
*/
