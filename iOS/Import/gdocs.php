<?php
require("plist.php");
function GetGDocsAuth() {
	static $auth = null;
	if ($auth === null) {
		if (!empty($_SERVER['GOOGLE_AUTH'])) {
			$auth = $_SERVER['GOOGLE_AUTH'];
		}
	}
	if ($auth === null) {
		//curl -F "accountType=HOSTED" -F "Email=apiaccess@gymfu.com" -F "Passwd=3d81be1229" -F "service=wise" -F "source=GymFu-exerciseplistconverter-1.0" https://www.google.com/accounts/ClientLogin
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.google.com/accounts/ClientLogin");
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, "accountType=HOSTED&Email=apiaccess@brainbakery.com&Passwd=0164087f&service=wise&source=BBl-apiaccess-1.0");
		$body = curl_exec($ch);
		/*
		SID=DQAAAIAAAABdJBszU6agWBEutzD65_90daZQPyFY7xp8GaNpOg-srChMaXis-DQIpwcwC_YY-SbTNAe8zhzcSgFjky929qtu16r9irNFFFyqRqJz_QOoG_Fakr8sU7vh-PVytP5VDoUqv3tcGWedLnq5FEGOMPwMAM98QzJ5ELclyF-YjasUXA
		LSID=DQAAAIIAAAAmbW2fUSO8EHR5QEef240w02n_X2kxZdXw_lsG-nY3-Dt-KV4d0l3769Agl2oWP6uT3iFGr_LnTKZgiiSX2LVOPaU30FE5DEdceNgfVSXmKflCvoUcMtSVIaJERNt706QTKFuB6DJPkjuNJNF6v_McU2DMObEFmfNl2hzAANPxa8GSOIbxGkwl8r_1WL_BB78
		Auth=DQAAAIEAAAAmbW2fUSO8EHR5QEef240w02n_X2kxZdXw_lsG-nY3-Dt-KV4d0l3769Agl2oWP6tH_Zv3vucaM0WzqynLZ4Z1u5YvIicK-AaToWKGPjcd7_hq_kdJP_TRH_B1saFEimeM11tiCbmX3rgZQPdK4spNe2rSBodto11gxqYuLtwIt0GnjLcxk54Qj4fFjrc_KU4
		*/
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
		curl_close($ch);
		if ($status == 200) {
			//echo "Authentication success\n";
		} else {
			die("Authentication failed with status code: $status, body: $body\n");
		}
		
		$tmp = explode("\n",$body);
		$tmp = explode("=",$tmp[2]);
		$auth = $tmp[1];
	}
	return $auth;
}

function GetGDocsCSVCurlHandle($key,$page,$auth) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=$key&exportFormat=csv&gid=$page");
	//curl_setopt($ch, CURLOPT_URL, "https://docs.google.com/feeds/default/private/full");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_HTTPGET, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER,array("GData-Version: 3.0","Authorization: GoogleLogin auth=$auth"));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	return $ch;

}

function GetGDocsCSVMulti($keyPageFiles) {
	$auth = GetGDocsAuth();
	$mh = curl_multi_init();
	$chs = array();
	foreach ($keyPageFiles as $i=>$keyPageFile) {
		$key = $keyPageFile[0];
		$page = $keyPageFile[1];
		$ch = GetGDocsCSVCurlHandle($key,$page,$auth);
		$chs[$i] = $ch;
		curl_multi_add_handle($mh,$ch);
	}
	$active = null;

	//execute the handles
	do {
		$mrc = curl_multi_exec($mh, $active);
	} while ($mrc == CURLM_CALL_MULTI_PERFORM);
	
	while ($active && $mrc == CURLM_OK) {
		curl_multi_select($mh); // Ignore return result due to https://bugs.php.net/bug.php?id=63411
		if (true) {
			do {
				$mrc = curl_multi_exec($mh, $active);
			} while ($mrc == CURLM_CALL_MULTI_PERFORM);
		}
	}
	$result = array();
	foreach ($chs as $i=>$ch) {
		$body = curl_multi_getcontent($ch);
		$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);	
		if ($status != 200) {
			die("\nERROR: Document fetch failed with status code: $status, body: $body\n");
		}
		$result[$i] = $body;
		if (count($keyPageFiles[$i])>2) {
			file_put_contents($keyPageFiles[$i][2],$body);
		}
		curl_multi_remove_handle($mh, $ch);
	}
	curl_multi_close($mh);
	return $result;
}

function GetGDocsCSV($key,$page=0,$auth = null) {
	if (!$auth) {
		$auth = GetGDocsAuth();
	}
	$ch = GetGDocsCSVCurlHandle($key,$page,$auth);

	$body = curl_exec($ch);
	$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	curl_close($ch);
	//echo $status."\n".$body."\n";
	
	if ($status != 200) {
		die("Document fetch failed with status code: $status, body: $body\n");
	}
	return $body;
}

function GDoc2Plist($key,$dest) {
	file_put_contents("temp.csv",GetGDocsCSV($key));
	$f = fopen("temp.csv","r");
	$colnames = fgetcsv($f);
	$result =array();
	while ($arr = fgetcsv($f)) {
		$new = array();
		foreach ($colnames as $i=>$k) {
			$new[$k] = $arr[$i];
		}
		if (!empty($new)) {
			$result[]=$new;
		}
	}
	fclose($f);
	unlink("temp.csv");
	//print_r($result);
	$plist = plist($result);
	if (!empty($plist)) {
		file_put_contents("../$dest",$plist);
		echo "Conversion succeeded.\n";
	} else {
		echo "Conversion failed.\n";	
	}	
}
