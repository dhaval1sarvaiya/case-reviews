//
//  Note.h
//  CaseReviews
//
//  Created by Benjie Gillam on 21/07/2011.
//  Copyright (c) 2011 BrainBakery Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Case;

@interface Note : NSManagedObject {
@private
}
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) Case * Case;

@end
