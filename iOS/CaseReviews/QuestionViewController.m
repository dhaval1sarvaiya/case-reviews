//
//  QuestionViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 13/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "QuestionViewController.h"
#import "ImageViewController.h"
#import "Case.h"
#import "AppDelegate.h"

@implementation QuestionViewController
@synthesize item;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
	RELEASE(topBar);//auto
	RELEASE(_webView);//auto
	self.item = nil;//auto
	[super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

- (void)viewDidLoad {
    [super viewDidLoad];
	self.navigationItem.title = @"Question";
	self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)] autorelease];
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Submit" style:UIBarButtonItemStyleBordered target:self action:@selector(submit)] autorelease];
	[_webView loadRequest:[NSURLRequest requestWithURL:[APP.book URLForResource:SWF(@"question-%@",[self.item objectForKey:@"case"]) withExtension:@"html" subdirectory:@"html"]]];
}

- (void)viewDidUnload {
	[self retain];//auto
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	RELEASE(topBar);//auto
	RELEASE(_webView);//auto
	[self autorelease];//auto
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
#pragma mark -
-(IBAction)close {
	[self dismissModalViewControllerAnimated:YES];
}
-(IBAction)submit {
	NSString *result = [_webView stringByEvaluatingJavaScriptFromString:@"submitAnswers();"];
	int r = 0;
	if ([result isEqualToString:@"correct"]) {
		[topBar setTintColor:RGB(20,180,0)]; // < RICHARD you can change the RGB value
		[[topBar topItem] setTitle:@"Correct"];
		r = 2;
	} else {
		[topBar setTintColor:RGB(255,20,0)]; // < RICHARD you can change the RGB value
		[[topBar topItem] setTitle:@"Incorrect"];
		r = 1;
	}
	ImageViewController *ivc = self.imageViewController;
	Case *c = [ivc currentCase];
	if (!c) {
		c = COREDATA_CREATE(Case);
		c.numberInt = [[ivc.playlist.currentItem objectForKey:@"case"] intValue];
	}
	c.questionStateInt = r;
	[APP saveContext];
	[ivc displayCurrent];
	
	if (![[NSUserDefaults standardUserDefaults] objectForKey:WRAP_KEY(@"assessmentStarted")]) {
		[[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:WRAP_KEY(@"assessmentStarted")];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
	topBar.topItem.rightBarButtonItem = nil;
}


@end
