//
//  NotesViewController_iPhone.m
//  CaseReviews
//
//  Created by Benjie Gillam on 22/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "NotesViewController_iPhone.h"
#import "NewNoteViewController.h"
#import "AppDelegate.h"

@implementation NotesViewController_iPhone
-(void)viewDidLoad {
	[super viewDidLoad];
	navBar.topItem.title = SWF(@"Case %i",[[self.parent.playlist.currentItem objectForKey:@"case"] intValue]);
}
-(void)openNote:(Note *)note {
	NewNoteViewController *nvc = ALOAD_VC(NewNoteViewController);
	nvc.parent = self.parent;
	nvc.notesViewController = self;
	nvc.note = note;
	[self.navigationController pushViewController:nvc animated:YES];
}
@end
