//
//  AppDelegate.m
//  CaseReviews
//
//  Created by Benjie Gillam on 11/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "AppDelegate.h"
#import "ContentsViewController_iPad.h"
#import "ContentsViewController_iPhone.h"
#import "ShelfViewController_iPad.h"
#import "ShelfViewController_iPhone.h"
//#import "UAirship.h"
//#import "UAStoreFront.h"
//#import "UAStoreFrontUI.h"

@implementation AppDelegate


@synthesize window=_window;

@synthesize managedObjectContext=__managedObjectContext;

@synthesize managedObjectModel=__managedObjectModel;

@synthesize persistentStoreCoordinator=__persistentStoreCoordinator;

@synthesize splitViewController,navigationController;

@synthesize book=_book;
@synthesize shelfViewController, shelfNavigationController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	NSLOG(@"Using flurry key: %@",kFlurryAPIKey);
	[Flurry startSession:kFlurryAPIKey];
	if (IPAD_IDIOM) {
		[Flurry logEvent:@"OpenBig"];
	} else {
		[Flurry logEvent:@"OpenLittle"];		
	}
	
	
	//Init Airship launch options
//	[UAStoreFront useCustomUI:[UAStoreFrontUI class]];

//	NSMutableDictionary *takeOffOptions = [[[NSMutableDictionary alloc] init] autorelease];
//	[takeOffOptions setValue:launchOptions forKey:UAirshipTakeOffOptionsLaunchOptionsKey];
	
	// Create Airship singleton that's used to talk to Urban Airship servers.
	// Please populate AirshipConfig.plist with your info from http://go.urbanairship.com
//	[UAirship takeOff:takeOffOptions];
	
	self.shelfNavigationController = [[[UINavigationController alloc] init] autorelease];
	shelfViewController = [ALOAD_VC(ShelfViewController) retain];
	shelfViewController.view.frame = [[UIScreen mainScreen] applicationFrame];
	self.shelfNavigationController.viewControllers = @[shelfViewController];
	self.window.rootViewController = self.shelfNavigationController;
	[self appOpened];
	[self.window makeKeyAndVisible];
	[Appirater appLaunched:YES];
    return YES;	
}
- (void) loadBook: (NSString *)bookId {
	// Override point for customization after application launch.
}

- (void)applicationWillResignActive:(UIApplication *)application {
	[self saveContext];
	/*
	 Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	 Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	 */
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	[self saveContext];
	/*
	 Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	 If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	 */
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	/*
	 Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	 */
	[Appirater appEnteredForeground:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	/*
	 Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	 */
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Saves changes in the application's managed object context before the application terminates.
	[self saveContext];
//	[UAirship land];
}

- (void)dealloc {
	[_window release];
	[__managedObjectContext release];
	[__managedObjectModel release];
	[__persistentStoreCoordinator release];
	self.splitViewController = nil;//auto
	self.navigationController = nil;//auto
	[super dealloc];
}

- (void)awakeFromNib {
    /*
     Typically you should set up the Core Data stack here, usually by passing the managed object context to the first view controller.
     self.<#View controller#>.managedObjectContext = self.managedObjectContext;
    */
}
-(void)saveManagedObjectContext {
	[self saveContext];
}
- (void)saveContext {
	if (!self.book) {
		NSLog(@"Not saving - no book loaded.");
		return;
	}
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext {
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}



/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel {
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"CaseReviews" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];    
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
	NSASSERT1(self.book != nil, @"No book?! %@",self.book);
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:SWF(@"%@.sqlite",self.book.identifier)];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory {
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - 
-(id)loadVC:(Class)cls {
	NSString *suffix = @"iPhone";
	if (IPAD_IDIOM) {
		suffix = @"iPad";
	}
	NSString *className = SWF(@"%@_%@",NSStringFromClass(cls),suffix);
	Class newClass = NSClassFromString(className);
	if (newClass) cls = newClass;
	return LOAD_VC(cls);
}
-(void)appOpened {
	
}

-(void)setDefaultHidden:(BOOL)hidden animated:(BOOL)animated {
	
}
-(UIImage *)imageNamed:(NSString *)name {
	UIImage *result = nil;
	if (self.book) {
		result = [self.book imageNamed:name];
	}
	if (!result) {
		result = [UIImage imageNamed:name];
	}
	return result;
}
-(UIImage *)imageWithContentsOfFile:(NSString *)file {
	UIImage *result = nil;
	if (self.book) {
		result = [self.book imageWithContentsOfFile:file];
	}
	if (!result) {
		result = [UIImage imageWithContentsOfFile:file];
	}
	return result;
}

-(void)openBook:(BBBook *)book fromRect:(CGRect)rect {
	if (self.book) return;
	self.book = book;
	[Flurry logEvent:self.book.shortIdentifier timed:YES];
}
-(void)closeBook {
	if (!self.book) return;
	[Flurry endTimedEvent:self.book.shortIdentifier withParameters:nil];
	[self saveContext];
	if (__persistentStoreCoordinator) {
		RELEASE(__persistentStoreCoordinator);
	}
	if (__managedObjectModel) {
		RELEASE(__managedObjectModel);
	}
	if (__managedObjectContext) {
		RELEASE(__managedObjectContext);
	}
	self.book = nil;
}

@end
