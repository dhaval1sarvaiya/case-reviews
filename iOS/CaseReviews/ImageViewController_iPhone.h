//
//  ImageViewController_iPhone.h
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageViewController.h"


@interface ImageViewController_iPhone : ImageViewController {
    IBOutlet UIButton *titleButton;
}

@end
