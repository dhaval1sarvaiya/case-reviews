//
//  Playlist.h
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
	PlaylistTypeNormal,
	PlaylistTypeBookmarks,
	PlaylistTypeNotes,
	PlaylistTypeQuestionsUnanswered,
	PlaylistTypeQuestionsCorrect,
	PlaylistTypeQuestionsIncorrect
} PlaylistType;

@interface Playlist : NSObject {
	NSUInteger _position;
    NSMutableArray *_items;
	NSMutableArray *_filteredItems;
	PlaylistType _type;
	NSUInteger _count;
	NSString *_title;
	BOOL isShuffled;
	
	NSMutableArray *_filters;
}
@property (readonly,assign) id currentItem;
@property (nonatomic,retain) NSString *title;
@property (readonly) BOOL isShuffled;
@property (readonly) BOOL isFiltered;
-(PlaylistType)type;
-(NSUInteger)count;
-(id)initWithTitle:(NSString *)title andItems:(NSArray *)items ofType:(PlaylistType)type;
//-(id)currentItem;
-(id)nextItem;
-(id)previousItem;
-(NSUInteger)position;
-(void)setPosition:(NSUInteger)position;
-(id)itemAtIndex:(NSUInteger)index;
-(BOOL)hasNext;
-(BOOL)hasPrevious;
-(void)toggleShuffle;
-(void)toggleShuffleWithCurrentFirst:(BOOL)currentFirst;
-(void)loadSpecial;
-(void)removeInvalid;
-(void)filterBy:(NSString *)searchText;
-(void)doFilter;
@end
