//
//  CaseCell.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "CaseCell.h"
#import "AppDelegate.h"
#import "Note.h"

@implementation CaseCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
	RELEASE(_item);
	RELEASE(numberLabel);//auto
	RELEASE(imagesContainer);//auto
	RELEASE(subtitleLabel);//auto
	[super dealloc];
}
#pragma mark -
-(void)setItem:(NSDictionary *)item inWidth:(CGFloat)width withTitle:(BOOL)withTitle {
	showTitle = withTitle;
	/*
	lastWidth = width;
	CGRect frame = self.frame;
	frame.size.width = width;
	frame.size.height = [CaseCell heightForCellWithEntry:item inWidth:width withTitle:withTitle];
	self.frame = frame;
	 */

	if (_item == item) return;
	RELEASE(_item);
	for (int i = [[imagesContainer subviews] count] - 1; i>=0;i--) {
		[[[imagesContainer subviews] objectAtIndex:i] removeFromSuperview];
	}
	subtitleLabel.font = [CaseCell font];
	
	
	_item = [item retain];
	NSString *caseNr = [[_item objectForKey:@"case"] description];
	numberLabel.text = caseNr;
	
	//[self updateImages];
	[self setNeedsLayout];
}
-(void)updateImages {
	UIView *v = [imagesContainer viewWithTag:12345];
	if (v) {
		[v removeFromSuperview];
	}
	NSString *caseNr = [[_item objectForKey:@"case"] description];
	CGFloat pad = [CaseCell padForWidth:self.frame.size.width];
	CGRect frame;
	frame.size.width = MIN(floor(imagesContainer.frame.size.width/6)-pad,imagesContainer.frame.size.height-pad);
	frame.size.height = frame.size.width;
	frame.origin.x = 0;
	frame.origin.y = floor((imagesContainer.frame.size.height - frame.size.height)/2);
	
	int i = 0;
	int c = [[imagesContainer subviews] count];
	for (NSDictionary *image in [_item objectForKey:@"images"]) {
		if (i < c) {
			UIImageView *iv = [[imagesContainer subviews] objectAtIndex:i];
			iv.frame = frame;
		} else {
			UIImageView *iv = [[UIImageView alloc] initWithFrame:frame];
			iv.backgroundColor = [UIColor blackColor];
			NSString *imageNr = [[image objectForKey:@"image"] description];
			iv.image = [APP.book imageWithContentsOfFile:[APP.book pathForResource:SWF(@"%@-%@",caseNr,imageNr) ofType:@"jpg" inDirectory:@"Thumbs"]];
			[imagesContainer addSubview:iv];
			RELEASE(iv);
		}
		if (i == 5) { // We don't support more than 6 images
			break;
		}
		frame.origin.x += frame.size.width + pad;
		i++;
	}	
	if ([[_item objectForKey:@"images"] count] > 6) {
		UIImageView *iv = [[UIImageView alloc] initWithFrame:frame];
		iv.tag = 12345;
		[iv setImage:[APP imageNamed:@"moreImg"]];
		[imagesContainer addSubview:iv];
		RELEASE(iv);
	}
}
-(void)layoutSubviews {
	[super layoutSubviews];
	CGRect frame = imagesContainer.frame;
	frame.origin.y = floor([CaseCell padForWidth:self.frame.size.width]/2);
	frame.size.width = self.frame.size.width - frame.origin.x;
	frame.size.height = [CaseCell heightForThumbnailsInWidth:self.frame.size.width];
	imagesContainer.frame = frame;
	
	frame = numberLabel.frame;
	frame.origin.y = imagesContainer.frame.origin.y;
	frame.size.height = imagesContainer.frame.size.height;
	numberLabel.frame = frame;
	
	if (showTitle) {
		subtitleLabel.hidden = NO;
		subtitleLabel.text = [CaseCell textForEntry:_item inWidth:self.frame.size.width];
		frame = subtitleLabel.frame;
		frame.origin.y = imagesContainer.frame.size.height + imagesContainer.frame.origin.y + [CaseCell padForWidth:self.frame.size.width];
		frame.size.height = [CaseCell heightForText:subtitleLabel.text inWidth:self.frame.size.width];
		subtitleLabel.frame = frame;
	} else {
		subtitleLabel.hidden = YES;
	}
	
	[self updateImages];
}
+(CGFloat)heightForCellWithEntry:(NSDictionary *)entry inWidth:(CGFloat)width withTitle:(BOOL)withTitle {
	CGFloat height = [self heightForThumbnailsInWidth:width] + [self padForWidth:width];
	if (withTitle) {
		height += [self heightForText:[self textForEntry:entry inWidth:width] inWidth:width];
		height += 2*[self padForWidth:width];
	}
	return height;
}
+(CGFloat)heightForThumbnailsInWidth:(CGFloat)width {
	return floor(width/7)-[self padForWidth:width];
}
+(CGFloat)heightForText:(NSString *)text inWidth:(CGFloat)width {
	return [text sizeWithFont:[self font] constrainedToSize:CGSizeMake(width-50, 9999) lineBreakMode:UILineBreakModeWordWrap].height;
}
+(CGFloat)padForWidth:(CGFloat)width {
	CGFloat pad = floor(width/64);
	return pad;
}
+(NSString *)textForEntry:(NSDictionary *)entry inWidth:(CGFloat)width {
	NSDictionary *c = [[APP.book allCases] objectForKey:[[entry objectForKey:@"case"] description]];
	if ([entry objectForKey:@"notes"]) {
		NSMutableString *str = [NSMutableString string];
		BOOL first = YES;
		for (Note *n in [entry objectForKey:@"notes"]) {
			if (!first) {
				[str appendString:@"\n"];
			}
			[str appendString:n.note];
			first = NO;
		}
		return str;
	} else {
		return [c objectForKey:@"diagnosis"];
	}
}
+(UIFont *)font {
	return [UIFont systemFontOfSize:[UIFont systemFontSize]];
}
@end
