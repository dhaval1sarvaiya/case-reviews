//
//  AssessmentViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 22/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
	ClearSourceQuestions=0,
	ClearSourceBookmarks=1,
	ClearSourceNotes=2
} ClearSource;

@interface AssessmentViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate> {
    IBOutlet UITableView *_tableView;
	IBOutlet UIButton *clearAssessmentButton;
	IBOutlet UIView *clearAssessmentContainer;
}

-(IBAction)clearAssessmentButtonPressed;
-(void)areYouSure:(ClearSource)m;
-(IBAction)clearButtonPressed:(UIButton *)sender;
@end
