//
//  HTMLViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface HTMLViewController : UIViewController <UIWebViewDelegate,MFMailComposeViewControllerDelegate, UIActionSheetDelegate> {
    IBOutlet UIWebView *_webView;
	
	NSString *fileToLoad;
}
-(void)loadFile:(NSString *)file;
-(void)resizeViewport;
@end
