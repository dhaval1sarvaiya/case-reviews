//
//  Playlist.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "Playlist.h"
#import "Case.h"
#import "Note.h"
#import "AppDelegate.h"

@implementation Playlist
@synthesize title=_title;
@synthesize isShuffled;
-(id)initWithTitle:(NSString *)title andItems:(NSArray *)items ofType:(PlaylistType)type {
	self = [super init];
	if (self) {
		self.title = title;
		if (!items) {
			items = [NSArray array];
		}
		_items = [[NSMutableArray alloc] initWithArray:items];
		_filteredItems = [[NSMutableArray alloc] initWithArray:_items];
		_position = 0;
		_type = type;
		_count = [items count];
		[self loadSpecial];
	}
	return self;
}
-(PlaylistType)type {
	return _type;
}
-(NSUInteger)count {
	return _count;
}
-(NSUInteger)position {
	return _position;
}
-(void)setPosition:(NSUInteger)position {
	_position = position;
}
-(id)currentItem {
	if (_position >= _count) {
		return nil;
	}
	return [_filteredItems objectAtIndex:_position];
}
-(id)nextItem {
	if ([self hasNext]) {
		_position++;
	}
	return [self currentItem];
}
-(id)previousItem {
	if ([self hasPrevious]) {
		_position--;
	}
	return [self currentItem];
}
- (void)dealloc {
	RELEASE(_items);
	RELEASE(_filteredItems);
	RELEASE(_filters);
	RELEASE(_title);//auto
	[super dealloc];
}
-(id)itemAtIndex:(NSUInteger)index {
	return [_filteredItems objectAtIndex:index];
}
-(BOOL)hasNext {
	return _position < _count-1;
}
-(BOOL)hasPrevious {
	return _position > 0;
}
// From http://stackoverflow.com/questions/791232/canonical-way-to-randomize-an-nsarray-in-objective-c
static NSUInteger random_below(NSUInteger n) {
    NSUInteger m = 1;
	
    // Compute smallest power of two greater than n.
    // There's probably a faster solution than this loop, but bit-twiddling
    // isn't my specialty.
    do {
        m <<= 1;
    } while(m < n);
	
    NSUInteger ret;
	
    do {
        ret = random() % m;
    } while(ret >= n);
	
    return ret;
}
-(void)toggleShuffle {
	[self toggleShuffleWithCurrentFirst:YES];
}
-(void)toggleShuffleWithCurrentFirst:(BOOL)currentFirst {
	if (_count == 0) return;
	id currentItem = [self currentItem];
	if (isShuffled) {
		[_filteredItems sortUsingDescriptors:[NSArray arrayWithObjects:[[[NSSortDescriptor alloc] initWithKey:@"case" ascending:YES] autorelease], nil]];
	} else {

		// http://en.wikipedia.org/wiki/Knuth_shuffle
		for(NSUInteger i = [_filteredItems count]; i > 1; i--) {
			NSUInteger j = random_below(i);
			[_filteredItems exchangeObjectAtIndex:i-1 withObjectAtIndex:j];
		}	
	}
	isShuffled = !isShuffled;
	
	_position = [_filteredItems indexOfObject:currentItem];
	if (currentFirst && _position != 0 && isShuffled) {
		[_filteredItems exchangeObjectAtIndex:_position withObjectAtIndex:0];
		_position = 0;
	}
	//return [_items objectAtIndex:_position];
}
-(void)loadSpecial {
	//id current = [[self currentItem] retain];
	if (_type != PlaylistTypeNormal) {
		[_items removeAllObjects];
		if (_type == PlaylistTypeBookmarks || _type == PlaylistTypeNotes) {
			[_items addObjectsFromArray:[APP.book.allCases allValues]];
		} else if (_type == PlaylistTypeQuestionsUnanswered || _type == PlaylistTypeQuestionsCorrect || _type == PlaylistTypeQuestionsIncorrect) {
			[_items addObjectsFromArray:[APP.book.allCasesWithQuestions allValues]];
		}
	}
	/*
	if (_type == PlaylistTypeBookmarks) {
		[_items removeAllObjects];
		NSArray *bookmarkedCases = [Case arrayMatchingPredicate:PWF(@"isStarred = %@",[NSNumber numberWithBool:YES]) orderedBy:@"number" orderAscending:YES];
		for (Case *c in bookmarkedCases) {
			NSDictionary *item = [APP.allCases objectForKey:[c.number description]];
			if (!item) {
				NSLogOkay(@"WARNING: Missing case: %i",c.numberInt);
			} else {
				[_items addObject:item];
			}
		}
	} else if (_type == PlaylistTypeNotes) {
		[_items removeAllObjects];
		NSArray *notes = [Note arrayMatchingPredicate:nil];
		for (Note *n in notes) {
			NSMutableDictionary *item = nil;
			for (NSMutableDictionary *e in _items) {
				if ([[e objectForKey:@"case"] intValue] == n.Case.numberInt) {
					item = [e retain];
					break;
				}
			}
			if (!item) {
				item = [[NSMutableDictionary alloc] initWithDictionary:[APP.allCases objectForKey:[n.Case.number description]]];
				[item setObject:[NSMutableArray array] forKey:@"notes"];
			}
			if (!item) {
				NSLogOkay(@"WARNING: Missing case: %i",n.Case.numberInt);
			} else {
				[[item objectForKey:@"notes"] addObject:n];
				if (![_items containsObject:item]) {
					[_items addObject:item];
				}
			}				
			RELEASE(item);
		}
	} else if (_type == PlaylistTypeQuestionsUnanswered || _type == PlaylistTypeQuestionsCorrect || _type == PlaylistTypeQuestionsIncorrect) {
		[_items removeAllObjects];
		[_items addObjectsFromArray:[APP.allCasesWithQuestions allValues]];
		[self removeInvalid];
	}
	 */
	for (int i = 0, l = [_items count]; i<l; i++) {
		[_items replaceObjectAtIndex:i withObject:[NSMutableDictionary dictionaryWithDictionary:[_items objectAtIndex:i]]];
	}
	[_filteredItems removeAllObjects];
	[_filteredItems addObjectsFromArray:_items];
	_position = 0;	
	_count = [_filteredItems count];
	[self removeInvalid];
	[self doFilter];
}
-(void)removeInvalid {
	NSArray *bookmarkedCases = nil;
	NSArray *notes = nil;
	NSArray *relevant = nil;
	if (_type == PlaylistTypeBookmarks) {
		bookmarkedCases = [Case arrayMatchingPredicate:PWF(@"isStarred = %@",[NSNumber numberWithBool:YES]) orderedBy:@"number" orderAscending:YES];
	} else if (_type == PlaylistTypeNotes) {
		notes = [Note arrayMatchingPredicate:nil];
	} else if (_type == PlaylistTypeQuestionsUnanswered) {
		relevant = [Case arrayMatchingPredicate:PWF(@"questionState>0")];
	} else if (_type == PlaylistTypeQuestionsCorrect) {
		relevant = [Case arrayMatchingPredicate:PWF(@"questionState = 2")];
	} else if (_type == PlaylistTypeQuestionsIncorrect) {
		relevant = [Case arrayMatchingPredicate:PWF(@"questionState = 1")];
	} else {
		return;
	}
	for (int i = [_items count] - 1; i >= 0; i--) {
		NSMutableDictionary *item = [_items objectAtIndex:i];
		BOOL okay = NO;
		if (_type == PlaylistTypeBookmarks) {
			for (int j = 0, m = [bookmarkedCases count]; j<m; j++) {
				if ([(Case *)[bookmarkedCases objectAtIndex:j] numberInt] == [[item objectForKey:@"case"] intValue]) {
					okay = YES;
					break;
				}
			}
		} else if (_type == PlaylistTypeNotes) {
			[item setObject:[NSMutableArray array] forKey:@"notes"];
			for (int j = 0, m = [notes count]; j<m; j++) {
				Note *n = [notes objectAtIndex:j];
				if (n.Case.numberInt == [[item objectForKey:@"case"] intValue]) {
					[[item objectForKey:@"notes"] addObject:n];
				}
			}
			if ([[item objectForKey:@"notes"] count] > 0) {
				okay = YES;
			}
		} else if (_type == PlaylistTypeQuestionsUnanswered) {
			okay = YES;
			for (Case *c in relevant) {
				if ([[item objectForKey:@"case"] intValue] == c.numberInt) {
					okay = NO;
					break;
				}
			}
		} else if (_type == PlaylistTypeQuestionsCorrect || _type == PlaylistTypeQuestionsIncorrect) {
			for (Case *c in relevant) {
				if ([[item objectForKey:@"case"] intValue] == c.numberInt) {
					okay = YES;
					break;
				}
			}			
		}
		if (!okay) {
			[_items removeObjectAtIndex:i];
		}
	}
	for (int i = [_filteredItems count] - 1; i>=0;i--) {
		if (![_items containsObject:[_filteredItems objectAtIndex:i]]) {
			[_filteredItems removeObjectAtIndex:i];
			if (_position >= i && _position > 0) {
				_position--;
				_count--;
			}
		}
	}
	_count = [_filteredItems count];
}
-(void)doFilter {
	int l = [_filters count];
	[_filteredItems removeAllObjects];
	[_filteredItems addObjectsFromArray:_items];
	_position = 0;
	if (l > 0) {
		NSMutableSet *filter = [[NSMutableSet alloc] init];
		for (int j = 0, m = [_filteredItems count]; j<m; j++) {
			[filter addObject:[[_filteredItems objectAtIndex:j] objectForKey:@"case"]];
		}
		NSLog(@"Filter 0/%i: %@",l,[[filter allObjects] sortedArrayUsingSelector:@selector(compare:)]);
		for (int i = 0; i<l; i++) {
			NSString *term = [_filters objectAtIndex:i];
			NSMutableSet *valid = [[NSMutableSet alloc] init];
			if (i < l-1) {
				//Exact match only
				NSArray *cases = [APP.book.searchDict objectForKey:term];
				[valid addObjectsFromArray:cases];
			} else {
				//Prefix match.
				NSArray *keys = [APP.book.searchDict allKeys];
				keys = [keys filteredArrayUsingPredicate:PWF(@"SELF BEGINSWITH %@",term)];
				for (NSString *key in keys) {
					[valid addObjectsFromArray:[APP.book.searchDict objectForKey:key]];
				}
			}
			[filter intersectSet:valid];
			NSLog(@"Filter %i/%i: %@",i+1,l,[[filter allObjects] sortedArrayUsingSelector:@selector(compare:)]);
			RELEASE(valid);
			if ([filter count] == 0) {
				break;
			}
		}
		for (int i = [_filteredItems count]-1; i>=0;i--) {
			if (![filter containsObject:[[_filteredItems objectAtIndex:i] objectForKey:@"case"]]) {
				[_filteredItems removeObjectAtIndex:i];
			}
		}
	}
	isShuffled = NO;
	[_filteredItems sortUsingDescriptors:[NSArray arrayWithObjects:[[[NSSortDescriptor alloc] initWithKey:@"case" ascending:YES] autorelease], nil]];
	_count = [_filteredItems count];
}
-(void)filterBy:(NSString *)searchText {
	RELEASE(_filters);
	searchText = [searchText lowercaseString];
	searchText = [searchText stringByReplacingOccurrencesOfRegex:@"[^a-z0-9]" withString:@" "];
	searchText = [searchText stringByReplacingOccurrencesOfRegex:@" +" withString:@" "];
	searchText = [searchText stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	NSArray *words = [searchText componentsSeparatedByString:@" "];
	_filters = [[NSMutableArray alloc] init];
	for (NSString *word in words) {
		if ([word length] > 0) {
			[_filters addObject:word];
		}
	}
	if ([_filters count] < 1) {
		RELEASE(_filters);
	}
	[self doFilter];
}
-(BOOL)isFiltered {
	return _filters != nil;
}
@end
