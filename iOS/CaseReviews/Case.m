//
//  Case.m
//  CaseReviews
//
//  Created by Benjie Gillam on 21/07/2011.
//  Copyright (c) 2011 BrainBakery Ltd. All rights reserved.
//

#import "Case.h"
#import "Note.h"


@implementation Case
@dynamic number;
@dynamic isStarred;
@dynamic questionState;
@dynamic Notes;
PROPERTY_ALIAS_INT_M(number);
PROPERTY_ALIAS_INT_M(questionState);
PROPERTY_ALIAS_BOOL_M(isStarred);

- (void)addNotesObject:(Note *)value {    
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"notes" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"notes"] addObject:value];
    [self didChangeValueForKey:@"notes" withSetMutation:NSKeyValueUnionSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)removeNotesObject:(Note *)value {
    NSSet *changedObjects = [[NSSet alloc] initWithObjects:&value count:1];
    [self willChangeValueForKey:@"notes" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [[self primitiveValueForKey:@"notes"] removeObject:value];
    [self didChangeValueForKey:@"notes" withSetMutation:NSKeyValueMinusSetMutation usingObjects:changedObjects];
    [changedObjects release];
}

- (void)addNotes:(NSSet *)value {    
    [self willChangeValueForKey:@"notes" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"notes"] unionSet:value];
    [self didChangeValueForKey:@"notes" withSetMutation:NSKeyValueUnionSetMutation usingObjects:value];
}

- (void)removeNotes:(NSSet *)value {
    [self willChangeValueForKey:@"notes" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
    [[self primitiveValueForKey:@"notes"] minusSet:value];
    [self didChangeValueForKey:@"notes" withSetMutation:NSKeyValueMinusSetMutation usingObjects:value];
}


@end
