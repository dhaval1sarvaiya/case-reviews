//
//  HTMLViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "HTMLViewController.h"
#import "NSStringExtensions.h"


@implementation HTMLViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
	RELEASE(_webView);//auto
	RELEASE(fileToLoad);//auto
	[super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

- (void)viewDidUnload {
	[self retain];//auto
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	RELEASE(_webView);//auto
	[self autorelease];//auto
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
-(void)viewDidAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[self resizeViewport];
}

#pragma mark -
-(void)viewDidLoad {
	[super viewDidLoad];
	self.navigationItem.title = @"Loading...";
	if (fileToLoad) {
		[self loadFile:fileToLoad];
		RELEASE(fileToLoad);
	}
	[self resizeViewport];
	//[self performSelector:@selector(resizeViewport) withObject:nil afterDelay:0.1];
}
-(void)loadFile:(NSString *)file {
	if ([self isViewLoaded]) {
		[_webView loadRequest:[NSURLRequest requestWithURL:[APP.book URLForResource:file withExtension:@"html" subdirectory:@"html"]]];
	} else {
		fileToLoad = [file retain];
	}
}
-(void)resizeViewport {
	NSString *script = @"var vp = null;var els = document.getElementsByTagName('meta');for (var i = 0, l = els.length;i<l;i++) {var el = els[i]; if (el.name == 'viewport') {vp = el; break;}}";
	script = SWF(@"%@;if(vp){vp.setAttribute('content','width=%.0f,height=%.0f');}",script,_webView.frame.size.width,_webView.frame.size.height);
	[_webView stringByEvaluatingJavaScriptFromString:script];
	
}
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[self resizeViewport];	
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self resizeViewport];
}
-(void)back {
	[self.navigationItem setLeftBarButtonItem:nil animated:YES];
	[_webView goBack];
}
-(void)webViewDidStartLoad:(UIWebView *)webView {
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	self.navigationItem.title = @"Loading...";
}
-(void)webViewDidFinishLoad:(UIWebView *)webView {
	//[self resizeViewport];
	[self performSelector:@selector(resizeViewport) withObject:nil afterDelay:0.01];
	if ([_webView canGoBack]) {
		self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(back)] autorelease];
	} else {
		[self.navigationItem setLeftBarButtonItem:nil animated:YES];
	}
	self.navigationItem.title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
	if ([[[request URL] scheme] isEqualToString:@"http"] && [[[request URL] host] isEqualToString:@"api"]) {
		if ([[[[request URL] path] substringFromIndex:1] isEqualToString:@"share"]) {
			NSString *str = [_webView stringByEvaluatingJavaScriptFromString:@"var a = document.getElementById('sharediv');a.offsetLeft+','+a.offsetTop+','+a.offsetWidth+','+a.offsetHeight;"];
			if ([str length]>0) {
				NSArray *pos = [str componentsSeparatedByString:@","];
				int x = [[pos objectAtIndex:0] intValue];
				int y = [[pos objectAtIndex:1] intValue];
				int w = [[pos objectAtIndex:2] intValue];
				int h = [[pos objectAtIndex:3] intValue];
				
				CGRect r;
				r.origin.x = x;
				r.origin.y = y;
				r.size.width = w;
				r.size.height = h;
				
				UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter",@"LinkedIn",@"Email",nil];
				
				[as showFromRect:r inView:_webView animated:YES];
				RELEASE(as);
			}
		} else {
			ALERT(@"Unknown action", [[request URL] path], @"Okay");
		}
		return NO;
	} else if ([[[request URL] scheme] isEqualToString:@"http"] && ![[[request URL] host] isEqualToString:@"downloads.antbits.com"]) {
		[[UIApplication sharedApplication] openURL:[request URL]];
		return NO;
	} else if ([[[request URL] scheme] isEqualToString:@"mailto"]) {
		
		if ([MFMailComposeViewController canSendMail]) {
			MFMailComposeViewController *mvc = [[MFMailComposeViewController alloc] init];
			[mvc setMailComposeDelegate:self];
			
			
			
			NSArray *rawURLparts = [[[request URL] resourceSpecifier] componentsSeparatedByString:@"?"];
			if (rawURLparts.count > 2) {
				return NO; // invalid URL
			}
			
			NSMutableArray *toRecipients = [NSMutableArray array];
			NSString *defaultRecipient = [rawURLparts objectAtIndex:0];
			if (defaultRecipient.length) {
				[toRecipients addObject:defaultRecipient];
			}
			
			if (rawURLparts.count == 2) {
				NSString *queryString = [rawURLparts objectAtIndex:1];
				
				NSArray *params = [queryString componentsSeparatedByString:@"&"];
				for (NSString *param in params) {
					NSArray *keyValue = [param componentsSeparatedByString:@"="];
					if (keyValue.count != 2) {
						continue;
					}
					NSString *key = [[keyValue objectAtIndex:0] lowercaseString];
					NSString *value = [keyValue objectAtIndex:1];
					
					value =  (NSString *)CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault,(CFStringRef)value,CFSTR(""),kCFStringEncodingUTF8);
					[value autorelease];
					
					if ([key isEqualToString:@"subject"]) {
						[mvc setSubject:value];
					}
					
					if ([key isEqualToString:@"body"]) {
						[mvc setMessageBody:value isHTML:NO];
					}
					
					if ([key isEqualToString:@"to"]) {
						[toRecipients addObjectsFromArray:[value componentsSeparatedByString:@","]];
					}
					
					if ([key isEqualToString:@"cc"]) {
						NSArray *recipients = [value componentsSeparatedByString:@","];
						[mvc setCcRecipients:recipients];
					}
					
					if ([key isEqualToString:@"bcc"]) {
						NSArray *recipients = [value componentsSeparatedByString:@","];
						[mvc setBccRecipients:recipients];
					}
				}
			}
			
			[mvc setToRecipients:toRecipients];

			
			
			
			
			mvc.modalPresentationStyle = UIModalPresentationFormSheet;
			[self presentModalViewController:mvc animated:YES];
			RELEASE(mvc);
		} else {
			[[UIApplication sharedApplication] openURL:[request URL]];			
		}
		return NO;
	}
	return YES;
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
	[self dismissModalViewControllerAnimated:YES];
}
-(void)dismissMVC {
	[self dismissModalViewControllerAnimated:YES];
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex < 4) {
		NSString *shareTitle = @"UnknownApp";
		NSString *shareURL = @"http://www.example.com/";
		NSURL *url = nil;
		if (buttonIndex == 2) { //Linked In
			url = [NSURL URLWithString:SWF(@"http://www.linkedin.com/shareArticle?mini=true&ro=false&trk=bookmarklet&title=%@&url=%@",[shareTitle urlEncodedString],[shareURL urlEncodedString])];
		} else if (buttonIndex == 0) {//Facebook
			url = [NSURL URLWithString:SWF(@"http://www.facebook.com/sharer.php?src=bm&v=4&i=1313684243&u=%@&t=%@",[shareURL urlEncodedString],[shareTitle urlEncodedString])];
		} else if (buttonIndex == 1) { //Twitter
			url = [NSURL URLWithString:SWF(@"http://twitter.com/home?status=%@",[SWF(@"%@ - %@",shareTitle,shareURL) urlEncodedString])];
		} else if (buttonIndex == 3) {
			MFMailComposeViewController *mvc = [[MFMailComposeViewController alloc] init];
			mvc.mailComposeDelegate = self;
			[mvc setSubject:SWF(@"Download %@",shareTitle)];
			[mvc setMessageBody:SWF(@"You can get it from here:\n%@",shareURL) isHTML:NO];
			
			[mvc setModalPresentationStyle:UIModalPresentationFormSheet];
			[self presentModalViewController:mvc animated:YES];
			RELEASE(mvc);
		} else {
			ALERT(@"Share", SWF(@"Option %i",buttonIndex), @"Okay");
		}
		if (url) {
			UIViewController *vc = [[UIViewController alloc] init];
			CGRect frame = CGRectMake(0, 0, 300, 100);
			vc.view.frame = frame;
			[vc.view setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
			
			UIToolbar *nBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 300, 44)];
			[nBar setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
			[nBar setItems:[NSArray arrayWithObjects:[[[UIBarButtonItem alloc] initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissMVC)] autorelease],nil]];
			[vc.view addSubview:nBar];
			RELEASE(nBar);
			
			frame.origin.y += 44;
			frame.size.height -=44;
			UIWebView *v = [[UIWebView alloc] initWithFrame:frame];
			[v setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
			[v loadRequest:[NSURLRequest requestWithURL:url]];
			[vc.view addSubview:v];
			RELEASE(v);
			vc.modalPresentationStyle = UIModalPresentationFormSheet;
			[self presentModalViewController:vc animated:YES];
			[vc.view setNeedsDisplay];
			RELEASE(vc);
			//[[UIApplication sharedApplication] openURL:url];
		}
	}
}
@end
