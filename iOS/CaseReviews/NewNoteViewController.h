//
//  NewNoteViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 22/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageViewController.h"
#import "Note.h"

@class NotesViewController;

@interface NewNoteViewController : UIViewController <UITextViewDelegate> {
	IBOutlet UITextView *_textView;
	
	ImageViewController *parent;
	
	Note *_note;
}
@property (nonatomic,assign) ImageViewController *parent;
@property (nonatomic,readonly) Playlist *playlist;
@property (nonatomic,retain) Note *note;
@property (nonatomic, retain) NotesViewController *notesViewController;
@property (nonatomic, retain) IBOutlet UINavigationItem *templateNavItem;
-(IBAction)cancelButtonPressed;
-(IBAction)saveButtonPressed;
@end
