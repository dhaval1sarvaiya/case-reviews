//
//  NotesViewController_iPad.m
//  CaseReviews
//
//  Created by Benjie Gillam on 22/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "NotesViewController_iPad.h"
#import "NewNoteViewController.h"
#import "AppDelegate.h"

@implementation NotesViewController_iPad
-(void)openNote:(Note *)note {
	if (self.parent.popoverController) {
		[self.parent.popoverController dismissPopoverAnimated:YES];
		self.parent.popoverController = nil;
	}
	NewNoteViewController *nvc = ALOAD_VC(NewNoteViewController);
	nvc.parent = self.parent;
	nvc.notesViewController = self;
	nvc.note = note;
	UINavigationController *navVC = [[[UINavigationController alloc] initWithRootViewController:nvc] autorelease];
	navVC.navigationBar.barStyle = UIBarStyleBlackTranslucent;
	navVC.modalPresentationStyle = UIModalPresentationFormSheet;
	[self.parent presentModalViewController:navVC animated:YES];
}

@end
