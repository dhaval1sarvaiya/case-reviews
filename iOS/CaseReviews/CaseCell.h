//
//  CaseCell.h
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CaseCell : UITableViewCell {
	IBOutlet UILabel *numberLabel;
	IBOutlet UIView *imagesContainer;
	IBOutlet UILabel *subtitleLabel;
	
	
	BOOL showTitle;
	CGFloat lastWidth;
	NSDictionary *_item;
}
-(void)setItem:(NSDictionary *)item inWidth:(CGFloat)width withTitle:(BOOL)withTitle;
+(CGFloat)heightForCellWithEntry:(NSDictionary *)entry inWidth:(CGFloat)width withTitle:(BOOL)withTitle;
+(CGFloat)heightForThumbnailsInWidth:(CGFloat)width;
+(CGFloat)padForWidth:(CGFloat)width;
+(NSString *)textForEntry:(NSDictionary *)entry inWidth:(CGFloat)width;
+(CGFloat)heightForText:(NSString *)text inWidth:(CGFloat)width;
+(UIFont *)font;
@end
