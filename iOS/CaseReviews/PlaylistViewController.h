//
//  PlaylistViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Playlist.h"

@interface PlaylistViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate> {
    IBOutlet UITableView *_tableView;
	IBOutlet UIBarButtonItem *shuffleButton;
	
	Playlist *playlist;
	
	BOOL titles;
	IBOutlet UIButton *showTitlesButton;
	
	id topCell;
	BOOL first;
	
	IBOutlet UISearchBar *_searchBar;
	
	BOOL isSearch;
	
	NSTimer *searchBarTimer;
	
	
	IBOutlet UIView *emptyView;
	IBOutlet UIImageView *emptyImageView;
	IBOutlet UILabel *emptyLabel;
}
@property (nonatomic) BOOL isSearch;
@property (nonatomic,retain) Playlist *playlist;
-(void)displayViewController:(UIViewController *)vc animated:(BOOL)animated;
-(void) updateWithTableReload:(BOOL)reload;
-(IBAction)shuffleButtonPressed;
-(IBAction)showTitlesButtonPressed;
-(void)updateShowTitlesButton;
@end
