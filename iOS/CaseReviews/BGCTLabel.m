//
//  BGCTLabel.m
//  CaseReviews
//
//  Created by Benjie Gillam on 19/08/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "BGCTLabel.h"
#import <QuartzCore/CALayer.h>

@implementation BGCTLabel
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Helper method to release our cached Core Text framesetter and frame
- (void)clearPreviousLayoutInformation
{
    if (_framesetter != NULL) {
        CFRelease(_framesetter);
        _framesetter = NULL;
    }
    
    if (_frame != NULL) {
        CFRelease(_frame);
        _frame = NULL;
    }
	self.layer.geometryFlipped = YES;  // For ease of interaction with the CoreText coordinate system.
}

-(void)textChanged {
	[self setNeedsDisplay];    
    [self clearPreviousLayoutInformation];
	
	// Create the Core Text framesetter using the attributed string
    _framesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)_formattedText);
	
	// Create the Core Text frame using our current view rect bounds
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.bounds];
    _frame = CTFramesetterCreateFrame(_framesetter, CFRangeMake(0, 0), [path CGPath], NULL);
    
}
-(void)setFrame:(CGRect)frame {
	[super setFrame:frame];

}
-(void)setHidden:(BOOL)hidden {
	[super setHidden:hidden];
	if (!hidden) {
		[self setNeedsDisplay];
	}
}

-(NSAttributedString *)formatText:(NSString *)txt {
		
	NSMutableAttributedString *as = [[NSMutableAttributedString alloc] init];
	
#define DWOK(...) [NSDictionary dictionaryWithObjectsAndKeys:__VA_ARGS__]
#define CTFWT(font,trait) ((id)CTFontCreateCopyWithSymbolicTraits((CTFontRef)font,0.0,NULL,trait,trait))
	
	UIFont *myFont = self.font;
	id font = (id)(CTFontCreateWithName((CFStringRef)[myFont fontName],[myFont pointSize],NULL));
	
	id fontColor = (id)([self.textColor CGColor]);
	NSDictionary *attrNormal = DWOK(fontColor,kCTForegroundColorAttributeName,font,kCTFontAttributeName, nil);
	NSDictionary *attrBold = DWOK(fontColor,kCTForegroundColorAttributeName,CTFWT(font,kCTFontBoldTrait),kCTFontAttributeName, nil);
	NSDictionary *attrItalic = DWOK(fontColor,kCTForegroundColorAttributeName,CTFWT(font,kCTFontItalicTrait),kCTFontAttributeName, nil);
	NSDictionary *attrUnderline = DWOK(fontColor,kCTForegroundColorAttributeName,font,kCTFontAttributeName, [NSNumber numberWithInt:1],kCTUnderlineStyleAttributeName, nil);
	NSDictionary *attrSuperscript = DWOK(fontColor,kCTForegroundColorAttributeName,font,kCTFontAttributeName, [NSNumber numberWithInt:1],kCTSuperscriptAttributeName, nil);
	NSDictionary *attrSubscript = DWOK(fontColor,kCTForegroundColorAttributeName,font,kCTFontAttributeName, [NSNumber numberWithInt:-1],kCTSuperscriptAttributeName, nil);
	
	//kCTSuperscriptAttributeName -1=sub 0=normal 1=super
	//kCTUnderlineStyleAttributeName kCTUnderlineStyleNone=none 1=underline
	
	for (int i = 0, l = [txt length]; i<l; i++) {
		NSString *substring = [txt substringFromIndex:i];
		int a = [substring rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"{"]].location;
		if (a != NSNotFound && a > 0) {
			NSRange r;
			r.location = a-1;
			r.length = 1;
			NSString *cmd = [substring substringWithRange:r];
			if ([cmd rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@"^_biu"]].location != NSNotFound) {
				int b = [[substring substringFromIndex:a+1] rangeOfString:@"}"].location;
				if (b != NSNotFound) {
					NSString *normal = [substring substringToIndex:a-1];
					r.location = a+1;
					r.length = b;
					NSString *styled = [substring substringWithRange:r];
					if ([normal length]>0) {
						[as appendAttributedString:[[[NSAttributedString alloc] initWithString:normal attributes:attrNormal] autorelease]];
					}
					if ([styled length]>0) {
						NSDictionary *styleDict = attrNormal;
						switch ([cmd characterAtIndex:0]) {
							case '^': styleDict = attrSuperscript;
								break;
							case '_': styleDict = attrSubscript;
								break;
							case 'b': styleDict = attrBold;
								break;
							case 'i': styleDict = attrItalic;
								break;
							case 'u': styleDict = attrUnderline;
								break;
						}
						[as appendAttributedString:[[[NSAttributedString alloc] initWithString:styled attributes:styleDict] autorelease]];
					}
					//NSLog(@"Normal: '%@', formatted: '%@'",normal,styled);
					i+=a+b+2;
					substring = @"";
				}
			}
		}
		if ([substring length] > 0) {
			[as appendAttributedString:[[[NSAttributedString alloc] initWithString:substring attributes:attrNormal] autorelease]];			
			i+=[substring length]-1;
		}
	}
	return [as autorelease];	
}
-(void)setText:(NSString *)text {
	NSLog(@"WARNING: Set text on a BGCTLabel!");
	[self setAndFormatText:text];
}
-(NSString *)text {
	NSLog(@"WARNING: Fetched text from a BGCTLabel!");
	return [_formattedText string];
}
-(void)setAndFormatText:(NSString *)txt {
	
	[self setFormattedText:[self formatText:txt]];
}
- (NSAttributedString *)formattedText {
    return _formattedText;
}
- (void)setFormattedText:(NSAttributedString *)text
{
    NSAttributedString *oldText = _formattedText;
    _formattedText = [text copy];
    [oldText release];
    
	// We need to call textChanged after setting the new property text to update layout
    [self textChanged];
}

-(CGFloat)heightForWidth:(CGFloat)width {
	//CGSize CTFramesetterSuggestFrameSizeWithConstraints(CTFramesetterRef framesetter,CFRange stringRange,CFDictionaryRef frameAttributes,CGSize constraints,CFRange* fitRange);
	CGSize s = CTFramesetterSuggestFrameSizeWithConstraints(_framesetter,CFRangeMake(0, [_formattedText length]),NULL,CGSizeMake(self.bounds.size.width, 9999),NULL);
	return s.height;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
	// Create the Core Text frame using our current view rect bounds
	UIBezierPath *path = [UIBezierPath bezierPathWithRect:self.bounds];
	_frame = CTFramesetterCreateFrame(_framesetter, CFRangeMake(0, 0), [path CGPath], NULL);
	CTFrameDraw(_frame, UIGraphicsGetCurrentContext());    
}

@end
