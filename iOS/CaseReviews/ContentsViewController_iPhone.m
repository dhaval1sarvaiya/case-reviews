//
//  ContentsViewController_iPhone.m
//  CaseReviews
//
//  Created by Benjie Gillam on 11/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "ContentsViewController_iPhone.h"

@implementation ContentsViewController_iPhone
-(void)viewDidLoad {
	[super viewDidLoad];
	self.navigationItem.title = @"Contents";
}
-(void)viewDidAppear:(BOOL)animated {
	NSIndexPath *ip = [_tableView indexPathForSelectedRow];
	if (ip) {
		[_tableView deselectRowAtIndexPath:ip animated:YES];
	}
	[super viewDidAppear:animated];//auto
}

#pragma mark -
-(void)displayViewController:(UIViewController *)vc animated:(BOOL)animated {
	[self.navigationController pushViewController:vc animated:animated];
}
-(void)viewWillAppear:(BOOL)animated {
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:animated];
	[super viewWillAppear:animated];//auto
}
@end
