//
//  PlaylistViewController_iPad.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "PlaylistViewController_iPad.h"
#import "AppDelegate.h"

@implementation PlaylistViewController_iPad
-(void)displayViewController:(UIViewController *)vc animated:(BOOL)animated {
	UINavigationController *navVC = [[[UINavigationController alloc] initWithRootViewController:vc] autorelease];
	navVC.navigationBar.barStyle = UIBarStyleBlack;
	[APP.splitViewController presentModalViewController:navVC animated:animated];
}
-(void)viewDidLoad {
	[super viewDidLoad];
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:showTitlesButton] autorelease];
}
@end
