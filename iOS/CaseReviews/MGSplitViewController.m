//
//  MGSplitViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 19/11/2013.
//  Copyright (c) 2013 BrainBakery Ltd. All rights reserved.
//

#import "MGSplitViewController.h"

@interface MGSplitViewController ()

@end

@implementation MGSplitViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		NSMutableArray *viewControllers = [[self viewControllers] mutableCopy];
		if (!viewControllers) viewControllers = [NSMutableArray arrayWithCapacity:2];
		NSUInteger c = [viewControllers count];
		while (c++ < 2) {
			[viewControllers addObject:[[[UIViewController alloc] init] autorelease]];
		}
		self.viewControllers = viewControllers;
    }
    return self;
}

- (void)setViewControllerAtIndex:(NSUInteger)index to:(UIViewController *)viewController
{
	NSMutableArray *viewControllers = [[self viewControllers] mutableCopy];
	[viewControllers replaceObjectAtIndex:index withObject:viewController];
	self.viewControllers = viewControllers;
}

- (void)setMasterViewController:(UIViewController *)masterViewController
{
	[self setViewControllerAtIndex:0 to:masterViewController];
}

- (UIViewController *)masterViewController
{
	return [[self viewControllers] objectAtIndex:0];
}

- (void)setDetailViewController:(UIViewController *)detailViewController
{
	[self setViewControllerAtIndex:1 to:detailViewController];
}

- (UIViewController *)detailViewController
{
	return [[self viewControllers] objectAtIndex:1];
}

@end
