//
//  QuestionViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 13/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageViewController;

@interface QuestionViewController : UIViewController <UIWebViewDelegate> {
    IBOutlet UINavigationBar *topBar;
	IBOutlet UIWebView *_webView;
	
	NSDictionary *item;
}
@property (nonatomic,retain) NSDictionary *item;
@property (nonatomic, retain) ImageViewController *imageViewController;
-(IBAction)close;
-(IBAction)submit;
@end
