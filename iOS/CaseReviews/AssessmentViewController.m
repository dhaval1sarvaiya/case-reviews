//
//  AssessmentViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 22/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "AssessmentViewController.h"
#import "Case.h"
#import "AppDelegate.h"
#import "Playlist.h"
#import "PlaylistViewController.h"
#import "Note.h"

@implementation AssessmentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		[Flurry logEvent:@"OverviewViewed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
    }
    return self;
}

- (void)dealloc {
	RELEASE(_tableView);//auto
	RELEASE(clearAssessmentButton);//auto
	RELEASE(clearAssessmentContainer);//auto
	[super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

- (void)viewDidUnload {
	[self retain];//auto
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	RELEASE(_tableView);//auto
	RELEASE(clearAssessmentButton);//auto
	RELEASE(clearAssessmentContainer);//auto
	[self autorelease];//auto
}
-(void)viewWillAppear:(BOOL)animated {
	[_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
	[super viewWillAppear:animated];//auto
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
#pragma mark - Table view
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 3;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
	NSString *str = @"-";
	if (section == 0) {
		if ([[NSUserDefaults standardUserDefaults] objectForKey:WRAP_KEY(@"assessmentStarted")]) {
			NSDateFormatter *df = [[NSDateFormatter alloc] init];
			[df setDateStyle:NSDateFormatterLongStyle];
			str = [df stringFromDate:[[NSUserDefaults standardUserDefaults] objectForKey:WRAP_KEY(@"assessmentStarted")]];
			RELEASE(df);
		}
		return SWF(@"Assessment since: %@",str);
	} else if (section == 1) {
		return @"Bookmarks";
	} else if (section == 2) {
		return @"Notes";
	}
	return nil;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	if (section == 0) {
		return 3;
	}
	return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
	return clearAssessmentContainer.frame.size.height;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
	UIView *footer = nil;
	if (section == 0) {
		footer = clearAssessmentContainer;
		if (![[NSUserDefaults standardUserDefaults] objectForKey:WRAP_KEY(@"assessmentStarted")]) {
			clearAssessmentButton.enabled = NO;
		}
	} else {
		footer = [[[UIView alloc] initWithFrame:clearAssessmentContainer.frame] autorelease];
		UIButton *btn = [UIButton buttonWithType:clearAssessmentButton.buttonType];
		btn.frame = clearAssessmentButton.frame;
		btn.backgroundColor = clearAssessmentButton.backgroundColor;
		[btn setBackgroundImage:[clearAssessmentButton backgroundImageForState:UIControlStateNormal] forState:UIControlStateNormal];
		[btn setBackgroundImage:[clearAssessmentButton backgroundImageForState:UIControlStateHighlighted] forState:UIControlStateHighlighted];
		[btn setBackgroundImage:[clearAssessmentButton backgroundImageForState:UIControlStateSelected] forState:UIControlStateSelected];
		[btn setBackgroundImage:[clearAssessmentButton backgroundImageForState:UIControlStateDisabled] forState:UIControlStateDisabled];
		btn.autoresizingMask = clearAssessmentButton.autoresizingMask;
		[btn addTarget:self action:@selector(clearButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
		if (section == 1) {
			[btn setTitle:@"Clear Bookmarks" forState:UIControlStateNormal];
			btn.tag = ClearSourceBookmarks;
			btn.enabled = ([Case anyObjectMatchingPredicate:PWF(@"isStarred = %@",[NSNumber numberWithBool:YES])] != nil);
		} else if (section == 2) {
			[btn setTitle:@"Clear Notes" forState:UIControlStateNormal];
			btn.tag = ClearSourceNotes;
			btn.enabled = ([Case anyObjectMatchingPredicate:PWF(@"Notes.@count>0")] != nil);
		}
		[footer addSubview:btn];
	}
	return footer;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"AssessmentCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (!cell) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		cell.textLabel.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
		cell.accessoryView = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 40)] autorelease];
		cell.accessoryView.backgroundColor = [UIColor clearColor];
		((UILabel *)cell.accessoryView).textAlignment = UITextAlignmentCenter;
	}
	UILabel *acc = (UILabel *)cell.accessoryView;
	acc.font = [UIFont boldSystemFontOfSize:[UIFont systemFontSize]];
	acc.textColor = RGB(3, 80, 255);
	
	if (indexPath.section == 0) {
		
		int c = [Case rowCountMatchingPredicate:PWF(@"questionState > 0")];
		if (c < 0) c = 0;
		if (indexPath.row == 0) {
			cell.textLabel.text = @"Questions unanswered";
			int a = [APP.book.allCasesWithQuestions count];
			acc.text = SWF(@"%i",a-c);
			acc.textColor = RGB(3, 80, 255);
		} else if (indexPath.row == 1) {
			cell.textLabel.text = @"Correct";
			int d = [Case rowCountMatchingPredicate:PWF(@"questionState = 2")];		
			acc.text = c==0?@"-":SWF(@"%i",d);
			acc.textColor = RGB(0, 208, 29);
		} else if (indexPath.row == 2) {
			cell.textLabel.text = @"Incorrect";
			int d = [Case rowCountMatchingPredicate:PWF(@"questionState = 1")];		
			acc.text = c==0?@"-":SWF(@"%i",d);	
			acc.textColor = RGB(254, 0, 0);
		}
	} else if (indexPath.section == 1) {
		cell.textLabel.text = @"Bookmarks";
		acc.text = SWF(@"%i",[Case rowCountMatchingPredicate:PWF(@"isStarred = %@",[NSNumber numberWithBool:YES])]);
	} else if (indexPath.section == 2) {
		cell.textLabel.text = @"Notes";
		acc.text = SWF(@"%i",[Case rowCountMatchingPredicate:PWF(@"Notes.@count>0")]);		
	}
	return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	PlaylistType type = PlaylistTypeNormal;
	NSString *title = nil;
	if (indexPath.section == ClearSourceQuestions) {
		if (indexPath.row == 0) {
			type = PlaylistTypeQuestionsUnanswered;
			title = @"Unanswered";
		} else if (indexPath.row == 1) {
			type = PlaylistTypeQuestionsCorrect;
			title = @"Correct";
		} else {
			type = PlaylistTypeQuestionsIncorrect;
			title = @"Incorrect";
		}
	} else if (indexPath.section == ClearSourceBookmarks) {
		type = PlaylistTypeBookmarks;
		title = @"Bookmarks";
	} else if (indexPath.section == ClearSourceNotes) {
		type = PlaylistTypeNotes;
		title = @"Notes";
	}
	Playlist *playlist = [[Playlist alloc] initWithTitle:title andItems:nil ofType:type];
	PlaylistViewController *pvc = ALOAD_VC(PlaylistViewController);
	pvc.playlist = playlist;
	pvc.navigationItem.title = title;
	RELEASE(playlist);
	
	[self.navigationController pushViewController:pvc animated:YES];
	
}
-(IBAction)clearAssessmentButtonPressed {
	[self clearButtonPressed:clearAssessmentButton];
	/*
	[[NSUserDefaults standardUserDefaults] removeObjectForKey:WRAP_KEY(@"assessmentStarted")];
	NSArray *cases = [Case arrayMatchingPredicate:PWF(@"questionState>0")];
	for (Case *c in cases) {
		c.questionState = 0;
	}
	[APP saveContext];
	[_tableView reloadData];
	 */
}
-(IBAction)clearButtonPressed:(UIButton *)sender {
	[self areYouSure:[sender tag]];
}
-(void)areYouSure:(ClearSource)m {
	NSString *title = @"Are you sure you want to ";
	NSString *destruct = nil;
	if (m == ClearSourceQuestions) {
		title = [title stringByAppendingString:@"clear all your answered questions?"];
		destruct = @"Yes - clear them";
	} else if (m == ClearSourceBookmarks) {
		title = [title stringByAppendingString:@"delete all your bookmarks?"];		
		destruct = @"Yes - delete them";
	} else if (m == ClearSourceNotes) {
		title = [title stringByAppendingString:@"delete all your notes?"];		
		destruct = @"Yes - delete them";
	}
	UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Cancel - keep them" destructiveButtonTitle:destruct otherButtonTitles:nil];
	as.tag = m;
	[as showInView:self.view];
	RELEASE(as);
}
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 0) {
		if (actionSheet.tag == ClearSourceQuestions) {
			[Flurry logEvent:@"OverviewClearedQuestions" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
			[[NSUserDefaults standardUserDefaults] removeObjectForKey:WRAP_KEY(@"assessmentStarted")];
			[[NSUserDefaults standardUserDefaults] synchronize];
			NSArray *cases = [Case arrayMatchingPredicate:PWF(@"questionState>0")];
			for (Case *c in cases) {
				c.questionState = 0;
			}
		} else if (actionSheet.tag == ClearSourceBookmarks) {
			[Flurry logEvent:@"OverviewClearedBookmarks" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
			NSArray *cases = [Case arrayMatchingPredicate:PWF(@"isStarred = %@",[NSNumber numberWithBool:YES])];
			for (Case *c in cases) {
				c.isStarredBool = NO;
			}
			[APP saveContext];
		} else if (actionSheet.tag == ClearSourceNotes) {
			[Flurry logEvent:@"OverviewClearedNotes" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
			NSArray *notes = [Note arrayMatchingPredicate:nil];
			for (Note *n in notes) {
				[n deleteFromCoreDataAndSave:NO];
			}
		}
		[APP saveContext];
		[_tableView reloadData];			
	}
}
@end
