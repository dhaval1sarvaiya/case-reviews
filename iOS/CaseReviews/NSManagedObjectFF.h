//
//  NSManagedObjectFF.h
//  Fitfu
//
//  Created by Benjie Gillam on 16/09/2010.
//  Copyright 2010 BrainBakery Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSManagedObject (BSG)

+ (NSArray *)arrayMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending limitedTo:(int)maxResults limitToLocalUserIfLoggedIn:(BOOL)localUserOnly;
+ (NSArray *)arrayMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending limitedTo:(int)maxResults;
+ (NSArray *)arrayMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending;
+ (NSArray *)arrayMatchingPredicate:(NSPredicate *)predicate;
+ (id)objectMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending limitToLocalUserIfLoggedIn:(BOOL)localUserOnly;
+ (id)objectMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending;
+ (id)objectMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey;
+ (id)anyObjectMatchingPredicate:(NSPredicate *)predicate;
+ (id)anyObjectMatchingPredicate:(NSPredicate *)predicate limitToLocalUserIfLoggedIn:(BOOL)localUserOnly;
+ (int)rowCountMatchingPredicate:(NSPredicate *)predicate;
+ (int)rowCountMatchingPredicate:(NSPredicate *)predicate limitToLocalUserIfLoggedIn:(BOOL)localUserOnly;
- (void)deleteFromCoreDataAndSave:(BOOL)save;
- (void)deleteFromCoreData;
+ (NSString *)generateUUID;
- (NSString *)generateUUID;

@end