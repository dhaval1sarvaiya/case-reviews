//
//  NewNoteViewController_iPhone.m
//  CaseReviews
//
//  Created by Benjie Gillam on 22/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "NewNoteViewController_iPhone.h"
#import "NotesViewController.h"

@implementation NewNoteViewController_iPhone
-(void)saveButtonPressed {
	[super saveButtonPressed];
	[self.notesViewController refreshNotes];
	[self.navigationController popViewControllerAnimated:YES];
}
- (void)cancelButtonPressed {
	[self.navigationController popViewControllerAnimated:YES];
}
@end
