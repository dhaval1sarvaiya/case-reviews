//
//  NSManagedObjectFF.m
//  Fitfu
//
//  Created by Benjie Gillam on 16/09/2010.
//  Copyright 2010 BrainBakery Ltd. All rights reserved.
//

#import "NSManagedObjectFF.h"
#import "AppDelegate.h"

@implementation NSManagedObject (BSG)

+ (NSArray *)arrayMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending limitedTo:(int)maxResults limitToLocalUserIfLoggedIn:(BOOL)localUserOnly {
	NSString *entityName = [self description];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:APP.managedObjectContext]];
	// Merge forced predicate on the next line
	[fetchRequest setPredicate:predicate];
	if (sortKey) {
		if ([sortKey isEqualToString:@"username"]) {
			[fetchRequest setSortDescriptors:[NSArray arrayWithObject:[[[NSSortDescriptor alloc] initWithKey:sortKey ascending:ascending selector:@selector(caseInsensitiveCompare:)] autorelease]]];
		} else {
			[fetchRequest setSortDescriptors:[NSArray arrayWithObject:[[[NSSortDescriptor alloc] initWithKey:sortKey ascending:ascending] autorelease]]];
		}
	}
	if (maxResults > 0) {
		[fetchRequest setFetchLimit:maxResults];
	}
	[fetchRequest setFetchBatchSize:20];
	[fetchRequest setReturnsObjectsAsFaults:(maxResults < 1 || maxResults > 20)];// Fault the data unless it's specified only 1-20 results.
	//#warning Next line potentially dangerous?
	//[fetchRequest setIncludesSubentities:NO];
	[fetchRequest setIncludesPendingChanges:YES];
	//[fetchRequest setIncludesPropertyValues:YES];
	NSError *error = nil;
	NSArray *objectArray = [[NSArray alloc] initWithArray:[APP.managedObjectContext executeFetchRequest:fetchRequest error:&error]];
	if (error) {
		NSLogOkay(@"WARNING: CoreData %@ fetch triggered error: %@", entityName, error);
	}
	RELEASE(fetchRequest);
	return [objectArray autorelease];	
}

+ (NSArray *)arrayMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending limitedTo:(int)maxResults {
	return [self arrayMatchingPredicate:predicate orderedBy:sortKey orderAscending:ascending limitedTo:maxResults limitToLocalUserIfLoggedIn:YES];
}

+ (NSArray *)arrayMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending {
	return [self arrayMatchingPredicate:predicate orderedBy:sortKey orderAscending:ascending limitedTo:-1];
}

+ (NSArray *)arrayMatchingPredicate:(NSPredicate *)predicate {
	return [self arrayMatchingPredicate:predicate orderedBy:nil orderAscending:YES limitedTo:-1];
}

+ (id)objectMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending limitToLocalUserIfLoggedIn:(BOOL)localUserOnly {
	NSArray *results = [self arrayMatchingPredicate:predicate orderedBy:sortKey orderAscending:ascending limitedTo:1 limitToLocalUserIfLoggedIn:localUserOnly];
	if ([results count] < 1) return nil;
	else return [results objectAtIndex:0];
}
+ (id)objectMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey orderAscending:(BOOL)ascending  {
	return [self objectMatchingPredicate:predicate orderedBy:sortKey orderAscending:ascending limitToLocalUserIfLoggedIn:YES];	
}
+ (id)objectMatchingPredicate:(NSPredicate *)predicate orderedBy:(NSString *)sortKey {
	return [self objectMatchingPredicate:predicate orderedBy:sortKey orderAscending:NO limitToLocalUserIfLoggedIn:YES];
}

+ (id)anyObjectMatchingPredicate:(NSPredicate *)predicate limitToLocalUserIfLoggedIn:(BOOL)localUserOnly {
	return [self objectMatchingPredicate:predicate orderedBy:nil orderAscending:NO limitToLocalUserIfLoggedIn:localUserOnly];
}
+ (id)anyObjectMatchingPredicate:(NSPredicate *)predicate {
	return [self objectMatchingPredicate:predicate orderedBy:nil orderAscending:NO limitToLocalUserIfLoggedIn:YES];
}

#pragma mark -

+ (int)rowCountMatchingPredicate:(NSPredicate *)predicate {
	return [self rowCountMatchingPredicate:predicate limitToLocalUserIfLoggedIn:YES];
}

+ (int)rowCountMatchingPredicate:(NSPredicate *)predicate limitToLocalUserIfLoggedIn:(BOOL)localUserOnly {
	NSString *entityName = [self description];
	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	[fetchRequest setEntity:[NSEntityDescription entityForName:entityName inManagedObjectContext:APP.managedObjectContext]];
	[fetchRequest setPredicate:predicate];
	[fetchRequest setIncludesPendingChanges:YES];
	NSError *error = nil;
	int fetchRequestCount = [APP.managedObjectContext countForFetchRequest:fetchRequest error:&error];
	if (error) {
		RELEASE(fetchRequest);
		NSLogOkay(@"WARNING: CoreData %@ fetch triggered error: %@", entityName, error);
		return 0;
	}
	RELEASE(fetchRequest);
	return fetchRequestCount;
}

#pragma mark -

+ (NSString *)generateUUID {
	CFUUIDRef uuid = CFUUIDCreate(NULL);
	CFStringRef uuidCfstr = CFUUIDCreateString(NULL,uuid);
	CFRelease(uuid);
	NSString *uuidStr = [(NSString *)uuidCfstr retain];
	CFRelease(uuidCfstr);
	return [uuidStr autorelease];
}

- (NSString *)generateUUID {
	return [[self class] generateUUID];
}

- (void)deleteFromCoreDataAndSave:(BOOL)save {
	[APP.managedObjectContext deleteObject:self];
	if (save) {
		[APP saveManagedObjectContext];
	}
}

- (void)deleteFromCoreData {
	[self deleteFromCoreDataAndSave:YES];
}

@end