//
//  AppDelegate.h
//  CaseReviews
//
//  Created by Benjie Gillam on 11/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGSplitViewController.h"
#import "NSManagedObjectFF.h"
#import "RegexKitLite.h"
#import "Flurry.h"
#import "Appirater.h"
#import "ShelfViewController.h"
#import "BBBook.h"

@class ShelfViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
    MGSplitViewController *splitViewController;
    UINavigationController *navigationController;
    UINavigationController *shelfNavigationController;
	ShelfViewController *shelfViewController;
}
@property (nonatomic, retain) BBBook *book;
@property (nonatomic, retain) IBOutlet UIWindow *window;

@property (nonatomic, retain, readonly) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, retain, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, retain, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property (nonatomic,retain) MGSplitViewController *splitViewController;
@property (nonatomic,retain) UINavigationController *navigationController;
@property (nonatomic,retain) UINavigationController *shelfNavigationController;
@property (nonatomic, retain) ShelfViewController *shelfViewController;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

-(void)appOpened;
-(void) loadBook: (NSString *)bookId;
-(id)loadVC:(Class)cls;
-(void)saveManagedObjectContext;
-(void)setDefaultHidden:(BOOL)hidden animated:(BOOL)animated;
-(UIImage *)imageNamed:(NSString *)name;
-(UIImage *)imageWithContentsOfFile:(NSString *)file;
-(void)openBook:(BBBook *)book fromRect:(CGRect)rect;
-(void)closeBook;
@end
