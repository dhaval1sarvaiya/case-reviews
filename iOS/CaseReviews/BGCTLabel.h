//
//  BGCTLabel.h
//  CaseReviews
//
//  Created by Benjie Gillam on 19/08/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreText/CoreText.h>

@interface BGCTLabel : UILabel {
	NSAttributedString *_formattedText;


    CTFramesetterRef    _framesetter; // Cached Core Text framesetter
    CTFrameRef          _frame; // Cached Core Text frame
}
@property (nonatomic, copy) NSAttributedString *formattedText;

-(NSAttributedString *)formatText:(NSString *)txt;
-(void)setAndFormatText:(NSString *)txt;
-(CGFloat)heightForWidth:(CGFloat)width;
@end
