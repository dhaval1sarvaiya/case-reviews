//
//  ContentsCell.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "ContentsCell.h"
#import "AppDelegate.h"

@implementation ContentsCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_bgimageView release];
    [_bglabel release];
	[super dealloc];
}
#pragma mark -
-(void)setEntry:(NSDictionary *)entry {
	NSString *imgNm =[entry objectForKey:@"image"];
	NSString *title = [entry objectForKey:@"title"];
	_bgimageView.image = imgNm?[APP imageNamed:SWF(@"%@",imgNm)]:nil;
	_bglabel.text = title;
}
@end
