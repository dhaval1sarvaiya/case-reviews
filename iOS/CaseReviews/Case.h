//
//  Case.h
//  CaseReviews
//
//  Created by Benjie Gillam on 21/07/2011.
//  Copyright (c) 2011 BrainBakery Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Note;

@interface Case : NSManagedObject {
@private
}
@property (nonatomic, retain) NSNumber * number;
@property (nonatomic, retain) NSNumber * isStarred;
@property (nonatomic, retain) NSNumber * questionState;
@property (nonatomic, retain) NSSet* Notes;

PROPERTY_ALIAS_INT_H(number);
PROPERTY_ALIAS_INT_H(questionState);
PROPERTY_ALIAS_BOOL_H(isStarred);

@end
