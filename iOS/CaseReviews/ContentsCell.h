//
//  ContentsCell.h
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ContentsCell : UITableViewCell {
	IBOutlet UIImageView *_bgimageView;
	IBOutlet UILabel *_bglabel;
}
-(void)setEntry:(NSDictionary *)entry;
@end
