//
//  ImageViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "ImageViewController.h"
#import "QuestionViewController_iPad.h"
#import "QuestionViewController_iPhone.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "Note.h"

@implementation ImageViewController
@synthesize popoverController;
@synthesize playlist;
@synthesize openNotesOnOpen;
-(UIFont *)diagnosisTextViewFont {
	return [UIFont boldSystemFontOfSize:17];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		[Flurry logEvent:@"ViewImage" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
    }
    return self;
}

- (void)dealloc {
	RELEASE(diagnosisTextViewFont);
	self.playlist = nil;
	self.templateNavItem = nil;
	RELEASE(topBar);//auto
	RELEASE(bottomBar);//auto
	RELEASE(navItem);//auto
	RELEASE(legendContainer);//auto
	RELEASE(legendImageNumber);//auto
	RELEASE(legendLegendLabel);//auto
	RELEASE(legendTextLabel);//auto
	RELEASE(_scrollView);//auto
	RELEASE(prevButton);//auto
	RELEASE(nextButton);//auto
	RELEASE(questionButton);//auto
	RELEASE(shuffleButton);//auto
	RELEASE(labelsButton);//auto
	RELEASE(notesButton);//auto
	RELEASE(diagnosisButton);//auto
	RELEASE(loopImage);//auto
	RELEASE(starButton);//auto
	RELEASE(diagnosisContainer);//auto
	RELEASE(diagnosisTextView);//auto
	RELEASE(bottomBarAllItems);//auto
	RELEASE(grid);//auto
	RELEASE(zoomScrollView);//auto
	RELEASE(zoomedImageContainer);//auto
	RELEASE(zoomedImageDetails);//auto
	self.popoverController = nil;//auto
	RELEASE(diagnosisDismisser);//auto
	RELEASE(diagnosisGR);//auto
	[super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidUnload {
	[self retain];//auto
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	self.templateNavItem = nil;
	RELEASE(topBar);//auto
	RELEASE(bottomBar);//auto
	RELEASE(legendContainer);//auto
	RELEASE(legendImageNumber);//auto
	RELEASE(legendLegendLabel);//auto
	RELEASE(legendTextLabel);//auto
	RELEASE(_scrollView);//auto
	RELEASE(prevButton);//auto
	RELEASE(nextButton);//auto
	RELEASE(questionButton);//auto
	RELEASE(shuffleButton);//auto
	RELEASE(labelsButton);//auto
	RELEASE(notesButton);//auto
	RELEASE(diagnosisButton);//auto
	RELEASE(loopImage);//auto
	RELEASE(starButton);//auto
	RELEASE(diagnosisContainer);//auto
	RELEASE(diagnosisTextView);//auto
	[self autorelease];//auto
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    if (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown) {
		return YES;
	}
	return NO;
}

-(void)viewDidLoad {
	[super viewDidLoad];
	navItem = [self.navigationItem retain]; // MUST come first.
	navItem.title = self.templateNavItem.title;
	navItem.leftBarButtonItem = self.templateNavItem.leftBarButtonItem;
	navItem.rightBarButtonItem = self.templateNavItem.rightBarButtonItem;
	if (!diagnosisTextViewFont) {
		RELEASE(diagnosisTextViewFont);
		diagnosisTextViewFont = [[self diagnosisTextViewFont] retain];
		diagnosisTextView.font = diagnosisTextViewFont;
	}
	self.view.backgroundColor = [UIColor colorWithPatternImage:[APP imageNamed:@"texture"]];
	
	UIInterfaceOrientation cIO = [[UIApplication sharedApplication] statusBarOrientation];
	if (UIInterfaceOrientationIsPortrait(cIO)) {
		UIInterfaceOrientation prev = UIInterfaceOrientationLandscapeLeft;
		[self willRotateToInterfaceOrientation:cIO duration:0];
		[self willAnimateRotationToInterfaceOrientation:cIO duration:0];
		CGRect frame = APP.window.bounds;
		frame.size.height -= 20;
		self.view.frame = frame;
		[self didRotateFromInterfaceOrientation:prev];
	}

	RELEASE(grid);
	grid = [[NSMutableDictionary alloc] init];
	
	legendContainerOriginalHeight = legendContainerBaseHeight = legendContainer.frame.size.height;
	
	legendTextLabelTopPad = legendTextLabel.frame.origin.y;
	legendTextLabelBottomPad = legendContainer.frame.size.height - legendTextLabelTopPad - legendTextLabel.frame.size.height;
	
	[self scrollToCurrent];
	if ([self.playlist position] == 0) {
		displayedPosition = 1;
		[self scrollViewDidScroll:_scrollView];
	}
	
	[self reset];
	[self displayCurrent];
}
-(void)viewDidAppear:(BOOL)animated {
	if (openNotesOnOpen) {
		[self notesButtonPressed];
		openNotesOnOpen = NO;
	}
	[super viewDidAppear:animated];//auto
}


-(IBAction)close {
	[self dismissModalViewControllerAnimated:YES];
}
-(IBAction)toggleLegend {
	if (legendExpanded) {
		legendLegendLabel.hidden = NO;
		legendTextLabel.hidden = YES;
		CGRect frame = legendContainer.frame;
		frame.size.height = legendContainerOriginalHeight;
		legendContainer.frame = frame;
	} else {
		legendLegendLabel.hidden = YES;
		legendTextLabel.hidden = NO;

		CGFloat textHeight = [legendTextLabel heightForWidth:legendTextLabel.bounds.size.width];

		CGRect frame = legendContainer.frame;
		frame.size.height = legendContainerBaseHeight + textHeight + 5; // < RICHARD the 5 is the bonus padding shared above and below the text label. iPhone AND iPad
		legendContainer.frame = frame;
		
		frame = legendTextLabel.frame;
		frame.origin.y = legendTextLabelTopPad;
		frame.size.height = textHeight+5;
		legendTextLabel.frame = frame;
		
		
		
	}
	legendExpanded = !legendExpanded;
}
-(void)updateShuffleButton {
	//shuffleButton.title = self.playlist.isShuffled?@"Ord":@"Rnd";
	shuffleButton.image = [APP imageNamed:self.playlist.isShuffled?@"shuffleOn":@"shuffleOff"];
	prevButton.enabled = [self.playlist hasPrevious];
	nextButton.enabled = [self.playlist hasNext];
	//TODO: Explicitly set next/prev button images.
	if (!nextButton.enabled && self.playlist.isShuffled) {
		nextButton.enabled = YES;
		//TODO: Set next button image to rewind one
	}
	if (!prevButton.enabled && self.playlist.isShuffled) {
		prevButton.enabled = YES;
		//TODO: Set next button image to ffwd one
	}
}
-(Case *)currentCase {
	Case *current = [Case anyObjectMatchingPredicate:PWF(@"number = %i",[[[self.playlist currentItem] objectForKey:@"case"] intValue])];
	return current;
}
-(IBAction)starButtonPressed {
	[Flurry logEvent:@"StarButtonPressed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	Case *c = [self currentCase];
	if (!c) {
		c = COREDATA_CREATE(Case);
		c.numberInt = [[[self.playlist currentItem] objectForKey:@"case"] intValue];
		[APP saveContext];
	}
	c.isStarredBool = !c.isStarredBool;
	[self displayCurrent];
}
-(void)displayCurrent {
	if (APP.window.userInteractionEnabled == NO) return;
	displayedPosition = [self.playlist position];
	displayedImage = imageIndex;

	navItem.title = SWF(@"Case %@",[self.playlist.currentItem objectForKey:@"case"]);
	NSLog(@"Displaying: %@",navItem.title);
	legendImageNumber.text = SWF(@"Image %i: ",imageIndex+1);
	CGRect frame = legendLegendLabel.frame;
	frame.origin.x = legendImageNumber.frame.origin.x + [legendImageNumber sizeThatFits:CGSizeMake(1000, 1000)].width;
	legendLegendLabel.frame = frame;
	//legendTextLabel.text = [[[self.playlist.currentItem objectForKey:@"images"] objectAtIndex:imageIndex] objectForKey:@"legend"];
	
	
	
	NSString *txt = [[[self.playlist.currentItem objectForKey:@"images"] objectAtIndex:imageIndex] objectForKey:@"legend"];
	
	[legendTextLabel setAndFormatText:txt];	
	
	
	if (legendExpanded) {
		legendExpanded = NO;
		[self toggleLegend];
	}
	
	NSArray *answers = [self.playlist.currentItem objectForKey:@"answers"];
	questionButton.enabled = answers && [answers count]>0;
	
	NSString *rel = @"Q";
	int i = [self currentCase].questionStateInt;
	if (i == 1) {
		rel = @"Q-incorrect";
	} else if (i == 2) {
		rel = @"Q-correct";
	}
	[questionButton setImage:[APP imageNamed:rel] forState:UIControlStateNormal];
	
	[self updateShuffleButton];
	
	[self refreshNotes];
	
	[starButton setImage:[APP imageNamed:@"star"] forState:UIControlStateNormal];
	Case *currentCase = [self currentCase];
	if (currentCase) {
		if (currentCase.isStarredBool) {
			[starButton setImage:[APP imageNamed:@"starOn"] forState:UIControlStateNormal];		
		}
	}
	
	diagnosisTextView.text = [self.playlist.currentItem objectForKey:@"diagnosis"];
	CGSize size = [diagnosisTextView.text sizeWithFont:diagnosisTextViewFont constrainedToSize:CGSizeMake(diagnosisTextView.frame.size.width-16, 999999) lineBreakMode:UILineBreakModeWordWrap];
	//size.width+=2*diagnosisTextView.frame.origin.x;
	diagnosisTextView.userInteractionEnabled = size.height > diagnosisTextView.frame.size.width-20;
	size.height = MIN(diagnosisTextView.frame.size.width,size.height);
	size.height += diagnosisContainer.frame.size.height - diagnosisTextView.frame.size.height+16;//16px is hack
	frame = diagnosisContainer.frame;
	frame.size.height = size.height;
	frame.origin.x = floor((self.view.bounds.size.width - frame.size.width)/2);
	frame.origin.y = floor((self.view.bounds.size.height - frame.size.height)/2);
	diagnosisContainer.frame = frame;
	
	[self updateLabelsButton];
}
-(void)reset {
	if (legendExpanded) {
		[self toggleLegend];
	}	
	imageIndex = 0;
	displayedImage = 0;
	
	/*
	NSArray *images = [self.playlist.currentItem objectForKey:@"images"];

	CGSize size = _scrollView.frame.size;
	UIScrollView *upDownScroll = [[UIScrollView alloc] initWithFrame:(CGRect){{0,0},size}];
	[upDownScroll setContentSize:CGSizeMake(size.width,size.height * [images count])];
	[upDownScroll setPagingEnabled:YES];
	 */
}
-(void)setLoopImageVisible:(BOOL)visible {
	loopImage.hidden = NO;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:visible?0.1:0.75];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(hideLoopImage)];
	loopImage.alpha = visible?1:0;
	[UIView commitAnimations];
}
-(void)showLoopImage:(NSString *)imageName {
	loopImage.image = [APP imageNamed:imageName];
	[self setLoopImageVisible:YES];
}
-(void)hideLoopImage {
	if (loopImage.alpha > 0.5) {
		[self setLoopImageVisible:NO];
	} else {
		loopImage.alpha = 0;
		loopImage.hidden = YES;
	}
}
-(IBAction)prev {
	[self reset];
	BOOL animated = NO;
	if (![self.playlist hasPrevious]) {
		[self.playlist setPosition:[self.playlist count]-1];
		[self showLoopImage:@"loopL"];
		animated = NO;
	} else {
		[self.playlist previousItem];
	}
	[_scrollView scrollRectToVisible:CGRectMake(_scrollView.frame.size.width*[self.playlist position], 0,_scrollView.frame.size.width,_scrollView.frame.size.height) animated:animated];
	[self displayCurrent];
}
-(IBAction)next {
	[self reset];
	BOOL animated = NO;
	if (![self.playlist hasNext]) {
		[self.playlist setPosition:0];
		[self showLoopImage:@"loopR"];
		animated = NO;
	} else {
		[self.playlist nextItem];
	}
	[_scrollView scrollRectToVisible:CGRectMake(_scrollView.frame.size.width*[self.playlist position], 0,_scrollView.frame.size.width,_scrollView.frame.size.height) animated:animated];
	[self displayCurrent];
}
-(IBAction)questionButtonPressed {
	[Flurry logEvent:@"QuestionButtonPressed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	QuestionViewController *qvc = ALOAD_VC(QuestionViewController);
	qvc.imageViewController = self;
	qvc.item = [self.playlist currentItem];
	UINavigationController *navVC = [[[UINavigationController alloc] initWithRootViewController:qvc] autorelease];
	navVC.navigationBar.barStyle = UIBarStyleBlack;
	navVC.modalPresentationStyle = UIModalPresentationFormSheet; //Ignored by iPhone/iPod
	[self presentModalViewController:navVC animated:YES];
}
-(IBAction)shuffleButtonPressed {
	[Flurry logEvent:@"ShuffleButtonPressed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	//[self reset];
	[self.playlist toggleShuffle];
	[self scrollToCurrent];
	[self updateShuffleButton];
	//[self displayCurrent];
}
-(IBAction)notesButtonPressed {
	[Flurry logEvent:@"NotesButtonPressed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
}
-(void)updateLabelsButton {
	NSDictionary *item = [self.playlist itemAtIndex:displayedPosition];
	NSDictionary *image = [[item objectForKey:@"images"] objectAtIndex:displayedImage];
	NSString *path = [APP.book pathForResource:SWF(@"%@-%@.label",[item objectForKey:@"case"],[image objectForKey:@"image"]) ofType:@"png" inDirectory:@"Images"];
	NSString *imgName = labelsButton.tag == 0?@"labelOff":@"labelOn";
	if (!path) {
		imgName = labelsButton.tag == 0?@"labelOffNo":@"labelOnNo";
	}
	labelsButton.image = [APP imageNamed:imgName];	
}
-(IBAction)labelsButtonPressed {
	[Flurry logEvent:@"LabelsButtonPressed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	labelsButton.tag = labelsButton.tag == 0?1:0;
	[self updateLabelsButton];
	for (NSNumber *key in grid) {
		NSDictionary *e = [grid objectForKey:key];
		for (NSNumber *key2 in e) {
			if ([key2 isKindOfClass:[NSNumber class]]) {
				NSDictionary *e2 = [e objectForKey:key2];
				if ([e2 objectForKey:@"labelImageView"]) {
					[(UIView *)[e2 objectForKey:@"labelImageView"] setHidden:(labelsButton.tag == 0)];
				}
			}
		}
	}
}
-(IBAction)diagnosisButtonPressed {
	[Flurry logEvent:@"DiagnosisButtonPressed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	diagnosisContainer.hidden = !diagnosisContainer.hidden;

	/*
	[diagnosisDismisser removeFromSuperview];
	RELEASE(diagnosisDismisser);
	 */
	
	if (diagnosisGR) {
		[_scrollView removeGestureRecognizer:diagnosisGR];
		RELEASE(diagnosisGR);
	}
	
	if (!diagnosisContainer.hidden) {
		diagnosisGR = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(diagnosisButtonPressed)];
		[_scrollView addGestureRecognizer:diagnosisGR];
		
		/*
		CGRect frame = self.view.bounds;
		frame.size.height -= bottomBar.frame.size.height;
		diagnosisDismisser = [[UIControl alloc] initWithFrame:frame];
		diagnosisDismisser.backgroundColor = [UIColor colorWithWhite:0 alpha:0.2];
		[diagnosisDismisser addTarget:self action:@selector(diagnosisButtonPressed) forControlEvents:UIControlEventTouchUpInside];
		[self.view addSubview:diagnosisDismisser];
		[self.view bringSubviewToFront:diagnosisContainer];
		 */
	}
	diagnosisButton.image = [APP imageNamed:diagnosisContainer.hidden?@"DxOff":@"Dx"];
}
#pragma mark - Scroll view stuff
-(void)loadUpDownScrollView:(int)p {
	[self loadUpDownScrollView:p atIndex:0];
}
#pragma mark Gesture recognizers.
-(void)imageTapped:(UIGestureRecognizer *)sender {
	if (!diagnosisContainer.hidden) {
		[self diagnosisButtonPressed];
		return;
	}
	if (sender.state != UIGestureRecognizerStateEnded) {
		return;
	}
	RELEASE(zoomedImageContainer);
	RELEASE(zoomScrollView);
	[Flurry logEvent:@"ImageZoomed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	
	
	NSLog(@"Image tapped: %i:%i",sender.view.superview.tag,sender.view.tag);
	UIImageView *imageView = (UIImageView *)sender.view;
	if (!imageView) {
		NSLog(@"ERROR: No image view?!");
		return;
	}
	CGRect rect = [self absolutePositionFromImageView:imageView];
	zoomedImageDetails = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInt:sender.view.superview.tag],@"position",[NSNumber numberWithInt:sender.view.tag],@"image", nil];
	
	UIView *container = [[UIView alloc] initWithFrame:rect];
	zoomedImageContainer = [container retain];
	container.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	container.backgroundColor = [UIColor blackColor];
	rect.origin = CGPointZero;
	
	UIImageView *iv = [[UIImageView alloc] initWithFrame:rect];
	iv.image = imageView.image;
	iv.contentMode = UIViewContentModeScaleAspectFit;
	iv.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[container addSubview:iv];
	RELEASE(iv);
	
	if (labelsButton.tag == 1) {
		iv = [[UIImageView alloc] initWithFrame:rect];
#warning LAZY CODING ON NEXT LINE, CAREFUL IF ANYTHING CHANGES!
		iv.image = ((UIImageView *)[[imageView.superview subviews] objectAtIndex:1]).image;
		iv.contentMode = UIViewContentModeScaleAspectFit;
		iv.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		[container addSubview:iv];
		RELEASE(iv);		
	}
	[self.view addSubview:container];
	APP.window.userInteractionEnabled = NO;
	[NSTimer scheduledTimerWithTimeInterval:0.00001 target:self selector:@selector(showZoomedImage) userInfo:nil repeats:NO];
}
-(void)hidZoomedImage {
	[zoomedImageContainer removeFromSuperview];
	RELEASE(zoomedImageContainer);
	APP.window.userInteractionEnabled = YES;
}
-(void)hideZoomedImage {
	[UIView beginAnimations:@"unzoom" context:NULL];
	[UIView setAnimationDuration:0.4];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(hidZoomedImage)];
	
	UIImageView *imageView = [[[grid objectForKey:[zoomedImageDetails objectForKey:@"position"]] objectForKey:[zoomedImageDetails objectForKey:@"image"]] objectForKey:@"imageView"];
	zoomedImageContainer.frame = [self absolutePositionFromImageView:imageView];
	RELEASE(zoomedImageDetails);
	[UIView commitAnimations];
}
-(void)closeZoomedImage {
	CGRect frame = zoomedImageContainer.frame;
	frame.origin.x -= zoomScrollView.contentOffset.x;
	frame.origin.y -= zoomScrollView.contentOffset.y;
	[zoomScrollView removeFromSuperview];
	RELEASE(zoomScrollView);
	if (frame.origin.y > 0) {
		frame.size.height += 2*frame.origin.y;
		frame.origin.y = 0;
	}
	if (frame.origin.x > 0) {
		frame.size.width += 2*frame.origin.x;
		frame.origin.x = 0;
	}
	CGFloat diff = self.view.bounds.size.height - frame.size.height;
	if (diff > 0) {
		frame.size.height += 2*diff;
		frame.origin.y -= diff;
	}
	diff = self.view.bounds.size.width - frame.size.width;
	if (diff > 0) {
		frame.size.width += 2*diff;
		frame.origin.x -= diff;
	}
	
	zoomedImageContainer.frame = frame;
	[self.view addSubview:zoomedImageContainer];
	APP.window.userInteractionEnabled = NO;
	[NSTimer scheduledTimerWithTimeInterval:0.00001 target:self selector:@selector(hideZoomedImage) userInfo:nil repeats:NO];
}
-(void)showedZoomedImage {
	APP.window.userInteractionEnabled = YES;
	RELEASE(zoomScrollView);
	zoomScrollView = [[UIScrollView alloc] initWithFrame:zoomedImageContainer.frame];
	zoomScrollView.delegate = self;
	zoomScrollView.contentSize = zoomedImageContainer.frame.size;
	
	UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeZoomedImage)];
	[zoomScrollView addGestureRecognizer:gr];
	RELEASE(gr);
	
	zoomScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	zoomScrollView.backgroundColor = [UIColor blackColor];
	[zoomScrollView setMinimumZoomScale:1];
	[zoomScrollView setMaximumZoomScale:2];
	[zoomScrollView addSubview:zoomedImageContainer];
	[self.view addSubview:zoomScrollView];
	
}
-(void)showZoomedImage {
	[UIView beginAnimations:@"zoom" context:NULL];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(showedZoomedImage)];
	[UIView setAnimationDuration:0.4];
	zoomedImageContainer.frame = (CGRect){CGPointZero,self.view.bounds.size};
	[UIView commitAnimations];
}
-(void)tappedSidebarImage:(UITapGestureRecognizer *)sender {
	if (sender.state != UIGestureRecognizerStateEnded) {
		return;
	}
	NSLog(@"Tapped sidebar: %i:%i",sender.view.superview.tag,sender.view.tag);
	UIScrollView *scrollView = [[grid objectForKey:[NSNumber numberWithInt:sender.view.superview.tag]] objectForKey:@"scrollView"];
	[scrollView scrollRectToVisible:CGRectMake(0, sender.view.tag*scrollView.frame.size.height, scrollView.contentSize.width, scrollView.frame.size.height) animated:NO];
	
}
-(CGRect)absolutePositionFromImageView:(UIImageView *)iv {
	CGRect rect = [self rectFromImageView:iv];
	UIView *v = iv;
	do {
		if (v == self.view) break;
		rect.origin.x += v.frame.origin.x;
		rect.origin.y += v.frame.origin.y;
		if ([v isKindOfClass:[UIScrollView class]]) {
			UIScrollView *sv = ((UIScrollView *)v);
			rect.origin.x -= sv.contentOffset.x;
			rect.origin.y -= sv.contentOffset.y;
		}
	} while ((v = [v superview]) != nil);
	return rect;
}
-(CGRect)rectFromImageView:(UIImageView *)iv {
	CGRect rect = (CGRect){{0,0},iv.frame.size};
	if (iv.image.size.width/iv.frame.size.width > iv.image.size.height/iv.frame.size.height) {
		//rect.origin.x == 0
		CGFloat sf = iv.frame.size.width/iv.image.size.width;
		rect.size.height = sf*iv.image.size.height;
		rect.origin.y = floor((iv.frame.size.height - rect.size.height)/2);
	} else {
		//rect.origin.y == 0;
		CGFloat sf = iv.frame.size.height/iv.image.size.height;
		rect.size.width = sf*iv.image.size.width;
		rect.origin.x = floor((iv.frame.size.width - rect.size.width)/2);		
	}
	return rect;
}
#pragma mark Specific image
-(void)loadPosition:(int)p image:(int)im {
	if (p < 0) return;
	if (im < 0) return;
	if (p >= [self.playlist count]) return;
	NSDictionary *item = [self.playlist itemAtIndex:p];
	if (im >= [[item objectForKey:@"count"] intValue]) return;

	NSMutableDictionary *entry = [[grid objectForKey:[NSNumber numberWithInt:p]] objectForKey:[NSNumber numberWithInt:im]];
	if (!entry) {
		entry = [NSMutableDictionary dictionary];
		[[grid objectForKey:[NSNumber numberWithInt:p]] setObject:entry forKey:[NSNumber numberWithInt:im]];
	}
	if ([entry objectForKey:@"container"]) return; //Already loaded.
	
	NSDictionary *image = [[item objectForKey:@"images"] objectAtIndex:im];
	

	NSLog(@"Load p: %i, image: %i",p,im);
	UIScrollView *sv = [[grid objectForKey:[NSNumber numberWithInt:p]] objectForKey:@"scrollView"];

	UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, 0+im*sv.frame.size.height, sv.contentSize.width, sv.frame.size.height)];
	[container setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
	container.tag = p;
	
	CGFloat pad = 20;
	UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(pad, pad, sv.contentSize.width-2*pad, sv.frame.size.height-2*pad)];
	UIImageView *labeliv = [[UIImageView alloc] initWithFrame:CGRectMake(pad, pad, sv.contentSize.width-2*pad, sv.frame.size.height-2*pad)];
	iv.userInteractionEnabled = YES;
	
	UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
	[gr setNumberOfTapsRequired:1];
	[iv addGestureRecognizer:gr];
	RELEASE(gr);
	
	UIPinchGestureRecognizer *pgr = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(imageTapped:)];
	[iv addGestureRecognizer:pgr];
	RELEASE(pgr);
	
	NSString *path = [APP.book pathForResource:SWF(@"%@-%@",[item objectForKey:@"case"],[image objectForKey:@"image"]) ofType:@"jpg" inDirectory:APP.window.userInteractionEnabled?@"Images":@"Thumbs"];
	iv.tag = im;
	[iv setImage:[APP.book imageWithContentsOfFile:path]];
	if (APP.window.userInteractionEnabled) {
		path = [APP.book pathForResource:SWF(@"%@-%@.label",[item objectForKey:@"case"],[image objectForKey:@"image"]) ofType:@"png" inDirectory:@"Images"];
		[labeliv setImage:[APP.book imageWithContentsOfFile:path]];
		iv.layer.masksToBounds = NO;
		iv.layer.shadowOffset = CGSizeMake(0/*sv.frame.size.width/128*/, sv.frame.size.width/128);
		iv.layer.shadowRadius = MIN(_scrollView.frame.size.width,_scrollView.frame.size.height)/(IPAD_IDIOM?200:100);
		iv.layer.shadowOpacity = kImageViewerShadowOpacity;
		
		CGRect rect = [self rectFromImageView:iv];
		iv.layer.shadowPath = [UIBezierPath bezierPathWithRect:rect].CGPath;
	}
	[iv setContentMode:UIViewContentModeScaleAspectFit];
	[iv setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
	[container addSubview:iv];
	[entry setObject:iv forKey:@"imageView"];
	RELEASE(iv);
	[labeliv setContentMode:UIViewContentModeScaleAspectFit];
	[labeliv setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
	[container addSubview:labeliv];
	labeliv.hidden = labelsButton.tag == 0;
	[entry setObject:labeliv forKey:@"labelImageView"];
	RELEASE(labeliv);
	
	[sv addSubview:container];
	[entry setObject:container forKey:@"container"];
	RELEASE(container);
}
-(void)position:(int)p unloadContainersAbove:(int)t orBelow:(int)b {
	NSMutableDictionary *pentry = [grid objectForKey:[NSNumber numberWithInt:p]];
	NSMutableArray *keys = [NSMutableArray array];
	for (id nr in pentry) {
		if ([nr isKindOfClass:[NSNumber class]]) {
			int im = [nr intValue];
			if (im < t || im > b) {
				[keys addObject:nr];
				[[[pentry objectForKey:nr] objectForKey:@"container"] removeFromSuperview];
			}
		}
	}
	[pentry removeObjectsForKeys:keys];
}
#pragma mark Up/down scroll view and thumbs
-(void)loadUpDownScrollView:(int)p atIndex:(int)im {
	if ([[grid objectForKey:[NSNumber numberWithInt:p]] objectForKey:@"scrollView"]) return; //Already loaded.
	if (p < 0) return;
	if (p >= [self.playlist count]) return;
	NSDictionary *pentry = [self.playlist itemAtIndex:p];

	UIView *container = [[UIView alloc] initWithFrame:CGRectMake(p*_scrollView.frame.size.width, 0, _scrollView.frame.size.width, _scrollView.frame.size.height)];
	container.tag = p;
	container.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	
	CGFloat sidebarWidth = MIN(APP.window.frame.size.width,APP.window.frame.size.height)/8;
	
	CGFloat sbp = sidebarWidth/10; //Sidebar padding;
	UIScrollView *sidebar = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, sidebarWidth, container.frame.size.height)];
	sidebar.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleRightMargin;
	sidebar.tag = p;
	sidebar.scrollEnabled = NO;
	sidebar.clipsToBounds = NO;
	sidebar.userInteractionEnabled = YES;
	
	UIView *sidebarIndicator = [[UIView alloc] initWithFrame:CGRectMake(sbp/2, sbp/2 + im*sidebarWidth, sidebarWidth, sidebarWidth)];
	sidebarIndicator.backgroundColor = [UIColor colorWithWhite:1 alpha:0.5];
	if (APP.window.userInteractionEnabled) {
		[sidebar addSubview:sidebarIndicator];
		
		NSArray *images = [pentry objectForKey:@"images"];
		sidebar.contentSize = CGSizeMake(sidebarWidth,[images count] * sidebarWidth+sbp);
		int i = 0;
		for (NSDictionary *d in images) {
			UIImageView *iv = [[UIImageView alloc] initWithImage:[APP.book imageWithContentsOfFile:[APP.book pathForResource:SWF(@"%@-%@",[pentry objectForKey:@"case"],[d objectForKey:@"image"]) ofType:@"jpg" inDirectory:@"Thumbs"]]];
			iv.userInteractionEnabled = YES;
			iv.tag = i;
			
			UITapGestureRecognizer *gr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedSidebarImage:)];
			gr.numberOfTapsRequired = 1;
			[iv addGestureRecognizer:gr];
			
			RELEASE(gr);
			iv.frame = CGRectMake(sbp, sbp+(sidebarWidth*i), sidebar.frame.size.width-sbp, sidebar.frame.size.width-sbp);
			[sidebar addSubview:iv];
			RELEASE(iv);
			i++;
		}
		[container addSubview:sidebar];
	}
	BOOL horizontal = kImageViewerRightPadInLandscape && UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
	CGRect frame = CGRectMake(sidebarWidth, 0, _scrollView.frame.size.width-sidebarWidth, _scrollView.frame.size.height);
	/*
	UILabel *sv = [[UILabel alloc] initWithFrame:frame];
	sv.font = [UIFont boldSystemFontOfSize:64];
	sv.text = SWF(@"%@",[[self.playlist itemAtIndex:p] objectForKey:@"case"]);
	sv.textAlignment = UITextAlignmentCenter;
	sv.backgroundColor = [UIColor colorWithWhite:0.5 alpha:1];
	 */
	UIScrollView *sv = [[UIScrollView alloc] initWithFrame:frame];
	sv.delegate = self;
	sv.pagingEnabled = YES;
	sv.showsHorizontalScrollIndicator = NO;
	sv.contentSize = CGSizeMake(sv.frame.size.width-(horizontal?sidebarWidth:0), _scrollView.frame.size.height*[[pentry objectForKey:@"count"] intValue]);
	if (im > 0) {
		sv.contentOffset = CGPointMake(0, _scrollView.frame.size.height*im);
	}

	
	sv.tag = p;
	sv.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	[container addSubview:sv];
	[_scrollView addSubview:container];
	[grid setObject:[NSMutableDictionary dictionaryWithObjectsAndKeys:sv,@"scrollView",container,@"container",sidebar,@"sidebar",sidebarIndicator,@"sidebarIndicator", nil] forKey:[NSNumber numberWithInt:p]];
	RELEASE(sv);
	RELEASE(container);
	RELEASE(sidebar);
	RELEASE(sidebarIndicator);

	[self loadPosition:p image:im];
}
-(void)unloadScrollViewsLeftOf:(int)l orRightOf:(int)r {
	NSMutableArray *toRemove = [[NSMutableArray alloc] init];
	for (NSNumber *key in grid) {
		if ([key intValue] < l || [key intValue] > r) {
			[toRemove addObject:key];
			[[[grid objectForKey:key] objectForKey:@"container"] removeFromSuperview];
			//TODO: Do all the other removal stuff!
		}
	}
	[grid removeObjectsForKeys:toRemove];
	RELEASE(toRemove);
}
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
	if (scrollView == _scrollView) {
		manual = YES;
	}
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
	if (scrollView == _scrollView) {
		if (!decelerate) manual = NO;
	}
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	if (scrollView == _scrollView) {
		manual = NO;
	}
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
	/*
	if (scrollView == _scrollView) {
		CGPoint contentOffset = scrollView.contentOffset;
		if (contentOffset.x >= _scrollView.frame.size.width * [self.playlist count]) {
			contentOffset.x -= _scrollView.frame.size.width * [self.playlist count];
			scrollView.contentOffset = contentOffset;
			
		}
	}
	 */
}
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
	if (rotating) return;
	CGPoint contentOffset = scrollView.contentOffset;
	if (scrollView == _scrollView) {
		if (ignore) return;
		CGFloat page = contentOffset.x/scrollView.frame.size.width;
		int l = floor(page);
		int r = ceil(page);
		[self loadUpDownScrollView:l];
		[self loadUpDownScrollView:r];
		[self unloadScrollViewsLeftOf:l orRightOf:r];
		int p = round(page);
		if (p < 0) p = 0;
		if (p >= [self.playlist count]) p = [self.playlist count] - 1;
		if (manual && displayedPosition != p) {
			[self reset];
			[self.playlist setPosition:p];
			[self displayCurrent];
		}
	} else if ([[grid objectForKey:[NSNumber numberWithInt:scrollView.tag]] objectForKey:@"scrollView"] == scrollView) {
		CGFloat page = contentOffset.y/scrollView.frame.size.height;
		
		UIView *sbi = [[grid objectForKey:[NSNumber numberWithInt:scrollView.tag]] objectForKey:@"sidebarIndicator"];
		CGRect frame = sbi.frame;
		frame.origin.y = page*(frame.size.width)+sbi.frame.origin.x;
		sbi.frame = frame;
		
		UIScrollView *sidebar = [[grid objectForKey:[NSNumber numberWithInt:scrollView.tag]] objectForKey:@"sidebar"];
		CGFloat percentage = page / ([[[self.playlist itemAtIndex:scrollView.tag] objectForKey:@"count"] intValue]-1);
		sidebar.contentOffset = CGPointMake(0, 0+MAX(0,(sidebar.contentSize.height - _scrollView.frame.size.height)) * percentage);
		//frame.size.width = sidebar.frame.size.width;
		//[sidebar scrollRectToVisible:frame animated:NO];
		
		
		int t = floor(page);
		int b = ceil(page);
		[self loadPosition:scrollView.tag image:t];
		[self loadPosition:scrollView.tag image:b];
		[self position:scrollView.tag unloadContainersAbove:t orBelow:b];
		if (displayedPosition == scrollView.tag) {
			int p = round(page);
			if (p < 0) p = 0;
			if (p != displayedImage) {
				if (p >= [[[self.playlist itemAtIndex:scrollView.tag] objectForKey:@"count"] intValue]) {
					p = [[[self.playlist itemAtIndex:scrollView.tag] objectForKey:@"count"] intValue]-1;
				}
				if (p != displayedImage) {
					[[grid objectForKey:[NSNumber numberWithInt:scrollView.tag]] setObject:[NSNumber numberWithInt:p] forKey:@"imageIndex"];
					imageIndex = p;
					[self displayCurrent];
				}
			}
		}
	}
}
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
	if (scrollView == zoomScrollView) {
		return zoomedImageContainer;
	}
	return nil;
}
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
	
}
-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	for (NSNumber *key in grid) {
		int p = [key intValue];
		UIScrollView *scrollView = [[grid objectForKey:key] objectForKey:@"scrollView"];
		scrollView.contentSize = CGSizeMake(scrollView.bounds.size.width-(kImageViewerRightPadInLandscape && UIInterfaceOrientationIsLandscape(toInterfaceOrientation)?1:0)*scrollView.frame.origin.x,[[[self.playlist itemAtIndex:p] objectForKey:@"count"] intValue]*_scrollView.frame.size.height);
		for (NSNumber *key2 in [grid objectForKey:key]) {
			if ([key2 isKindOfClass:[NSNumber class]]) {
				UIView *container = [[[grid objectForKey:key] objectForKey:key2] objectForKey:@"container"];
				if (container) {
					int img = [key2 intValue];
					CGRect frame = container.frame;
					frame.size.width = scrollView.contentSize.width;
					frame.size.height = _scrollView.frame.size.height;
					frame.origin.y = img*frame.size.height;		
					frame.origin.x = 0;
					container.frame = frame;
				} else {
					NSLog(@"No container?! %@ %@ %@",key,key2,[[grid objectForKey:key] objectForKey:key2]);
				}
			}
		}
		/*
		for (UIView *container in [scrollView subviews]) {
		}
		 */
		scrollView.contentOffset = CGPointMake(0, _scrollView.frame.size.height * [[[grid objectForKey:[NSNumber numberWithInt:p]] objectForKey:@"imageIndex"] intValue]);
		UIView *container = [[grid objectForKey:key] objectForKey:@"container"];
		container.frame = CGRectMake(p*_scrollView.frame.size.width, 0, _scrollView.frame.size.width, _scrollView.frame.size.height);
		
	}
	_scrollView.contentSize = CGSizeMake([self.playlist count]*_scrollView.frame.size.width, _scrollView.frame.size.height);
	_scrollView.contentOffset = CGPointMake(displayedPosition*_scrollView.frame.size.width, 0);
	
}
- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	if (legendExpanded) {
		[self toggleLegend];
	}	
	rotating = YES;
	[super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
	//Disable shadows.
	for (NSNumber *key in grid) {
		for (id key2 in [grid objectForKey:key]) {
			if ([key2 isKindOfClass:[NSNumber class]]) {
				UIImageView *iv = [[[grid objectForKey:key] objectForKey:key2] objectForKey:@"imageView"];
				if (iv) {
					iv.layer.shadowOpacity = 0.0;
				}
			}
		}
	}
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
	for (NSNumber *key in grid) {
		for (id key2 in [grid objectForKey:key]) {
			if ([key2 isKindOfClass:[NSNumber class]]) {
				UIImageView *iv = [[[grid objectForKey:key] objectForKey:key2] objectForKey:@"imageView"];
				if (iv) {
					iv.layer.shadowPath = [UIBezierPath bezierPathWithRect:[self rectFromImageView:iv]].CGPath;
					iv.layer.shadowOpacity = kImageViewerShadowOpacity;
				}
			}
		}
	}
	rotating = NO;
	/*
	ignore = YES;
	[self scrollToCurrent];
	ignore = NO; 
	 */
}
-(void)scrollToCurrent {
	[_scrollView setContentSize:CGSizeMake(_scrollView.frame.size.width*[self.playlist count], _scrollView.frame.size.height)];
	[_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width*[self.playlist position], 0)];
	//[self unloadScrollViewsLeftOf:-1 orRightOf:-1];
	//[self loadUpDownScrollView:[self.playlist position] atIndex:imageIndex];	
}
/*
-(void)animateScrollView:(UIScrollView *)scrollView toContentOffset:(CGPoint)endPoint overPeriod:(NSTimeInterval)duration {
	NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:scrollView,@"scrollView",NSStringFromCGPoint(scrollView.contentOffset),@"startPoint",NSStringFromCGPoint(endPoint),@"endPoint",[NSDate date],@"startDate",[NSNumber numberWithDouble:duration],@"duration", nil];
	APP.window.userInteractionEnabled = NO;
	[NSTimer scheduledTimerWithTimeInterval:0.00001 target:self selector:@selector(animateScrollTimer:) userInfo:userInfo repeats:YES];
}
-(void)animateScrollTimer:(NSTimer *)timer {
	NSDictionary *uI = [timer userInfo];
	NSTimeInterval duration = [[uI objectForKey:@"duration"] doubleValue];
	NSDate *startDate = [uI objectForKey:@"startDate"];
	UIScrollView *sv = [uI objectForKey:@"scrollView"];
	CGPoint startPoint = CGPointFromString([uI objectForKey:@"startPoint"]);
	CGPoint endPoint = CGPointFromString([uI objectForKey:@"endPoint"]);
	BOOL x = fabs(startPoint.x-endPoint.x) > 0.5;
	CGFloat dist = x?endPoint.x-startPoint.x:endPoint.y-startPoint.y;
	
	CGFloat progress = (-[startDate timeIntervalSinceNow]/duration);
	if (progress > 1) progress = 1;
	if (progress < 0) progress = 0;
	
	CGPoint cur = startPoint;
	if (x) {
		cur.x += dist*progress;
	} else {
		cur.y += dist*progress;
	}
	if (progress == 1) {
		[timer invalidate];
		APP.window.userInteractionEnabled = YES;
		[self scrollToCurrent];
	}
	[sv setContentOffset:cur animated:NO];
	
}
*/
-(void)refreshNotes {
	int c = [Note rowCountMatchingPredicate:PWF(@"Case = %@",[self currentCase])];
	NSString *imgName = (c>0?@"noteOn":@"noteOff");
	notesButton.image = [APP imageNamed:imgName];
}
-(IBAction)diagnosisPressed {
	[Flurry logEvent:@"DiagnosisPressed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	[self diagnosisButtonPressed];
}
@end
