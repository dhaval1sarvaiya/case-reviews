//
//  ShelfViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 05/03/2012.
//  Copyright (c) 2012 BrainBakery Ltd. All rights reserved.
//

#import "ShelfViewController.h"
//#import "UAStoreFront.h"
//#import "UAStoreFrontUI.h"
//#import "UAInboxUI.h"
#import "AppDelegate.h"
//#import "UAInventory.h"
//#import "UADownloadManager.h"
//#import "UAStoreFrontDelegate.h"
//#import "UAStoreFrontViewController.h"
//#import "UAStoreFrontUI.h"
//#import "UAProductDetailViewController.h"
//#import "UAStoreFrontCellView.h"
#import "ProductListViewController.h"



@implementation ShelfViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
//		[[UAStoreFront shared] setDelegate:self];
//		[[[UAStoreFront shared] inventory] loadInventory];
    }
    return self;
}
-(void)dealloc {
	RELEASE(installedBooks);
	RELEASE(_navItem);
	[super dealloc];
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	if (editing) {
		[self editButtonPressed:nil];
	}
	[self loadInstalled];
	if (self.interfaceOrientation != UIInterfaceOrientationPortrait) {
		[self willAnimateRotationToInterfaceOrientation:self.interfaceOrientation duration:0];
		[self didRotateFromInterfaceOrientation:UIInterfaceOrientationPortrait];
	}
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)loadInstalled {
	NSMutableArray *installed = [[NSMutableArray alloc] init];
	NSError *error = nil;
//	NSArray *contents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:kUADownloadDirectory error:&error];
//	BOOL isDir = NO;
//	for (NSString *path in contents) {
//		isDir = NO;
//		if ([path hasPrefix:@"com."] && 
//			![path hasSuffix:@".zip"] && 
//			[[NSFileManager defaultManager] fileExistsAtPath:[kUADownloadDirectory stringByAppendingPathComponent:path] isDirectory:&isDir] && 
//			isDir) {
//			[installed addObject:[BBBook bookWithIdentifier:path]];
//		}
//	}
	NSLog(@"Installed: %@",installed);
	RELEASE(installedBooks);
	[installed sortUsingSelector:@selector(compare:)];	
	installedBooks = [[NSMutableArray alloc] initWithArray:installed];
	RELEASE(installed);
	[self renderBooks];
}
-(void)populateView:(UIView *)view withBook:(BBBook *)book isLeft:(BOOL)isLeft isRight:(BOOL)isRight isTop:(BOOL)isTop isBottom:(BOOL)isBottom index:(NSInteger)i {
	
	CGFloat padding = 20; // MUST BE EVEN
	
	CGFloat lPad = padding;
	CGFloat tPad = padding;
	CGFloat rPad = padding;
	CGFloat bPad = padding;
	if (!isLeft) {
		lPad/=2;
	}
	if (!isRight) {
		rPad/=2;
	}
	if (!isTop) {
		tPad/=2;
	}
	if (!isBottom) {
		bPad/=2;
	}
	UIImage *image = nil;
	BOOL portrait = UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation);
	if (IPAD_IDIOM) {
		image = [book imageNamed:SWF(@"DefaultThumb-%@~ipad",(portrait?@"Portrait":@"Landscape"))];
	} else {
		image = [book imageNamed:@"Default"];
	}
	UIImageView *img = [[UIImageView alloc] initWithImage:image];
	img.contentMode = portrait?UIViewContentModeScaleAspectFit:UIViewContentModeScaleAspectFill;
	img.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
	
	CGFloat iw = image.size.width;
	CGFloat ih = image.size.height;
	CGFloat vw = view.frame.size.width - 2*padding;
	CGFloat vh = view.frame.size.height - 2*padding;
	CGFloat sh = 0;
	CGFloat sw = 0;
	CGFloat diff = 0;
	if (ih == 0 || iw == 0) {
		sh = vh;
		sw = vw;
	} else if (ih/vh > iw/vw) {
		tPad = isTop?padding:padding/2;
		sh = floor(ih * vh/ih);
		sw = floor(iw * vh/ih);
		diff = vw - sw + (2*padding - lPad - rPad);
		if (isLeft && !isRight) {
			lPad += 2*diff/3;
		} else if (isRight && !isLeft) {
			lPad += 1*diff/3;
		} else {
			lPad += diff/2;
		}
	} else {
		lPad = isLeft?padding:padding/2;
		sh = floor(ih * vw/iw);
		sw = floor(iw * vw/iw);
		diff = vh - sh + (2*padding - tPad - bPad);
		if (isTop && !isBottom) {
			tPad += 2*diff/3;
		} else if (isBottom && !isTop) {
			tPad += 1*diff/3;
		} else {
			tPad += diff/2;
		}
	}
	tPad = floor(tPad);
	lPad = floor(lPad);
	
	CGRect frame = CGRectMake(lPad, tPad, sw, sh);
	
	//CGRect frame = CGRectMake(lPad, tPad, view.frame.size.width - (lPad+rPad), view.frame.size.height - (tPad+bPad));
	img.frame = frame;
	
	if (editing) {
		image = [UIImage imageNamed:@"delete"];
		padding = 6;
		UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-image.size.width/2 - padding, -image.size.height/2 + padding, image.size.width, image.size.height)];
		btn.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
		btn.tag = i;
		[btn addTarget:self action:@selector(deleteBook:) forControlEvents:UIControlEventTouchUpInside];
		[btn setImage:image forState:UIControlStateNormal];
		[img addSubview:btn];
		RELEASE(btn);
	}
	[view addSubview:img];
	RELEASE(img);
}
-(void)renderBooks {
	for (int i = [[scrollView subviews] count] - 1; i>=0; i--) {
		[[[scrollView subviews] objectAtIndex:i] removeFromSuperview];
	}
	int cols = IPAD_IDIOM ? 2 : 1;
	int rows = IPAD_IDIOM ? 2 : 1;
	int booksPerPage = cols*rows;
	pageControl.numberOfPages = ceil([installedBooks count]/(cols*rows+0.0));
	CGRect frame = pageControl.frame;
	frame.size.width = [pageControl sizeForNumberOfPages:pageControl.numberOfPages].width;
	frame.origin.x = (self.view.bounds.size.width - frame.size.width)/2;
	pageControl.frame = frame;
	CGRect f;
	if ([installedBooks count] == 0) {
		scrollView.contentSize = scrollView.frame.size;
		UIView *goToStore = [[UIView alloc] initWithFrame:scrollView.bounds];
		goToStore.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
		f = goToStore.bounds;
		f.origin.x += f.size.width/4;
		f.origin.y += f.size.height/4;
		f.size.width/=2;
		f.size.height /=2;
		
		f.origin.x = floor(f.origin.x);
		f.origin.y = floor(f.origin.y);
		f.size.width = floor(f.size.width);
		f.size.height = floor(f.size.height);

		UILabel *label = [[UILabel alloc] initWithFrame:f];
        label.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
		label.numberOfLines = 10;
		label.backgroundColor = [UIColor clearColor];
		label.textColor = [UIColor whiteColor];
		label.shadowColor = [UIColor grayColor];
		label.shadowOffset = CGSizeMake(0, 1);
		label.textAlignment = UITextAlignmentCenter;
		label.text = @"Go to the Store to download content";
		[goToStore addSubview:label];
		RELEASE(label);
		
		[scrollView	addSubview:goToStore];
		RELEASE(goToStore);
		return;
	}
	CGFloat w = (scrollView.frame.size.width/cols);
	CGFloat h = (scrollView.frame.size.height/rows);
	scrollView.contentSize = CGSizeMake(pageControl.numberOfPages * scrollView.frame.size.width, scrollView.frame.size.height);
	UIView *currentPage = nil;
	for (NSInteger i = 0, l = [installedBooks count]; i<l; i++) {
		BBBook *book = [installedBooks objectAtIndex:i];
		int page = floor(i/booksPerPage);
		int x = i % cols;
		int y = floor( (i%booksPerPage)/cols);
		if (i % booksPerPage == 0) {
			f = scrollView.bounds;
			f.origin.x = page * f.size.width;
			RELEASE(currentPage);
			currentPage = [[UIView alloc] initWithFrame:f];
			[scrollView addSubview:currentPage];
		}
		
		UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bookTapped:)];

		UIView *bookView = [[UIView alloc] initWithFrame:CGRectMake(floor((x * w)), floor(y*h), floor(w), floor(h))];
		bookView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
		bookView.tag = i;
		[bookView addGestureRecognizer:tgr];
		bookView.clipsToBounds = YES;

		BOOL isLeft = x == 0,
			isRight = x == (cols-1),
			isTop = y == 0,
			isBottom = y == (rows-1);
		
		[self populateView:bookView withBook:book isLeft:isLeft isRight:isRight isTop:isTop isBottom:isBottom index:i];
		
		[currentPage addSubview:bookView];
		RELEASE(tgr);
		RELEASE(bookView);
	}
	RELEASE(currentPage);
}
- (void)bookTapped:(UITapGestureRecognizer *)sender {
	if (sender.state == UIGestureRecognizerStateEnded) {
		if (editing) {
			[self deleteBook:sender.view];
		} else {
			BBBook *book = [installedBooks objectAtIndex:sender.view.tag];
			NSLog(@"Tapped book: %@",book);
			UIImageView *imageView = [sender.view.subviews objectAtIndex:0];
			CGRect frame = imageView.frame;
			frame.origin.x += sender.view.frame.origin.x;
			frame.origin.y += sender.view.frame.origin.y;

			frame.origin.x = (int)floor(frame.origin.x) % (int)floor(scrollView.frame.size.width);
			
			frame.origin.x += scrollView.frame.origin.x;
			frame.origin.y += scrollView.frame.origin.y + 20;
			[APP openBook:book fromRect:frame];
		}
	} 
}

-(void)editButtonPressed:(id)sender {
	editing = !editing;
	if (!editing) {
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonPressed:)] autorelease];
	} else {
		self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editButtonPressed:)] autorelease];
	}
	[self renderBooks];
}
-(void)deleteBook:(UIView *)sender {
	int i = sender.tag;
	UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Delete Book" message:@"Are you sure you want to delete this book?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
	[av show];
	av.tag = i;
	RELEASE(av);
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (buttonIndex == 1) {
		[self reallyDeleteBook:alertView.tag];
	}
}
-(void)reallyDeleteBook:(NSInteger)i {
	BBBook *book = [installedBooks objectAtIndex:i];
	if (book) {
		if ([book removeFromDevice]) {
			[installedBooks removeObjectAtIndex:i];
			[self renderBooks];
		} else {
			UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:@"An error occured whilst deleting the book" delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
			[av show];
			RELEASE(av);
		}
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationItem.title = self.navItem.title;
	self.navigationItem.leftBarButtonItem = self.navItem.leftBarButtonItem;
	self.navigationItem.rightBarButtonItem = self.navItem.rightBarButtonItem;
	[self loadInstalled];
	progressView.hidden = YES;
	pageControl.hidden = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
	[self setNavItem:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)storeButtonPressed:(id)sender {
    
    ProductListViewController *productOb = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil];
    [self.navigationController pushViewController:productOb animated:YES];
//	[UAStoreFront displayStoreFront:self animated:YES];
   
//    skpayment initWithProductIdentifiers:[RageIAPHelper sharedInstance]
//    [skpayment b]
    
//    [RageIAPHelper sharedInstance]
    
}

-(void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	int i = 0;
	for (UIView *v in scrollView.subviews) {
		CGRect f = v.frame;
		f.size = scrollView.bounds.size;
		f.origin.x = i*f.size.width;
		v.frame = f;
		i++;
	}
	CGFloat oldWidth = scrollView.contentSize.width / [scrollView.subviews count];
	int page = ceil(scrollView.contentOffset.x/oldWidth);
	scrollView.contentSize = CGSizeMake([scrollView.subviews count]*scrollView.bounds.size.width, scrollView.bounds.size.height);
	[scrollView setContentOffset:CGPointMake(page*scrollView.bounds.size.width, 0) animated:NO];
}


-(void)scrollViewDidScroll:(UIScrollView *)_scrollView {
	pageControl.currentPage = round(scrollView.contentOffset.x/scrollView.frame.size.width);
}


-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self renderBooks];
}



//- (void)productPurchased:(UAProduct *) product {
//	NSLog(@"Purchased: %@",product);
//	[self loadInstalled];
//	progressView.hidden = YES;
//	pageControl.hidden = NO;
//	[activityIndicatorView stopAnimating];
//}
- (void)storeFrontWillHide {
	NSLog(@"About to hide storefront");
	[self loadInstalled];
}
- (void)productsDownloadProgress:(float)progress count:(int)count {
	NSLog(@"Products download progress: %.2f (%d)",progress, count);
	progressView.progress = progress;
	progressView.hidden = NO;
	pageControl.hidden = YES;
	if (progress > 0.995) {
		[activityIndicatorView startAnimating];
	}

}
-(void)openAbout:(id)sender {
	if (![MFMailComposeViewController canSendMail]) {
		for (UIView *view in aboutView.subviews) {
			if ([view isKindOfClass:[UIButton class]]) {
				view.hidden = YES;
			}
		}
	}
	RELEASE(aboutContainerView);
	aboutContainerView = [[UIView alloc] initWithFrame:self.view.bounds];
	aboutContainerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	aboutContainerView.backgroundColor = [UIColor clearColor];
	CGRect frame = aboutView.frame;
	frame.origin.y = self.view.bounds.size.height;
	frame.origin.x = (self.view.bounds.size.width - frame.size.width)/2;
	aboutView.frame = frame;
	frame.origin.y = (self.view.bounds.size.height - frame.size.height + [[UIScreen mainScreen] applicationFrame].origin.y)/2;
	aboutView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin;
	[aboutContainerView addSubview:aboutView];
	[APP.window addSubview:aboutContainerView];
	
	[UIView animateWithDuration:0.5 animations:^{
		if (IPAD_IDIOM) {
			aboutContainerView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.7];
		}
		aboutView.frame = frame;
	} completion:^(BOOL finished){}];
}
-(void)closeAbout:(id)sender {
	CGRect frame = aboutView.frame;
	frame.origin.y = self.view.bounds.size.height;
	[UIView animateWithDuration:0.5 animations:^{
		aboutView.frame = frame;
		aboutContainerView.backgroundColor = [UIColor clearColor];
	} completion:^(BOOL finished){
		dispatch_async( dispatch_get_main_queue(), ^{
			[aboutContainerView removeFromSuperview];
			RELEASE(aboutContainerView);
		});
		
	}];
}
-(void)email:(NSString *)address subject:(NSString *)subject {
	[self closeAbout:nil];
	MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init];
	[vc setModalPresentationStyle:UIModalPresentationFormSheet];
	vc.mailComposeDelegate = self;
	[vc setToRecipients:[NSArray arrayWithObject:address]];
	[vc setSubject:subject];
	[self presentViewController:vc animated:YES completion:^{}];
	RELEASE(vc);	
}
-(void)emailSupport:(id)sender {
	[self email:@"Case Reviews App Support <technical.support@elsevier.com>" subject:@"Enquiry from Case Reviews App"];
}
-(void)emailFeedback:(id)sender {
	[self email:@"Case Reviews App Feedback <apps@elsevierhealth.com>" subject:@"Feedback from Case Reviews App"];	
}
-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
	[self dismissViewControllerAnimated:YES completion:^{}];
}

@end
