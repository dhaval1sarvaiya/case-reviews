//
//  ContentsViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 11/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ContentsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	IBOutlet UITableView *_tableView;
	IBOutlet UIImageView *_productIdentifierImageView;
	
	NSDictionary *cases;
	NSArray *contents;
}
-(void)displayViewController:(UIViewController *)vc animated:(BOOL)animated;
-(IBAction)index;
@end
