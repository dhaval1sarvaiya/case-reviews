//
//  NotesViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 22/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "NotesViewController.h"
#import "NewNoteViewController.h"
#import "AppDelegate.h"
#import "Note.h"

@implementation NotesViewController
@synthesize parent;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
	RELEASE(notes);
	self.parent = nil;
	RELEASE(navBar);//auto
	RELEASE(editButton);//auto
	RELEASE(_tableView);//auto
	[super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.navigationItem.title = self.templateNavItem.title;
	self.navigationItem.leftBarButtonItem = self.templateNavItem.leftBarButtonItem;
	self.navigationItem.rightBarButtonItem = self.templateNavItem.rightBarButtonItem;
	[self refreshNotes];
}


- (void)viewDidUnload {
	[self retain];//auto
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	RELEASE(navBar);//auto
	RELEASE(editButton);//auto
	RELEASE(_tableView);//auto
	[self autorelease];//auto
}
-(void)viewDidAppear:(BOOL)animated {
	[_tableView reloadData];
	[super viewDidAppear:animated];//auto
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Table view

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	
	return [notes count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return [self tableView:tableView cellForRowAtIndexPath:indexPath].frame.size.height;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	static NSString *CellIdentifier = @"NoteCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	int tag = 44436;
	UILabel *label;
	if (!cell) {
		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
		cell.frame = CGRectMake(0, 0, tableView.bounds.size.width, 44);
		UILabel *label = [[[UILabel alloc] initWithFrame:CGRectMake(10, 3, cell.frame.size.width-2*10, cell.frame.size.height-2*3)] autorelease];
		//label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
		label.tag = tag;
		[cell.contentView addSubview:label];
		label.numberOfLines = 2;
		label.lineBreakMode = UILineBreakModeTailTruncation;
		label.font = [UIFont systemFontOfSize:20];
		label.backgroundColor = [UIColor clearColor];
		cell.backgroundColor = [UIColor clearColor];
	}
	label = (UILabel *)[cell viewWithTag:tag];
	CGFloat width = label.frame.size.width;

	label.text = [(Note *)[notes objectAtIndex:indexPath.row] note];
	CGSize size = [label.text sizeWithFont:label.font constrainedToSize:CGSizeMake(width, [@"M\nM" sizeWithFont:label.font constrainedToSize:CGSizeMake(999, 999) lineBreakMode:UILineBreakModeWordWrap].height) lineBreakMode:UILineBreakModeWordWrap];
	CGRect frame = label.frame;
	frame.size.width = width;
	frame.size.height = size.height;
	label.frame = frame;

	frame = cell.frame;
	frame.size.height = label.frame.origin.y * 2 + label.frame.size.height;
	cell.frame = frame;
	return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[self openNote:[notes objectAtIndex:indexPath.row]];
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
	return UITableViewCellEditingStyleDelete;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		Note *n = [notes objectAtIndex:indexPath.row];
		[n deleteFromCoreDataAndSave:YES];
		[notes removeObjectAtIndex:indexPath.row];
		[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
		[self.parent refreshNotes];
	}
}

#pragma mark - Buttons

-(IBAction)closeButtonPressed {
	[self dismissModalViewControllerAnimated:YES];
}
-(IBAction)editButtonPressed {
	_tableView.editing = !_tableView.editing;
}
-(IBAction)newButtonPressed {
	[self openNote:nil];
}
-(void)openNote:(Note *)note {
	UNIMPLEMENTED();
}
-(Playlist *)playlist {
	return self.parent.playlist;
}

-(void)refreshNotes {
	int caseNr = [[self.parent.playlist.currentItem objectForKey:@"case"] intValue];
	RELEASE(notes);
	notes = [[NSMutableArray alloc] initWithArray:[Note arrayMatchingPredicate:PWF(@"Case.number = %i",caseNr)]];	
	[_tableView reloadData];
}
@end
