//
//  ContentsViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 11/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "ContentsViewController.h"
#import "ContentsCell.h"
#import "HTMLViewController_iPhone.h"
#import "HTMLViewController_iPad.h"
#import "AppDelegate.h"
#import "Playlist.h"
#import "PlaylistViewController_iPad.h"
#import "PlaylistViewController_iPhone.h"
#import "Case.h"
#import "Note.h"
#import "AssessmentViewController.h"

@implementation ContentsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		cases = [[APP.book allCases] retain];
        // Custom initialization
		contents = [[NSArray alloc] initWithContentsOfFile:[APP.book pathForFile:@"Plists/contents.plist"]];
    }
    return self;
}

- (void)dealloc {
	RELEASE(contents);
	RELEASE(cases);
	RELEASE(_tableView);//auto
	RELEASE(_productIdentifierImageView);//auto
	[super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

- (void)viewDidLoad {
    [super viewDidLoad];
	_productIdentifierImageView.image = [APP imageNamed:IPAD_IDIOM?@"titleLg":@"title"];
	CGRect frame = _productIdentifierImageView.frame;
	CGFloat diff = frame.size.height - _productIdentifierImageView.image.size.height;
	frame.size.height -= diff;
	frame.origin.y += diff;
	_productIdentifierImageView.frame = frame;
	frame = _tableView.frame;
	frame.size.height += diff;
	_tableView.frame = frame;
	
	self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(close)] autorelease];
}

-(void)close {
	[APP closeBook];
}

- (void)viewDidUnload {
	[self retain];//auto
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	RELEASE(_tableView);//auto
	RELEASE(_productIdentifierImageView);//auto
	[self autorelease];//auto
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Table View stuff
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [contents count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 60;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSString *identifier = @"ContentsCell";
	ContentsCell *cell = (ContentsCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
	if (!cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContentsCell" owner:self options:nil];
		for (id obj in nib) {
			if ([obj isKindOfClass:[ContentsCell class]]) {
				cell = obj;
				break;
			}
		}
	}
	NSAssert(cell,@"ARGH! Couldn't load cell!");
	NSDictionary *entry = [contents objectAtIndex:indexPath.row];
	[cell setEntry:entry];
	return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	NSDictionary *entry = [contents objectAtIndex:indexPath.row];
	NSString *type = [entry objectForKey:@"type"];
	NSString *data = [entry objectForKey:@"data"];
	NSString *title = [entry objectForKey:@"title"];
	if ([type isEqualToString:@"HTML"]) {
		HTMLViewController *vc = ALOAD_VC(HTMLViewController);
		[vc loadFile:data];
		[self displayViewController:vc animated:YES];
	} else if ([type isEqualToString:@"Playlist"]) {
		NSMutableArray *items = [[NSMutableArray  alloc] init];
		PlaylistType playlistType = PlaylistTypeNormal;
		PlaylistViewController *pvc = ALOAD_VC(PlaylistViewController);
		if ([data isEqualToString:@"Bookmarks"]) {
			playlistType = PlaylistTypeBookmarks;
			NSArray *bookmarkedCases = [Case arrayMatchingPredicate:PWF(@"isStarred = %@",[NSNumber numberWithBool:YES]) orderedBy:@"number" orderAscending:YES];
			for (Case *c in bookmarkedCases) {
				NSDictionary *item = [cases objectForKey:[c.number description]];
				if (!item) {
					NSLogOkay(@"WARNING: Missing case: %i",c.numberInt);
				} else {
					[items addObject:item];
				}
			}
		} else if ([data isEqualToString:@"Notes"]) {
			playlistType = PlaylistTypeNotes;
			NSArray *notes = [Note arrayMatchingPredicate:nil];
			for (Note *n in notes) {
				NSMutableDictionary *item = nil;
				for (NSMutableDictionary *e in items) {
					if ([[e objectForKey:@"case"] intValue] == n.Case.numberInt) {
						item = [e retain];
						break;
					}
				}
				if (!item) {
					item = [[NSMutableDictionary alloc] initWithDictionary:[cases objectForKey:[n.Case.number description]]];
					[item setObject:[NSMutableArray array] forKey:@"notes"];
				}
				if (!item) {
					NSLogOkay(@"WARNING: Missing case: %i",n.Case.numberInt);
				} else {
					[[item objectForKey:@"notes"] addObject:n];
					if (![items containsObject:item]) {
						[items addObject:item];
					}
				}				
				RELEASE(item);
			}
		} else if ([data isEqualToString:@"Search"]) {
			playlistType = PlaylistTypeNormal;
			[items addObjectsFromArray:[cases allValues]];
			pvc.isSearch = YES;
		} else {
			playlistType = PlaylistTypeNormal;
			NSArray *tmp = [data componentsSeparatedByString:@"-"];
			if ([tmp count] == 2) {
				int start = [[tmp objectAtIndex:0] intValue];
				int stop = [[tmp objectAtIndex:1] intValue];
				for (int i = start; i<=stop; i++) {
					NSDictionary *item = [cases objectForKey:[[NSNumber numberWithInt:i] description]];
					if (!item) {
						NSLogOkay(@"WARNING: Missing case: %i",i);
					} else {
						[items addObject:item];
					}
				}
			} else {
				UNIMPLEMENTED();
			}
		}
		
		Playlist *playlist = [[Playlist alloc] initWithTitle:title andItems:items ofType:playlistType];
		RELEASE(items);
		pvc.playlist = playlist;
		RELEASE(playlist);
		
		[self displayViewController:pvc animated:YES];
	} else if ([type isEqualToString:@"Overview"]) {
		AssessmentViewController *vc = ALOAD_VC(AssessmentViewController);
		[self displayViewController:vc animated:YES];
	} else {
		UNIMPLEMENTED();
	}
}

#pragma mark - Other
-(void)displayViewController:(UIViewController *)vc animated:(BOOL)animated {
	UNIMPLEMENTED();
}
-(IBAction)index {
	UNIMPLEMENTED();
}
@end
