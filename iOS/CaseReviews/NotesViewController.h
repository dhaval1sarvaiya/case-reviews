//
//  NotesViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 22/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageViewController.h"
#import "Playlist.h"

@interface NotesViewController : UIViewController <UITableViewDelegate,UITableViewDataSource> {
	IBOutlet UINavigationBar *navBar;
    IBOutlet UIBarButtonItem *editButton;
	IBOutlet UITableView *_tableView;
	
	ImageViewController *parent;
	
	NSMutableArray *notes;
}
@property (nonatomic,assign) ImageViewController *parent;
@property (nonatomic,readonly) Playlist *playlist;
@property (nonatomic, retain) IBOutlet UINavigationItem *templateNavItem;


-(IBAction)closeButtonPressed;
-(IBAction)editButtonPressed;
-(IBAction)newButtonPressed;


-(void)openNote:(Note *)note;
-(void)refreshNotes;
@end
