//
//  Note.m
//  CaseReviews
//
//  Created by Benjie Gillam on 21/07/2011.
//  Copyright (c) 2011 BrainBakery Ltd. All rights reserved.
//

#import "Note.h"
#import "Case.h"


@implementation Note
@dynamic note;
@dynamic Case;


@end
