//
//  NewNoteViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 22/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "NewNoteViewController.h"
#import "Note.h"
#import "Case.h"
#import "AppDelegate.h"


@implementation NewNoteViewController
@synthesize parent;
@synthesize note=_note;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
	RELEASE(_textView);//auto
	self.parent = nil;//auto
	RELEASE(_note);//auto
	[super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
	self.navigationItem.title = SWF(@"Case %@",[self.parent.playlist.currentItem objectForKey:@"case"]);
	self.navigationItem.leftBarButtonItem = self.templateNavItem.leftBarButtonItem;
	self.navigationItem.rightBarButtonItem = self.templateNavItem.rightBarButtonItem;
	if (self.note) {
		_textView.text = self.note.note;
	}
	[_textView becomeFirstResponder];
}


- (void)viewDidUnload {
	[self retain];//auto
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	RELEASE(_textView);//auto
	[self autorelease];//auto
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}
-(IBAction)cancelButtonPressed {
	UNIMPLEMENTED();
}
-(IBAction)saveButtonPressed {
	if (!self.note) {
		[Flurry logEvent:@"NewNoteAdded" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
		int caseNr = [[self.playlist.currentItem objectForKey:@"case"] intValue];
		Case *thisCase = [Case anyObjectMatchingPredicate:PWF(@"number=%i",caseNr)];
		if (!thisCase) {
			thisCase = COREDATA_CREATE(Case);
			thisCase.numberInt = caseNr;
		}
		self.note = COREDATA_CREATE(Note);
		self.note.Case = thisCase;
	} else {
		[Flurry logEvent:@"NoteEdited" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	}
	self.note.note = _textView.text;
	[APP saveContext];
	[self.parent refreshNotes];
}
-(Playlist *)playlist {
	return self.parent.playlist;
}
@end
