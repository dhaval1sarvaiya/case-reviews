//
//  BBBook.m
//  CaseReviews
//
//  Created by Benjie Gillam on 06/03/2012.
//  Copyright (c) 2012 BrainBakery Ltd. All rights reserved.
//

#import "BBBook.h"
//#import "UADownloadManager.h"
//#import "UAStoreFront.h"

NSMutableDictionary *BBBook_Cache = nil;

@implementation BBBook
@synthesize identifier;
@synthesize allCases, allCasesWithQuestions, searchDict;
@synthesize shortIdentifier;

+(BBBook *)bookWithIdentifier:(NSString *)_identifier {
	if (!BBBook_Cache) {
		BBBook_Cache = [[NSMutableDictionary alloc] init];
	}
	BBBook *book = [BBBook_Cache objectForKey:_identifier];
	if (!book) {
		book = [[[BBBook alloc] initWithIdentifier:_identifier] autorelease];
		[BBBook_Cache setObject:book forKey:_identifier];
	} else {
		[book refresh];
	}
	return book;
}
-(id)initWithIdentifier:(NSString *)_identifier {
	if ((self = [super init])) {
		identifier = [_identifier retain];
		retina = NO;
		if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)] == YES && [[UIScreen mainScreen] scale] == 2.00) {
			retina = YES;
		}
		[self refresh];
	}
	return self;
}
-(void)refresh {
	RELEASE(infoPlist);
	infoPlist = [[NSDictionary alloc] initWithContentsOfFile:[self.path stringByAppendingPathComponent:@"Plists/Info.plist"]];	
}
-(void)dealloc {
	RELEASE(infoPlist);
	RELEASE(identifier);
	RELEASE(allCases);
	RELEASE(allCasesWithQuestions);
	RELEASE(searchDict);
	[super dealloc];
}
-(NSString *)description {
	return SWF(@"BBBook: %@",self.bookName);
}

-(NSString *)path {
//	return [kUADownloadDirectory stringByAppendingPathComponent:self.identifier];
}
-(NSURL *)URL {
	return [NSURL fileURLWithPath:self.path isDirectory:YES];
}
-(NSString *)bookName {
	return [self identifier];
}
-(NSString *)shortIdentifier {
	NSString *str = [self identifier];
	NSRange range = [str rangeOfString:@"." options:NSBackwardsSearch];
	if (range.location != NSNotFound) {
		str = [str substringFromIndex:range.location+1];
	}
	return str;
}
-(UIImage *)icon {
	return [self imageNamed:@"Icon"];
}
-(NSDictionary *)allCases {
	if (allCases) {
		return allCases;
	}
	allCases = [[NSDictionary alloc] initWithContentsOfFile:[self pathForFile:@"Plists/cases.plist"]];
	searchDict = [[NSDictionary alloc] initWithContentsOfFile:[self pathForFile:@"Plists/search.plist"]];
	NSMutableDictionary *qns = [[NSMutableDictionary alloc] init];
	for (id k in allCases) {
		if ([[[allCases objectForKey:k] objectForKey:@"answers"] count] > 0) {
			[qns setObject:[allCases objectForKey:k] forKey:k];
		}
	}
	allCasesWithQuestions = [[NSDictionary alloc] initWithDictionary:qns];
	RELEASE(qns);
	return allCases;
}
-(NSDictionary *)allCasesWithQuestions {
	[self allCases];
	return allCasesWithQuestions;
}

-(NSString *)pathForFile:(NSString *)file {
	return [self.path stringByAppendingPathComponent:file];
}

-(UIImage *)imageNamed:(NSString *)name {
	if ([name hasSuffix:@".png"]) {
		name = [name substringToIndex:[name length]-4];
	}
	NSMutableArray *alternatives = [NSMutableArray array];
	NSString *suffix = @"";
	if ([name hasSuffix:@"~ipad"]) {
		suffix = @"~ipad";
		name = [name substringToIndex:[name length]-5];
	}
	if (retina) {
		[alternatives addObject:SWF(@"%@@2x%@.png",name,suffix)];
	}
	[alternatives addObject:SWF(@"%@%@.png",name,suffix)];
	[alternatives addObject:SWF(@"%@%@",name,suffix)];
	UIImage *result = nil;
	for (NSString *file in alternatives) {
		result = [self imageWithContentsOfFile:file];
		if (result) {
			break;
		}
	}
	if (!result) {
		NSLog(@"Book %@ failed to find image %@",self,name);
	}
	return result;
}
-(UIImage *)imageWithContentsOfFile:(NSString *)file {
	if ([file hasPrefix:@"/"]) {
		return [UIImage imageWithContentsOfFile:file];
	}
	NSString *path = [self.path stringByAppendingPathComponent:file];
	if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
		return [UIImage imageWithContentsOfFile:path];
	}
	return nil;
}
-(NSString *)pathForResource:(NSString *)resource ofType:(NSString *)type inDirectory:(NSString *)dir {
	NSString *path = self.path;
	if (dir) {
		path = [path stringByAppendingPathComponent:dir];
	}
	path = [path stringByAppendingPathComponent:SWF(@"%@.%@",resource,type)];
	return path;
}
-(NSURL *)URLForResource:(NSString *)resource withExtension:(NSString *)ext subdirectory:(NSString *)dir {
	NSURL *URL = self.URL;
	if (dir) {
		URL = [URL URLByAppendingPathComponent:dir isDirectory:YES];
	}
	URL = [URL URLByAppendingPathComponent:SWF(@"%@.%@",resource,ext) isDirectory:NO];
	return URL;
}
-(BOOL)removeFromDevice {
	NSLog(@"DELETE BOOK");
	NSError *error = nil;
	BOOL res = [[NSFileManager defaultManager] removeItemAtPath:[self path] error:&error];
	if (!res) {
		NSLog(@"Error deleting book: %@",error);
	} else {
		/*
		 Despite this being a bit ridiculous and making the user reauthenticate, UA 
		 themselves recommend this method:
		 https://support.urbanairship.com/customer/portal/questions/176611-remove-product
		 */
//		UAStoreFront *shared = [UAStoreFront shared];
//		[shared.purchaseReceipts setValue:nil forKey:[self identifier]];
//		[UAStoreFront loadInventory];
	}

	return res;
}
-(NSComparisonResult)compare:(BBBook *)other {
	return [[self identifier] compare:[other identifier]];
	
}
@end
