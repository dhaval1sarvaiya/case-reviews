//
//  PlaylistViewController.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "PlaylistViewController.h"
#import "CaseCell.h"
#import "ImageViewController_iPad.h"
#import "ImageViewController_iPhone.h"
#import "AppDelegate.h"
@implementation PlaylistViewController
@synthesize playlist;
@synthesize isSearch;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		first = YES;
    }
    return self;
}

- (void)dealloc {
	self.playlist = nil;
	RELEASE(_tableView);//auto
	RELEASE(shuffleButton);//auto
	RELEASE(showTitlesButton);//auto
	RELEASE(topCell);//auto
	RELEASE(_searchBar);//auto
	RELEASE_TIMER(searchBarTimer);//auto
	[super dealloc];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
-(void)checkEmpty {
	PlaylistType type = [self.playlist type];
	
	NSString *event = @"ViewPlaylist";
	NSString *add = @"";
	if (type == PlaylistTypeNotes) {
		add = @"Notes";
	} else if (type == PlaylistTypeBookmarks) {
		add = @"Bookmarks";
	} else if (type == PlaylistTypeQuestionsCorrect) {
		add = @"QuestionsCorrect";		
	} else if (type == PlaylistTypeQuestionsIncorrect) {
		add = @"QuestionsIncorrect";				
	} else if (type == PlaylistTypeQuestionsUnanswered) {
		add = @"QuestionsUnanswered";		
	}
	event = [event stringByAppendingString:add];
	[Flurry logEvent:event withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	
	if ([_tableView numberOfRowsInSection:0] == 0 && (type == PlaylistTypeBookmarks || type == PlaylistTypeNotes)) {
		shuffleButton.enabled = NO;
		[_tableView setHidden:YES];
		[self.view addSubview:emptyView];
		emptyView.center = self.view.center;
		if (type == PlaylistTypeBookmarks) {
			emptyLabel.text = @"To bookmark a case tap the star icon";
			emptyImageView.image = [APP imageNamed:@"noBookmark"];
		} else if (type == PlaylistTypeNotes) {
			emptyLabel.text = @"To add a note to a case tap the note icon";
			emptyImageView.image = [APP imageNamed:@"noNote"];
		}
	}
}
- (void)viewDidLoad {
    [super viewDidLoad];
	if ([self.playlist type] == PlaylistTypeNotes) {
		titles = YES;
		_tableView.backgroundColor = [UIColor colorWithRed:246/255.0 green:240/255.0 blue:134/255.0 alpha:1]; // < RICHARD
	}
	[self updateShowTitlesButton];
	[self updateWithTableReload:NO];
	if ([self tableView:_tableView numberOfRowsInSection:0]>1 && [self.playlist type] != PlaylistTypeNotes) {
		if (!isSearch) {
			[_tableView reloadData];
			[_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:NO];
		}
	} else {
		_tableView.tableHeaderView = nil;
	}
	if (isSearch) {
		//searchBarTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1 target:_searchBar selector:@selector(becomeFirstResponder) userInfo:nil repeats:NO] retain];
	}
	[self checkEmpty];
}
-(void)viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];
	RELEASE_TIMER(searchBarTimer);
	if (isSearch) {
		if (self.playlist.isShuffled) {
			[self.playlist toggleShuffle];
		}
		//[_searchBar becomeFirstResponder];
	}
	[self checkEmpty];
}
- (void)viewDidUnload {
	[self retain];//auto
	[super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
	RELEASE(_tableView);//auto
	RELEASE(shuffleButton);//auto
	RELEASE(showTitlesButton);//auto
	RELEASE(_searchBar);//auto
	[self autorelease];//auto
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return [self.playlist count];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {	
	return [CaseCell heightForCellWithEntry:[playlist itemAtIndex:indexPath.row] inWidth:tableView.frame.size.width withTitle:titles];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
	NSString *caseCellIdentifier = @"CaseCell";
	CaseCell *cell = (CaseCell *)[tableView dequeueReusableCellWithIdentifier:caseCellIdentifier];
	if (!cell) {
		NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CaseCell" owner:self options:nil];
		for (id a in nib) {
			if ([a isKindOfClass:[CaseCell class]]) {
				cell = a;
				break;
			}
		}
	}
	[cell setItem:[playlist itemAtIndex:indexPath.row] inWidth:tableView.frame.size.width withTitle:titles];
	NSAssert(cell,@"ERROR: Could not load cell!");
	return cell;
}
-(void)displayViewController:(UIViewController *)vc animated:(BOOL)animated {
	UNIMPLEMENTED();
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[self.playlist setPosition:indexPath.row];
	ImageViewController *ivc = ALOAD_VC(ImageViewController);
	ivc.playlist = self.playlist;
	if ([self.playlist type] == PlaylistTypeNotes) {
		ivc.openNotesOnOpen = YES;
	}
	[self displayViewController:ivc animated:YES];	
}
-(void) updateWithTableReload:(BOOL)reload {
	//shuffleButton.title = self.playlist.isShuffled?@"Ord":@"Rnd";
	shuffleButton.image = [APP imageNamed:self.playlist.isShuffled?@"shuffleOn":@"shuffleOff"];

	if (reload) {
		[self.playlist removeInvalid];
		[_tableView reloadData];
	}
}
-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	RELEASE_TIMER(searchBarTimer);
	[self updateWithTableReload:YES];
	if (first) {
		first = NO;
		return;
	}
	if ([self.playlist count] > 0) {
		[_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:[self.playlist position] inSection:0] animated:NO scrollPosition:UITableViewScrollPositionMiddle];
	}
	
	if (isSearch) {
		//searchBarTimer = [[NSTimer scheduledTimerWithTimeInterval:0.1 target:_searchBar selector:@selector(becomeFirstResponder) userInfo:nil repeats:NO] retain];
	}
}
-(IBAction)shuffleButtonPressed {
	[Flurry logEvent:@"PlaylistShuffleButtonPressed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	[self.playlist toggleShuffleWithCurrentFirst:NO];
	[self updateWithTableReload:YES];
}
-(void)updateShowTitlesButton {
	[showTitlesButton setImage:[APP imageNamed:titles?@"titlesOn":@"titlesOff"] forState:UIControlStateNormal];	
}
-(IBAction)showTitlesButtonPressed {
	[Flurry logEvent:@"PlaylistShowTitlesButtonPressed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	titles = !titles;
	[self updateShowTitlesButton];
	[_tableView reloadData];
}
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	RELEASE(topCell);
	topCell = [[_tableView indexPathsForVisibleRows] count]>0?[[[_tableView indexPathsForVisibleRows] objectAtIndex:0] retain]:nil;
}
-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	if (topCell) {
		//[_tableView scrollToRowAtIndexPath:topCell atScrollPosition:UITableViewScrollPositionTop animated:NO];
		RELEASE(topCell);
	}	
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	[self.playlist filterBy:searchText];
	[self updateWithTableReload:NO];
	[_tableView reloadData];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
	[Flurry logEvent:@"SearchBarDismissed" withParameters:[NSDictionary dictionaryWithObjectsAndKeys:APP.book.identifier,@"book",nil]];
	[self.playlist filterBy:nil];
	[self updateWithTableReload:NO];
	[_tableView reloadData];
}
@end
