//
//  AppDelegate_iPhone.m
//  CaseReviews
//
//  Created by Benjie Gillam on 11/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "AppDelegate_iPhone.h"
#import "ContentsViewController.h"

@implementation AppDelegate_iPhone
-(void)openBook:(BBBook *)book fromRect:(CGRect)rect {
	if (self.book) return;
	[super openBook:book fromRect:rect];
	navigationController = [[UINavigationController alloc] initWithRootViewController:ALOAD_VC(ContentsViewController)];
	[APP.shelfViewController presentModalViewController:navigationController animated:YES];
}
-(void)closeBook {
	if (!self.book) return;
	[APP.shelfViewController dismissModalViewControllerAnimated:YES];
	RELEASE(navigationController);
	[super closeBook];
}

- (void)dealloc
{
	RELEASE(navigationController);
	[super dealloc];
}

@end
