//
//  ContentsViewController_iPad.m
//  CaseReviews
//
//  Created by Benjie Gillam on 11/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "ContentsViewController_iPad.h"
#import "AppDelegate.h"
#import "AssessmentViewController.h"
#import "PlaylistViewController.h"

@implementation ContentsViewController_iPad
#pragma mark -
-(void)displayViewController:(UIViewController *)vc animated:(BOOL)animated {
	UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
	[APP splitViewController].detailViewController = nc;
	RELEASE(nc);
}
-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	self.navigationItem.title = @"Contents";
	self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(close)] autorelease];
	int contents_row = [[[NSUserDefaults standardUserDefaults] objectForKey:WRAP_KEY(@"contents_row")] intValue];
	if (contents_row >= [_tableView numberOfRowsInSection:0]) {
		contents_row = [_tableView numberOfRowsInSection:0]-1;
	}
	if (contents_row < 0) {
		contents_row = 0;
	}
	if (contents_row < [_tableView numberOfRowsInSection:0]) {
		NSIndexPath *ip = [NSIndexPath indexPathForRow:contents_row inSection:0];
		[_tableView selectRowAtIndexPath:ip animated:NO scrollPosition:UITableViewScrollPositionTop];
		[self tableView:_tableView didSelectRowAtIndexPath:ip];
	}
	
	UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(productTapped)];
	_productIdentifierImageView.userInteractionEnabled = YES;
	[_productIdentifierImageView addGestureRecognizer:tgr];
	RELEASE(tgr);
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:indexPath.row] forKey:WRAP_KEY(@"contents_row")];
	[[NSUserDefaults standardUserDefaults] synchronize];
	[super tableView:tableView didSelectRowAtIndexPath:indexPath];
}
-(void)productTapped {
	[APP setDefaultHidden:NO animated:YES];
}

@end
