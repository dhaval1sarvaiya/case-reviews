//
//  BBBook.h
//  CaseReviews
//
//  Created by Benjie Gillam on 06/03/2012.
//  Copyright (c) 2012 BrainBakery Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BBBook : NSObject {
	NSString *identifier;
	BOOL retina;
	NSDictionary *infoPlist;
	NSDictionary *allCases;
	NSDictionary *allCasesWithQuestions;
	NSDictionary *searchDict;
}
@property (readonly) NSDictionary *allCases;
@property (readonly) NSDictionary *allCasesWithQuestions;
@property (readonly) NSDictionary *searchDict;
@property (readonly) NSString *identifier;
@property (readonly) NSString *path;
@property (readonly) NSURL *URL;
@property (readonly) NSString *bookName;
@property (readonly) UIImage *icon;
@property (readonly) NSString *shortIdentifier;

+(BBBook *)bookWithIdentifier:(NSString *)_identifier;
-(id)initWithIdentifier:(NSString *)_identifier;
-(void)refresh;
-(UIImage *)imageNamed:(NSString *)name;
-(UIImage *)imageWithContentsOfFile:(NSString *)path;
-(NSString *)pathForFile:(NSString *)file;
-(NSString *)pathForResource:(NSString *)resource ofType:(NSString *)type inDirectory:(NSString *)dir;
-(NSURL *)URLForResource:(NSString *)resource withExtension:(NSString *)ext subdirectory:(NSString *)dir;
-(BOOL)removeFromDevice;
-(NSComparisonResult)compare:(BBBook *)other;
@end
