//
//  AppDelegate_iPad.h
//  CaseReviews
//
//  Created by Benjie Gillam on 11/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface AppDelegate_iPad : AppDelegate <UISplitViewControllerDelegate> {
	UIImageView *defaultIV;
	UIView *defaultIVC;
}

-(void)updateDefaultImageToVertical:(BOOL)isVertical;
@end
