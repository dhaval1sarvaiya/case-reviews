//
//  AppDelegate_iPad.m
//  CaseReviews
//
//  Created by Benjie Gillam on 11/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "AppDelegate_iPad.h"
#import "MGSplitViewController.h"
#import "ContentsViewController.h"

@implementation AppDelegate_iPad
-(void)appOpened {
	defaultIVC = [[UIView alloc] initWithFrame:APP.window.bounds];
	defaultIVC.backgroundColor = [UIColor clearColor];
	CGRect frame = defaultIVC.bounds;
	defaultIV = [[UIImageView alloc] initWithFrame:frame];
	defaultIV.userInteractionEnabled = YES;
	
	UITapGestureRecognizer *tgr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideDefault)];
	[defaultIV addGestureRecognizer:tgr];
	RELEASE(tgr);
	
	defaultIV.backgroundColor = [UIColor blackColor];
	defaultIV.contentMode = UIViewContentModeScaleAspectFill;
	defaultIV.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	defaultIV.hidden = NO;
	defaultIV.image = [UIImage imageNamed:SWF(@"Default-%@~ipad",(UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)?@"Portrait":@"Landscape"))];
	[defaultIVC addSubview:defaultIV];
	[self.window addSubview:defaultIVC];
	[self application:[UIApplication sharedApplication] willChangeStatusBarOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
	[self performSelector:@selector(hideDefault) withObject:nil afterDelay:0.0001];
}
-(void)openBook:(BBBook *)book fromRect:(CGRect)rect {
	if (self.book) return;
	[super openBook:book fromRect:rect];
	[self updateDefaultImageToVertical: UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)];
	
	CGRect oldFrame = defaultIV.frame;
	APP.window.userInteractionEnabled = NO;
	[self setDefaultHidden:NO animated:NO];
	defaultIV.frame = rect;
	defaultIV.backgroundColor = [UIColor clearColor];
	[UIView animateWithDuration:0.5 animations:^{
		defaultIV.frame = oldFrame;
	} completion:^(BOOL finished){
		dispatch_async(dispatch_get_main_queue(), ^{
			defaultIV.backgroundColor = [UIColor blackColor];
			splitViewController = [[MGSplitViewController alloc] init];
			ContentsViewController *vc = ALOAD_VC(ContentsViewController);
			UINavigationController *navVC = [[[UINavigationController alloc] initWithRootViewController:vc] autorelease];
			splitViewController.masterViewController = navVC;
			splitViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
			
			
			splitViewController.delegate = self;
			
			APP.window.rootViewController = splitViewController;
			APP.window.userInteractionEnabled = YES;
			[self setDefaultHidden:YES animated:YES];
		});
	}];
	
	
}
-(void)closeBook {
	if (!self.book) return;
	[self setDefaultHidden:NO animated:YES];
	CGRect oldFrame = defaultIV.frame;
	self.window.userInteractionEnabled = NO;
	double delayInSeconds = 0.8;
	dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
	dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
		self.window.rootViewController = self.shelfNavigationController;
		[self.window addSubview:defaultIVC];
		[UIView animateWithDuration:0.5 animations:^{
			CGRect frame = oldFrame;
			frame.origin.x = frame.size.width/2;
			frame.origin.y = frame.size.height/2;
			frame.size = CGSizeMake(0, 0);
			defaultIV.frame = frame;
		} completion:^(BOOL finished){
			dispatch_async(dispatch_get_main_queue(), ^{
				self.window.userInteractionEnabled = YES;
				defaultIV.frame = oldFrame;
				splitViewController.delegate = nil;
				RELEASE(splitViewController);
				[super closeBook];
				[self updateDefaultImageToVertical: UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation)];
				[self setDefaultHidden:YES animated:NO];
			});
		}];
	});
}

- (void)hideDefault {
	[self setDefaultHidden:YES animated:YES];
}

- (void)dealloc
{
	RELEASE(splitViewController);
	[super dealloc];
}

-(void)setDefaultHidden:(BOOL)hidden animated:(BOOL)animated {
	[self.window addSubview:defaultIVC];
	defaultIVC.hidden = NO;
	defaultIV.hidden = hidden;
	if (animated) {
		[UIView transitionWithView:defaultIV duration:0.5 options:hidden?UIViewAnimationOptionTransitionCurlUp:UIViewAnimationOptionTransitionCurlDown animations:^{} completion:^(BOOL b){
			defaultIVC.hidden = hidden;
		}];	
	} else {
		defaultIVC.hidden = hidden;
	}
}

-(void)updateDefaultImageToVertical:(BOOL)isVertical {
	defaultIV.image = [self imageNamed:SWF(@"Default-%@~ipad.png",(isVertical?@"Portrait":@"Landscape"))];	
}

- (void)splitViewController:(MGSplitViewController*)svc willChangeSplitOrientationToVertical:(BOOL)isVertical {
	[self updateDefaultImageToVertical:isVertical];
}

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
	return NO;
}

-(void)application:(UIApplication *)application willChangeStatusBarOrientation:(UIInterfaceOrientation)newStatusBarOrientation duration:(NSTimeInterval)duration {
	[UIView animateWithDuration:duration animations:^{
		BOOL portrait = UIInterfaceOrientationIsPortrait(newStatusBarOrientation);
		[self updateDefaultImageToVertical:portrait];
		CGFloat big = MAX(APP.window.bounds.size.width,APP.window.bounds.size.height);
		CGFloat small = MIN(APP.window.bounds.size.width,APP.window.bounds.size.height);
		CGFloat angle;
		if (newStatusBarOrientation == UIInterfaceOrientationPortrait) {
			angle = 0;
		} else if (newStatusBarOrientation == UIInterfaceOrientationLandscapeRight) {
			angle = PI/2.0;
		} else if (newStatusBarOrientation == UIInterfaceOrientationLandscapeLeft) {
			angle = -PI/2.0;		
		} else {
			angle = PI;
		}
		defaultIVC.transform = CGAffineTransformMakeRotation(angle);	
		defaultIVC.frame = CGRectMake(0, 0, small,big);
	} completion:NULL];
}
-(void)application:(UIApplication *)application willChangeStatusBarFrame:(CGRect)newStatusBarFrame {
	
}
@end
