//
//  MGSplitViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 19/11/2013.
//  Copyright (c) 2013 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MGSplitViewController : UISplitViewController

@property (nonatomic, weak) UIViewController *masterViewController;
@property (nonatomic, weak) UIViewController *detailViewController;

@end
