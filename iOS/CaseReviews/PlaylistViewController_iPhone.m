//
//  PlaylistViewController_iPhone.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "PlaylistViewController_iPhone.h"
#import "AppDelegate.h"

@implementation PlaylistViewController_iPhone
-(void)viewDidLoad {
	[super viewDidLoad];
	self.navigationItem.title = self.playlist.title;
	//self.navigationItem.rightBarButtonItem = shuffleButton;
	self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:showTitlesButton] autorelease];
}
-(void)displayViewController:(UIViewController *)vc animated:(BOOL)animated {
	UINavigationController *navVC = [[[UINavigationController alloc] initWithRootViewController:vc] autorelease];
	navVC.navigationBar.barStyle = UIBarStyleBlack;
	[APP.navigationController presentModalViewController:navVC animated:YES];
	//[APP.navigationController pushViewController:vc animated:animated];
}
-(void)viewWillAppear:(BOOL)animated {
	[super viewWillAppear:animated];
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:animated];
}

-(void)updateShowTitlesButton {
	[showTitlesButton setImage:[APP imageNamed:titles?@"titlesOn2":@"titlesOff2"] forState:UIControlStateNormal];	
}

@end
