//
//  ImageViewController_iPad.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "ImageViewController_iPad.h"
#import "NotesViewController.h"
#import "AppDelegate.h"

@implementation ImageViewController_iPad
-(void)viewDidLoad {
	[super viewDidLoad];
}
- (void)viewDidUnload {
	RELEASE(navItem);
}
-(IBAction)notesButtonPressed {
	[super notesButtonPressed];
	if (self.popoverController) {
		[self.popoverController dismissPopoverAnimated:YES];
		self.popoverController = nil;
	} else {
		NotesViewController *nvc = ALOAD_VC(NotesViewController);
		nvc.parent = self;
		UINavigationController *navVC = [[[UINavigationController alloc] initWithRootViewController:nvc] autorelease];
		navVC.navigationBar.barStyle = UIBarStyleBlackTranslucent;
		self.popoverController = [[[UIPopoverController alloc] initWithContentViewController:navVC] autorelease];
		self.popoverController.delegate = self;
		[self.popoverController setPopoverContentSize:nvc.view.frame.size animated:NO];
		[self.popoverController presentPopoverFromBarButtonItem:notesButton permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
	}
}
-(void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
	self.popoverController = nil;
}
-(void)refreshNotes {
	[super refreshNotes];
	if (self.popoverController) {
		NotesViewController *nvc = ALOAD_VC(NotesViewController);
		nvc.parent = self;
		self.popoverController.contentViewController = nvc;
	}
}
- (void)dealloc {
	[super dealloc];
}
@end
