//
//  ImageViewController_iPhone.m
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import "ImageViewController_iPhone.h"
#import "NotesViewController.h"
#import "AppDelegate.h"

@implementation ImageViewController_iPhone
-(UIFont *)diagnosisTextViewFont {
	return [UIFont boldSystemFontOfSize:15];
}
-(void)viewDidLoad {
	navItem = [self.navigationItem retain];// MUST come first
	
	[super viewDidLoad];
	legendContainerOriginalHeight = 5;
	CGRect frame = legendContainer.frame;
	frame.size.height = legendContainerOriginalHeight;
	legendContainer.frame = frame;
}
-(void)viewWillAppear:(BOOL)animated {
	[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackOpaque animated:animated];
	[super viewWillAppear:animated];//auto
}
-(void)displayCurrent {
	[super displayCurrent];
	[titleButton setTitle:SWF(@" Case %@",[self.playlist.currentItem objectForKey:@"case"]) forState:UIControlStateNormal];
	
	//Resize titleButton and it's container.
	[titleButton sizeToFit];
	CGRect frame = [titleButton superview].frame;
	frame.size.width = titleButton.frame.size.width;
	[titleButton superview].frame = frame;
}
-(IBAction)notesButtonPressed {
	[super notesButtonPressed];
	NotesViewController *nvc = ALOAD_VC(NotesViewController);
	nvc.parent = self;
	UINavigationController *navVC = [[[UINavigationController alloc] initWithRootViewController:nvc] autorelease];
	navVC.navigationBar.barStyle = UIBarStyleBlackTranslucent;
	[self presentModalViewController:navVC animated:YES];
}

@end
