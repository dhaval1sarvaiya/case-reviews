//
//  ImageViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 12/07/2011.
//  Copyright 2011 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Playlist.h"
#import "Case.h"
#import <CoreText/CoreText.h>
#import <CoreText/CTFont.h>
#import "BGCTLabel.h"

#define kImageViewerShadowOpacity 0.5
#define kImageViewerRightPadInLandscape YES

@interface ImageViewController : UIViewController <UIScrollViewDelegate> {
	IBOutlet UINavigationBar *topBar;
    IBOutlet UIToolbar *bottomBar;
	
	UINavigationItem *navItem;
	
	IBOutlet UIControl *legendContainer;
	IBOutlet UILabel *legendImageNumber;
	IBOutlet UILabel *legendLegendLabel;
	IBOutlet BGCTLabel *legendTextLabel;
	
	IBOutlet UIScrollView *_scrollView;
	
	IBOutlet UIBarButtonItem *prevButton;
	IBOutlet UIBarButtonItem *nextButton;
	IBOutlet UIButton *questionButton;
	IBOutlet UIBarButtonItem *shuffleButton;
	IBOutlet UIBarButtonItem *labelsButton;
	IBOutlet UIBarButtonItem *notesButton;
	IBOutlet UIBarButtonItem *diagnosisButton;
	IBOutlet UIImageView *loopImage;
	IBOutlet UIButton *starButton;
	
	IBOutlet UIControl *diagnosisContainer;
	IBOutlet UITextView *diagnosisTextView;
	
	NSArray *bottomBarAllItems;
	
	Playlist *playlist;
	BOOL legendExpanded;
	CGFloat legendContainerBaseHeight;
	CGFloat legendContainerOriginalHeight;
	
	NSUInteger imageIndex;
	
	NSMutableDictionary *grid;
	
	
	int displayedPosition;
	int displayedImage;
	
	BOOL ignore;
	BOOL manual;
	BOOL rotating;
	
	UIScrollView *zoomScrollView;
	UIView *zoomedImageContainer;
	NSDictionary *zoomedImageDetails;
	UIPopoverController *popoverController;
	
	BOOL openNotesOnOpen;
	
	UIFont *diagnosisTextViewFont;
	
	UIControl *diagnosisDismisser;
	
	UITapGestureRecognizer *diagnosisGR;
	
	
	CGFloat legendTextLabelTopPad;
	CGFloat legendTextLabelBottomPad;
}
@property (nonatomic,retain) Playlist *playlist;
@property (nonatomic,retain) UIPopoverController *popoverController;
@property (retain, nonatomic) IBOutlet UINavigationItem *templateNavItem;
@property BOOL openNotesOnOpen;
-(void)reset;
-(void)displayCurrent;


-(IBAction)close;
-(IBAction)toggleLegend;


-(IBAction)prev;
-(IBAction)next;
-(IBAction)questionButtonPressed;
-(IBAction)starButtonPressed;
-(IBAction)notesButtonPressed;
-(IBAction)diagnosisPressed;


-(void)loadUpDownScrollView:(int)p;
-(void)loadUpDownScrollView:(int)p atIndex:(int)im;
-(void)loadPosition:(int)p image:(int)im;
-(void)unloadScrollViewsLeftOf:(int)l orRightOf:(int)r;
-(void)scrollToCurrent;
-(void)position:(int)p unloadContainersAbove:(int)t orBelow:(int)b;
/*
-(void)animateScrollView:(UIScrollView *)scrollView toContentOffset:(CGPoint)endPoint overPeriod:(NSTimeInterval)duration;
-(void)animateScrollTimer:(NSTimer *)timer;
 */
-(CGRect)rectFromImageView:(UIImageView *)iv;
-(CGRect)absolutePositionFromImageView:(UIImageView *)iv;
-(Case *)currentCase;
-(void)refreshNotes;
-(UIFont *)diagnosisTextViewFont;
-(void)updateLabelsButton;
@end
