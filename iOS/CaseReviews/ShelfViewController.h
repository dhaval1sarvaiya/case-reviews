//
//  ShelfViewController.h
//  CaseReviews
//
//  Created by Benjie Gillam on 05/03/2012.
//  Copyright (c) 2012 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
//@protocol UAStoreFrontDelegate;
#import <MessageUI/MessageUI.h>

@interface ShelfViewController : UIViewController </*UAStoreFrontDelegate,*/ UIScrollViewDelegate, UIAlertViewDelegate, MFMailComposeViewControllerDelegate> {
	IBOutlet UIPageControl *pageControl;
	IBOutlet UIScrollView *scrollView;
	IBOutlet UIProgressView *progressView;
	IBOutlet UIActivityIndicatorView *activityIndicatorView;
	NSMutableArray *installedBooks;
	BOOL editing;
	IBOutlet UIView *aboutView;
	UIView *aboutContainerView;
}
@property (retain, nonatomic) IBOutlet UINavigationItem *navItem;
- (IBAction)editButtonPressed:(id)sender;
- (IBAction)storeButtonPressed:(id)sender;
-(void)renderBooks;
-(void)loadInstalled;
-(IBAction)editButtonPressed:(id)sender;

-(IBAction)openAbout:(id)sender;
-(IBAction)closeAbout:(id)sender;
-(IBAction)emailSupport:(id)sender;
-(IBAction)emailFeedback:(id)sender;
@end
