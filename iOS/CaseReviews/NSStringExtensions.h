//
//  NSStringExtensions.h
//  FitFu
//
//  Created by Benjie Gillam on 12/04/2010.
//  Copyright 2010 Brain Bakery Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>
#import "AppDelegate.h"

@interface NSString (md5)

+ (NSString *)md5:(NSString *)str;
- (NSString *)md5String;
- (NSString *)md5Base64String;

@end


@interface NSString (url)

- (NSString *)urlEncodedString;
- (NSString *)urlDecodedString;
- (NSString *)urlEscapeQueryString;
- (NSString *)htmlentitiesEncodedString;

@end


@interface NSString (Hex2Int)

- (int)hexValue;
- (int)hex2int;

@end


@interface NSData (md5)

- (NSString *)md5Base64String;
- (NSString *)md5String;
- (NSString *)base64Encoding;

@end