//
//  NSStringExtensions.m
//  FitFu
//
//  Created by Benjie Gillam on 12/04/2010.
//  Copyright 2010 Brain Bakery Ltd. All rights reserved.
//

#import "NSStringExtensions.h"

@implementation NSString (md5)
static const char encodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

+ (NSString *)md5:(NSString *)str {
	const char *cStr = [str UTF8String];
	unsigned char result[16];
	CC_MD5( cStr, strlen(cStr), result );
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1], result[2], result[3], 
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];	
}

- (NSString *)md5Base64String {
	const char *cStr = [self UTF8String];
	unsigned char result[16];
	CC_MD5( cStr, strlen(cStr), result );
	NSData *d = [NSData dataWithBytes:result length:16];
	return [d base64Encoding];
}

- (NSString *)md5String {
	return [NSString md5:self];
}

@end



@implementation NSString (url) 

- (NSString *)urlEncodedString {
	return [(NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)self, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8) autorelease];
}

- (NSString *)urlDecodedString {
	return [self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

- (NSString *)urlEscapeQueryString {
	NSMutableString *string = [[NSMutableString alloc] initWithString:self];
	[string replaceOccurrencesOfString:@">" withString:@"%3E" options:NSLiteralSearch range:NSMakeRange(0, [string length])];
	[string replaceOccurrencesOfString:@"<" withString:@"%3C" options:NSLiteralSearch range:NSMakeRange(0, [string length])];
	[string replaceOccurrencesOfString:@"*" withString:@"%2A" options:NSLiteralSearch range:NSMakeRange(0, [string length])];
	[string replaceOccurrencesOfString:@"^" withString:@"%5E" options:NSLiteralSearch range:NSMakeRange(0, [string length])];
	
	NSString *result = [NSString stringWithString:string];
	RELEASE(string);
	return result;	
}

- (NSString *)htmlentitiesEncodedString {
	NSMutableString *string = [[NSMutableString alloc] initWithString:self];
	[string replaceOccurrencesOfString:@"&" withString:@"&amp;" options:NSLiteralSearch range:NSMakeRange(0, [string length])];
	[string replaceOccurrencesOfString:@"\"" withString:@"&quot;" options:NSLiteralSearch range:NSMakeRange(0, [string length])];
	[string replaceOccurrencesOfString:@"'" withString:@"&apos;" options:NSLiteralSearch range:NSMakeRange(0, [string length])];
	[string replaceOccurrencesOfString:@"<" withString:@"&lt;" options:NSLiteralSearch range:NSMakeRange(0, [string length])];
	[string replaceOccurrencesOfString:@">" withString:@"&gt;" options:NSLiteralSearch range:NSMakeRange(0, [string length])];
	
	NSString *result = [NSString stringWithString:string];
	RELEASE(string);
	return result;
}
@end



@implementation NSString (Hex2Int) 

- (int)hexValue {
	NSString *valueToCheck = [self lowercaseString];
	if ([valueToCheck length] != 1) return 0;
	if ([valueToCheck isEqualToString:@"0"]) return 0;
	if ([valueToCheck isEqualToString:@"1"]) return 1;
	if ([valueToCheck isEqualToString:@"2"]) return 2;
	if ([valueToCheck isEqualToString:@"3"]) return 3;
	if ([valueToCheck isEqualToString:@"4"]) return 4;
	if ([valueToCheck isEqualToString:@"5"]) return 5;
	if ([valueToCheck isEqualToString:@"6"]) return 6;
	if ([valueToCheck isEqualToString:@"7"]) return 7;
	if ([valueToCheck isEqualToString:@"8"]) return 8;
	if ([valueToCheck isEqualToString:@"9"]) return 9;
	if ([valueToCheck isEqualToString:@"a"]) return 10;
	if ([valueToCheck isEqualToString:@"b"]) return 11;
	if ([valueToCheck isEqualToString:@"c"]) return 12;
	if ([valueToCheck isEqualToString:@"d"]) return 13;
	if ([valueToCheck isEqualToString:@"e"]) return 14;
	if ([valueToCheck isEqualToString:@"f"]) return 15;
	if ([valueToCheck isEqualToString:@"g"]) return 16;
	if ([valueToCheck isEqualToString:@"h"]) return 17;
	if ([valueToCheck isEqualToString:@"i"]) return 18;
	if ([valueToCheck isEqualToString:@"j"]) return 19;
	if ([valueToCheck isEqualToString:@"k"]) return 20;
	if ([valueToCheck isEqualToString:@"l"]) return 21;
	if ([valueToCheck isEqualToString:@"m"]) return 22;
	if ([valueToCheck isEqualToString:@"n"]) return 23;
	if ([valueToCheck isEqualToString:@"o"]) return 24;
	if ([valueToCheck isEqualToString:@"p"]) return 25;
	if ([valueToCheck isEqualToString:@"q"]) return 26;
	if ([valueToCheck isEqualToString:@"r"]) return 27;
	if ([valueToCheck isEqualToString:@"s"]) return 28;
	if ([valueToCheck isEqualToString:@"t"]) return 29;
	if ([valueToCheck isEqualToString:@"u"]) return 30;
	if ([valueToCheck isEqualToString:@"v"]) return 31;
	if ([valueToCheck isEqualToString:@"w"]) return 32;
	if ([valueToCheck isEqualToString:@"x"]) return 33;
	if ([valueToCheck isEqualToString:@"y"]) return 34;
	if ([valueToCheck isEqualToString:@"z"]) return 35;
	return 0;
}

- (int)hex2int {
	int result = 0;
	for (int i = 0, l=[self length]; i<l; i++) {
		NSString *chr = [self substringWithRange:NSMakeRange(i,1)]; // Look for characterAtIndex: method first
		int value = [chr hexValue];
		result += value * pow(16,[self length]-i-1);
	}
	return result;
}

@end



@implementation NSData (md5)

- (NSString *)md5Base64String {
	unsigned char result[16];
	CC_MD5( [self bytes], [self length], result );
	NSData *d = [NSData dataWithBytes:result length:16];
	return [d base64Encoding];
}

- (NSString *)md5String {
	unsigned char result[16];
	CC_MD5( [self bytes], [self length], result );
	return [NSString stringWithFormat:
			@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
			result[0], result[1], result[2], result[3], 
			result[4], result[5], result[6], result[7],
			result[8], result[9], result[10], result[11],
			result[12], result[13], result[14], result[15]
			];		
}

- (NSString *)base64Encoding {
	if ([self length] == 0)
		return @"";
	
    char *characters = malloc((([self length] + 2) / 3) * 4);
	if (characters == NULL)
		return nil;
	NSUInteger length = 0;
	
	NSUInteger i = 0;
	while (i < [self length])
	{
		char buffer[3] = {0,0,0};
		short bufferLength = 0;
		while (bufferLength < 3 && i < [self length])
			buffer[bufferLength++] = ((char *)[self bytes])[i++];
		
		//  Encode the bytes in the buffer to four characters, including padding "=" characters if necessary.
		characters[length++] = encodingTable[(buffer[0] & 0xFC) >> 2];
		characters[length++] = encodingTable[((buffer[0] & 0x03) << 4) | ((buffer[1] & 0xF0) >> 4)];
		if (bufferLength > 1)
			characters[length++] = encodingTable[((buffer[1] & 0x0F) << 2) | ((buffer[2] & 0xC0) >> 6)];
		else characters[length++] = '=';
		if (bufferLength > 2)
			characters[length++] = encodingTable[buffer[2] & 0x3F];
		else characters[length++] = '=';	
	}
	
	return [[[NSString alloc] initWithBytesNoCopy:characters length:length encoding:NSASCIIStringEncoding freeWhenDone:YES] autorelease];
}

@end