#!/bin/bash

cp -R ~/Dropbox/Antbits-BB/CaseReviews/Common/UIImages .

export GOOGLE_AUTH=$(curl -d accountType=HOSTED -d Email=apiaccess@brainbakery.com -d Passwd=0164087f -d service=wise -d source=BBl-apiaccess-1.0 https://www.google.com/accounts/ClientLogin | head -n 3 | tail -n 1 | sed 's/^[^=]*=//');


cd Books
for I in */; do
echo
echo "#####################################"
echo
echo "Importing $I:"
echo
cd "$I/Import";
sh runme.sh;
cd - >/dev/null;
done;
cd ..
echo 
clear
echo "#####################################"
echo
echo "All done! Command-W to close."
echo
echo
echo
echo
echo
echo
echo
echo
