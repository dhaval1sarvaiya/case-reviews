#!/bin/bash

ECHO="/bin/echo"

$ECHO "Welcome to book creation!";
$ECHO -n "Name of book? ";
read BOOKNAME;
echo "$BOOKNAME" | grep " ";
if [ "$?" != "1" ]; then
	echo "ERROR: Book names cannot contain spaces";
	exit 1;
fi;
$ECHO -n "GDoc key? ";
read GDOC;
$ECHO -n "Cases gid (GDoc)? ";
read CASES_GID;
$ECHO -n "Images gid (GDoc)? ";
read IMAGES_GID;
$ECHO -n "Contents gid (GDoc)? ";
read CONTENTS_GID;

$ECHO 
$ECHO
$ECHO "You've asked to create a book called '$BOOKNAME' with GDoc '$GDOC' and gids $CASES_GID, $IMAGES_GID and $CONTENTS_GID.";
$ECHO -n "Is this correct? [y/N]"
read CORRECT;
if [ "$CORRECT" == "y" ]; then
	$ECHO "Okay... here goes!"
else 
	$ECHO "ABORTED!"
	exit 1;
fi

#exit 0;


mkdir $BOOKNAME;
svn add -N $BOOKNAME;
cp -R ~/Dropbox/Antbits-BB/CaseReviews/Common/Import $BOOKNAME/
sed -i "" "s/^GDOC=.*$/GDOC=\"$GDOC\"/" $BOOKNAME/Import/runme.sh
sed -i "" "s/^CASES_GID=.*$/CASES_GID=$CASES_GID/" $BOOKNAME/Import/runme.sh
sed -i "" "s/^IMAGES_GID=.*$/IMAGES_GID=$IMAGES_GID/" $BOOKNAME/Import/runme.sh
sed -i "" "s/^CONTENTS_GID=.*$/CONTENTS_GID=$CONTENTS_GID/" $BOOKNAME/Import/runme.sh
svn add $BOOKNAME/Import;
cd $BOOKNAME/Import;
./runme.sh;
cd -;
cd $BOOKNAME;
svn add -N Blue Yellow Blue/Plists Blue/Plists/Info.plist;
cd Blue;
svn propset svn:ignore "*" .
cd Plists
svn propset svn:ignore "*" .
cd ../../Yellow;
svn propset svn:ignore "*" .
cd ../..
pwd
