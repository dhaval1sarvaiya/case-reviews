//
//  ProductListViewController.m
//  CaseReviews
//
//  Created by Ashvin Ajadiya on 04/03/15.
//  Copyright (c) 2015 BrainBakery Ltd. All rights reserved.
//

#import "ProductListViewController.h"
#import "IAPHelper.h"
#import "RageIAPHelper.h"
#import "ProductListCell.h"
#import "AppDelegate.h"


#define SUB_PRODUCT_ID @"com.antbits.casereviews.vasc"

@interface ProductListViewController ()
{
}
@property (nonatomic, strong) NSMutableArray *prodcutListArr;

@property (nonatomic, strong) IBOutlet UITableView *tableViewObj;

@end

@implementation ProductListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!self.prodcutListArr)
    {
        self.prodcutListArr = [[NSMutableArray alloc] init];
    }
    
    // Create an instance of EBPurchase.
    demoPurchase = [[EBPurchase alloc] init];
    demoPurchase.delegate = self;
    
    
    isPurchased = NO; // default.
    
    // Request In-App Purchase product info and availability.
    
    if (![demoPurchase requestProduct:SUB_PRODUCT_ID])
    {
        // Returned NO, so notify user that In-App Purchase is Disabled in their Settings.
        
        [buyButton setTitle:@"Purchase Disabled in Settings" forState:UIControlStateNormal];
    }
    
    [self getProductList];
    [self.tableViewObj reloadData];
    
    self.title = @"Content";
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(done:)];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"Restore" style:UIBarButtonItemStylePlain target:self action:@selector(restore)];
    self.navigationItem.rightBarButtonItem = rightItem;

    
    // Do any additional setup after loading the view from its nib.
}

- (IBAction)done:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)restore
{
    
}

- (void)getProductList
{
    NSString *str = @"http://s3.amazonaws.com/CaseReviewsManifest/index.json";
    NSURL *url = [NSURL URLWithString:str];
    NSData *data = [NSURLConnection sendSynchronousRequest:[NSURLRequest requestWithURL:url] returningResponse:nil error:nil];
    self.prodcutListArr =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    [self.prodcutListArr removeObjectAtIndex:0];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.prodcutListArr.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"ProductListCell";
    
    ProductListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil){

        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ProductListCell" owner:nil options:nil];
        
        for(id currentObject in topLevelObjects)
        {
            if([currentObject isKindOfClass:[ProductListCell class]])
            {
                cell = (ProductListCell *)currentObject;
                break;
            }
        }
    }
    
    NSDictionary *dict = [[NSDictionary alloc] init];
    dict = [self.prodcutListArr objectAtIndex:indexPath.row];
    [cell setEntry:dict];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//    [self buy];
    [self purchaseProduct];
}

-(void)buy
{
    NSSet * productIdentifiers = [NSSet setWithObjects:
                                  @"com.antbits.casereviews.vasc",
                                  nil];
    IAPHelper *skpayment = [[IAPHelper alloc] initWithProductIdentifiers:productIdentifiers];
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:2.0]];
    [skpayment requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (products.count>0)
        {
            SKProduct *skP = products[0];
            [skpayment buyProduct:skP];
        }
    }];

}

-(IBAction)purchaseProduct
{
    // First, ensure that the SKProduct that was requested by
    // the EBPurchase requestProduct method in the viewWillAppear
    // event is valid before trying to purchase it.
    
    if (demoPurchase.validProduct != nil)
    {
        // Then, call the purchase method.
        
        if (![demoPurchase purchaseProduct:demoPurchase.validProduct])
        {
            // Returned NO, so notify user that In-App Purchase is Disabled in their Settings.
            UIAlertView *settingsAlert = [[UIAlertView alloc] initWithTitle:@"Allow Purchases" message:@"You must first enable In-App Purchase in your iOS Settings before making this purchase." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [settingsAlert show];
            [settingsAlert release];
        }
    }
}

-(IBAction)restorePurchase
{
    // Restore a customer's previous non-consumable or subscription In-App Purchase.
    // Required if a user reinstalled app on same device or another device.
    
    // Call restore method.
    if (![demoPurchase restorePurchase])
    {
        // Returned NO, so notify user that In-App Purchase is Disabled in their Settings.
        UIAlertView *settingsAlert = [[UIAlertView alloc] initWithTitle:@"Allow Purchases" message:@"You must first enable In-App Purchase in your iOS Settings before restoring a previous purchase." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [settingsAlert show];
        [settingsAlert release];
    }
}


#pragma mark -
#pragma mark EBPurchaseDelegate Methods

-(void) requestedProduct:(EBPurchase*)ebp identifier:(NSString*)productId name:(NSString*)productName price:(NSString*)productPrice description:(NSString*)productDescription
{
    NSLog(@"ViewController requestedProduct");
    
    if (productPrice != nil)
    {
        // Product is available, so update button title with price.
        
        [buyButton setTitle:[@"Buy Game Levels Pack " stringByAppendingString:productPrice] forState:UIControlStateNormal];
        buyButton.enabled = YES; // Enable buy button.
        
    } else {
        // Product is NOT available in the App Store, so notify user.
        
        buyButton.enabled = NO; // Ensure buy button stays disabled.
        [buyButton setTitle:@"Buy Game Levels Pack" forState:UIControlStateNormal];
        
        UIAlertView *unavailAlert = [[UIAlertView alloc] initWithTitle:@"Not Available" message:@"This In-App Purchase item is not available in the App Store at this time. Please try again later." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [unavailAlert show];
        [unavailAlert release];
    }
}

-(void) successfulPurchase:(EBPurchase*)ebp restored:(bool)isRestore identifier:(NSString*)productId receipt:(NSData*)transactionReceipt
{
    NSLog(@"ViewController successfulPurchase");
    
    // Purchase or Restore request was successful, so...
    // 1 - Unlock the purchased content for your new customer!
    // 2 - Notify the user that the transaction was successful.
    
    if (!isPurchased)
    {
        // If paid status has not yet changed, then do so now. Checking
        // isPurchased boolean ensures user is only shown Thank You message
        // once even if multiple transaction receipts are successfully
        // processed (such as past subscription renewals).
        
        isPurchased = YES;
        
        //-------------------------------------
        
        // 1 - Unlock the purchased content and update the app's stored settings.
        
        //-------------------------------------
        
        // 2 - Notify the user that the transaction was successful.
        
        NSString *alertMessage;
        
        if (isRestore) {
            // This was a Restore request.
            alertMessage = @"Your purchase was restored and the Game Levels Pack is now unlocked for your enjoyment!";
            
        } else {
            // This was a Purchase request.
            alertMessage = @"Your purchase was successful and the Game Levels Pack is now unlocked for your enjoyment!";
        }
        
        UIAlertView *updatedAlert = [[UIAlertView alloc] initWithTitle:@"Thank You!" message:alertMessage delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [updatedAlert show];
        [updatedAlert release];
    }
}

-(void) failedPurchase:(EBPurchase*)ebp error:(NSInteger)errorCode message:(NSString*)errorMessage
{
    NSLog(@"ViewController failedPurchase");
    
    // Purchase or Restore request failed or was cancelled, so notify the user.
    
    UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:@"Purchase Stopped" message:@"Either you cancelled the request or Apple reported a transaction error. Please try again later, or contact the app's customer support for assistance." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [failedAlert show];
    [failedAlert release];
}

-(void) incompleteRestore:(EBPurchase*)ebp
{
    NSLog(@"ViewController incompleteRestore");
    
    // Restore queue did not include any transactions, so either the user has not yet made a purchase
    // or the user's prior purchase is unavailable, so notify user to make a purchase within the app.
    // If the user previously purchased the item, they will NOT be re-charged again, but it should
    // restore their purchase.
    
    UIAlertView *restoreAlert = [[UIAlertView alloc] initWithTitle:@"Restore Issue" message:@"A prior purchase transaction could not be found. To restore the purchased product, tap the Buy button. Paid customers will NOT be charged again, but the purchase will be restored." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [restoreAlert show];
    [restoreAlert release];
}

-(void) failedRestore:(EBPurchase*)ebp error:(NSInteger)errorCode message:(NSString*)errorMessage
{
    NSLog(@"ViewController failedRestore");
    
    // Restore request failed or was cancelled, so notify the user.
    
    UIAlertView *failedAlert = [[UIAlertView alloc] initWithTitle:@"Restore Stopped" message:@"Either you cancelled the request or your prior purchase could not be restored. Please try again later, or contact the app's customer support for assistance." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [failedAlert show];
    [failedAlert release];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
