//
//  ProductListViewController.h
//  CaseReviews
//
//  Created by Ashvin Ajadiya on 04/03/15.
//  Copyright (c) 2015 BrainBakery Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

// Import the EBPurchase class.

#import "EBPurchase.h"

@interface ProductListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, EBPurchaseDelegate> {
    
    // Assign this class as a delegate of EBPurchaseDelegate.
    // Then add an EBPurchase property.
    
    EBPurchase* demoPurchase;
    
    IBOutlet UIButton *buyButton;
    
    BOOL isPurchased;
}

-(IBAction)purchaseProduct;

-(IBAction)restorePurchase;


@end
