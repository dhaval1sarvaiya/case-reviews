/*
 * Copyright 2010-2013 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

#import "Request.h"

@implementation Request

-(NSString *)buildRequestUrl
{
    return nil;
}

- (void)startWithCompletionBlock:(void (^)(NSError *error, int statusCode, NSData *result))done {
	completion = Block_copy(done);
	NSURL *url = [[[NSURL alloc] initWithString:[self buildRequestUrl]] autorelease];
	NSLog(@"URL: %@", url);
	
	NSData *postData = [[url absoluteString] dataUsingEncoding:NSUTF8StringEncoding];
	NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
	[theRequest setHTTPMethod:@"POST"];
	[theRequest setValue:[NSString stringWithFormat:@"%d", postData.length] forHTTPHeaderField:@"Content-Length"];
	[theRequest setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
	[theRequest setHTTPBody:postData];
  
	NSURLConnection *conn = [NSURLConnection connectionWithRequest:theRequest delegate:self];
	responseData = [[NSMutableData alloc] init];
	[conn start];
	
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	responseCode = ((NSHTTPURLResponse *)response).statusCode;
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	[responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	completion(error, 0, nil);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	completion(nil, responseCode, responseData);
}

- (void)dealloc {
	RELEASE(responseData);
	Block_release(completion);
	[super dealloc];
}

@end

