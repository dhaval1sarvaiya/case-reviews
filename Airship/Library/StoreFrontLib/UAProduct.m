/*
Copyright 2009-2012 Urban Airship Inc. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.

2. Redistributions in binaryform must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided withthe distribution.

THIS SOFTWARE IS PROVIDED BY THE URBAN AIRSHIP INC ``AS IS'' AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL URBAN AIRSHIP INC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#import "UAProduct.h"
#import "UAirship.h"
#import "UAStoreFront.h"
#import "UAStoreFrontDownloadManager.h"
#import "UADownloadContent.h"

#import "Crypto.h"
#import "RegisterDeviceRequest.h"
#import "JSONUtilities.h"

@implementation UAProduct

@synthesize productIdentifier;
@synthesize previewURL;
@synthesize preview;
@synthesize iconURL;
@synthesize icon;
@synthesize objectName;
@synthesize revision;
@synthesize price;
@synthesize priceNumber;
@synthesize productDescription;
@synthesize skProduct;
@synthesize title;
@synthesize receipt;
@synthesize isFree;
@synthesize fileSize;
@synthesize status;
@synthesize progress;
@synthesize transaction;

@synthesize downloadTmpPath, downloadPath, downloadManagerDelegate;

#pragma mark -

- (void)dealloc {
    RELEASE_SAFELY(skProduct);
    RELEASE_SAFELY(productIdentifier);
    RELEASE_SAFELY(previewURL);
    RELEASE_SAFELY(preview);
    RELEASE_SAFELY(iconURL);
    RELEASE_SAFELY(icon);
    RELEASE_SAFELY(objectName);
    RELEASE_SAFELY(price);
    RELEASE_SAFELY(priceNumber);
    RELEASE_SAFELY(productDescription);
    RELEASE_SAFELY(title);
    RELEASE_SAFELY(receipt);
	RELEASE_SAFELY(downloadManagerDelegate);
	RELEASE_SAFELY(downloadTmpPath);
	RELEASE_SAFELY(downloadPath);
    transaction = nil;
    [super dealloc];
}

- (id)init {
    if (!(self = [super init]))
        return nil;

    self.priceNumber = [NSDecimalNumber zero];
    self.receipt = @"";
    self.status = UAProductStatusUnpurchased;
	[AmazonErrorHandler shouldNotThrowExceptions];
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    UAProduct *copy = [[[self class] allocWithZone:zone] init];
    copy.skProduct = self.skProduct;
    copy.productIdentifier = self.productIdentifier;
    copy.previewURL = self.previewURL;
    copy.preview = self.preview;
    copy.iconURL = self.iconURL;
    copy.icon = self.icon;
    copy.objectName = self.objectName;
    copy.revision = self.revision;
    copy.fileSize = self.fileSize;
    copy.price = self.price;
    copy.priceNumber = self.priceNumber;
    copy.productDescription = self.productDescription;
    copy.title = self.title;
    copy.receipt = self.receipt;
    copy.isFree = self.isFree;
    copy.status = self.status;

    return copy;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"[product id:%@, status:%d]", productIdentifier, status];
}

- (NSURL *)downloadURL {
	return [NSURL URLWithString:SWF(@"http://s3.amazonaws.com/%@/%@",S3_BUCKET,self.objectName)];
}

- (NSComparisonResult)compare:(UAProduct *)product {
    return [self.title compare:product.title];
}

- (BOOL)isEqual:(id)anObject {
    if (![anObject isKindOfClass:[UAProduct class]] || !anObject)
        return NO;

    UAProduct *other = (UAProduct *)anObject;
    return ([self.productIdentifier isEqualToString:other.productIdentifier]
            && self.status == other.status
            && [self.objectName isEqual:other.objectName]
            && [self.title isEqualToString:other.title]);
}

- (NSString *)receipt {
    return [[receipt copy] autorelease];
}

+ (UAProduct *)productFromDictionary:(NSDictionary *)item {

    UAProduct* product = [[UAProduct alloc] init];
    product.productIdentifier = [item objectForKey: @"product_id"];

    NSString* previewURL =  [item objectForKey: @"preview_url"]==nil ? @"" : [item objectForKey: @"preview_url"];
    if (previewURL != nil && ![previewURL isEqualToString:@""]) {
        product.previewURL = [NSURL URLWithString: previewURL];
    }

    product.objectName = [item objectForKey:@"object_name"];

    NSString* iconURL = [item objectForKey: @"icon_url"]== nil ? @"" : [item objectForKey: @"icon_url"];
    product.iconURL = [NSURL URLWithString: iconURL];

    product.title = [item objectForKey: @"name"];
    product.isFree = NO;
    if([item objectForKey: @"free"] != [NSNull null] && [[item objectForKey: @"free"] intValue] != 0) {
        product.isFree = YES;
        product.productDescription = [item objectForKey: @"description"];
        product.price = @"FREE";
    }

    product.revision = [[item objectForKey: @"current_revision"] intValue];
    product.fileSize = [[item objectForKey:@"file_size"] doubleValue];

    [product resetStatus];

    return [product autorelease];
}

#pragma mark -
#pragma mark Status Methods

- (void)notifyInventoryObservers:(UAProductStatus)aStatus {
    [[UAStoreFront shared].inventory notifyObservers:@selector(inventoryProductsChanged:)
                                          withObject:[NSNumber numberWithInt:status]];
}

- (void)setStatus:(UAProductStatus)aStatus {
    if (aStatus != status) {
        status = aStatus;
        [self notifyObservers:@selector(productStatusChanged:)
                   withObject:[NSNumber numberWithInt:status]];
        [self notifyInventoryObservers:aStatus];
    }
}

- (void)resetStatus {
    // Check permament status
    NSDictionary *item = [[UAStoreFront shared].purchaseReceipts objectForKey:productIdentifier];
    if (item != nil) {
        receipt = [[item objectForKey:@"receipt"] copy];
        if(revision > [[item objectForKey:@"revision"] intValue]) {
            self.status = UAProductStatusHasUpdate;
        } else {
            if ([[UAStoreFront shared].downloadManager hasPendingProduct:self]) {
                self.status = UAProductStatusPurchased;
            } else if ([[UAStoreFront shared].downloadManager hasDecompressingProduct:self]) {
                self.status = UAProductStatusDecompressing;
            } else {
                self.status = UAProductStatusInstalled;
            }
        }
    } else {
        self.status = UAProductStatusUnpurchased;
        receipt = @"";
    }
}

- (void)setProgress:(float)_progress {
    if (progress != _progress) {
        progress = _progress;
        [self notifyObservers:@selector(productProgressChanged:)
                   withObject:[NSNumber numberWithFloat:progress]];
    }
}

- (BOOL)hasUpdate {
    NSDictionary* item = [[UAStoreFront shared].purchaseReceipts objectForKey:productIdentifier];
    if (item != nil && revision > [[item objectForKey:@"revision"] intValue])
        return YES;
    else
        return NO;
}


- (NSString *)downloadPath {
    if (downloadPath == nil) {
        if ([self downloadFileName] == nil) {
            UALOG(@"download path is nil");
            return nil;
        }
        self.downloadPath = [NSTemporaryDirectory() stringByAppendingPathComponent:
                         [NSString stringWithFormat: @"%@.zip", [self downloadFileName]]];
        
        UALOG(@"download path: %@", downloadPath);
    }

    return downloadPath;
}

- (NSString *)downloadTmpPath {
    if (downloadTmpPath == nil) {
        if ([self downloadFileName] == nil) {
            UALOG(@"temp download path is nil");
            return nil;
        }
        self.downloadTmpPath = [NSTemporaryDirectory() stringByAppendingPathComponent:
                            [NSString stringWithFormat: @"%@_tmp.zip", [self downloadFileName]]];
        
        UALOG(@"temp download path: %@", downloadTmpPath);
    }
    return downloadTmpPath;
}

- (NSString *)downloadFileName {
	return self.productIdentifier;
}

- (void)initiateS3Client {
	if (getTokenRequest) return;
    NSString         *uid = [AmazonKeyChainWrapper getUidForDevice];
    NSString         *key = [AmazonKeyChainWrapper getKeyForDevice];
   
    NSData *data = self.transaction.transactionReceipt;
    getTokenRequest = [[GetTokenRequest alloc] initWithEndpoint:TVM_ENDPOINT andUid:uid andKey:key usingSSL:USE_SSL password:nil bucketName:S3_BUCKET tvmSecret:TVMSECRET  receiptData:data];
	[getTokenRequest startWithCompletionBlock:^(NSError *error, int statusCode, NSData *result) {
		[getTokenRequest autorelease];
		getTokenRequest = nil;
		if (statusCode == 200) {
			NSData   *body = [Crypto decrypt:[[[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding] autorelease] key:[AmazonKeyChainWrapper getKeyForDevice]];
			
			NSString *json = [[NSString alloc] initWithData:body encoding:NSUTF8StringEncoding];
			
			NSString *accessKey      = [JSONUtilities getJSONElement:json element:@"accessKey"];
			NSString *secretKey      = [JSONUtilities getJSONElement:json element:@"secretKey"];
			NSString *securityToken  = [JSONUtilities getJSONElement:json element:@"securityToken"];
			NSString *expirationDate = [JSONUtilities getJSONElement:json element:@"expirationDate"];
			
			[json release];

			[AmazonKeyChainWrapper storeCredentialsInKeyChain:accessKey secretKey:secretKey securityToken:securityToken expiration:expirationDate];
			AmazonCredentials *credentials = [[[AmazonCredentials alloc] initWithAccessKey:accessKey withSecretKey:secretKey] autorelease];
			credentials.securityToken = securityToken;

			[s3 release];
			s3  = [[AmazonS3Client alloc] initWithCredentials:credentials];
			s3.endpoint = [AmazonEndpoints s3Endpoint:US_EAST_1];
			s3.timeout=2400;
			
			[AmazonLogger verboseLogging];
			
			S3GetPreSignedURLRequest *urlReq = [[S3GetPreSignedURLRequest alloc] init];
			urlReq.bucket = S3_BUCKET;
			urlReq.key = self.objectName;
			urlReq.expires = [NSDate dateWithTimeIntervalSinceNow:2*60*60];
			dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
			dispatch_async(queue, ^{
				NSError *error = nil;
				NSURL *url = [s3 getPreSignedURL:urlReq error:&error];
				dispatch_async(dispatch_get_main_queue(), ^{
					if (error) {
						NSLog(@"**********************************didFailWithServiceException : %@", error);
//						[self.downloadManagerDelegate requestDidFail:content];
//						[content release];
//						content = nil;
						preSignedURLCallback(nil);
					} else {
						preSignedURLCallback(url);
					}
					Block_release(preSignedURLCallback);
					preSignedURLCallback = nil;
				});
			});
			
			return;
		}
		// Fallback if not successful
		NSLog(@"Failed to fetch S3 credentials with status code: %d",statusCode);
		preSignedURLCallback(nil);
		Block_release(preSignedURLCallback);
		preSignedURLCallback = nil;
//		[self.downloadManagerDelegate requestDidFail:content];
//		[content release];
//		content = nil;
	}];
}
    
-(void)deleteFile{
    NSError *error=nil;
    if ([[NSFileManager defaultManager] isDeletableFileAtPath:[self downloadTmpPath]]) {
        BOOL success = [[NSFileManager defaultManager] removeItemAtPath:[self downloadTmpPath] error:&error];
        if (!success) {
            NSLog(@"Error removing file at path: %@", error.localizedDescription);
        }
    }
}

- (NSString *)md5StringFromData:(NSData *)data
{
    void *cData = malloc([data length]);
    unsigned char resultCString[16];
    [data getBytes:cData length:[data length]];
    
    CC_MD5(cData, [data length], resultCString);
    free(cData);
    
    NSString *result = [NSString stringWithFormat:
                        @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
                        resultCString[0], resultCString[1], resultCString[2], resultCString[3],
                        resultCString[4], resultCString[5], resultCString[6], resultCString[7],
                        resultCString[8], resultCString[9], resultCString[10], resultCString[11],
                        resultCString[12], resultCString[13], resultCString[14], resultCString[15]
                        ];
    return result;
}

- (void)getPreSignedURL:(void (^)(NSURL *url))callback {
	preSignedURLCallback = Block_copy(callback);
	if ([AmazonKeyChainWrapper getUidForDevice]) {
		[self initiateS3Client];
	} else {
		[self anonymousRegister];
	}

}

- (void)anonymousRegister {
	if (registerDeviceRequest) return;
	NSString              *uid = [Crypto generateRandomString];
	NSString              *key = [Crypto generateRandomString];
	
	registerDeviceRequest = [[RegisterDeviceRequest alloc] initWithEndpoint:TVM_ENDPOINT andUid:uid andKey:key usingSSL:USE_SSL];
	[registerDeviceRequest startWithCompletionBlock:^(NSError *error, int statusCode, NSData *result) {
		[registerDeviceRequest autorelease];
		registerDeviceRequest = nil;
		if (statusCode == 200) {
			[AmazonKeyChainWrapper registerDeviceId:uid andKey:key];
			[self initiateS3Client];
		} else {
			AMZLogDebug(@"Token Vending Machine responded with Code: [%d] and Messgae: [%@]", statusCode, [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding]);
			preSignedURLCallback(nil);
			Block_release(preSignedURLCallback);
			preSignedURLCallback = nil;
//			[self.downloadManagerDelegate requestDidFail:content];
//			[content release];
//			content = nil;
		}
	}];
}

@end
