/*
 Copyright 2009-2012 Urban Airship Inc. All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:

 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.

 2. Redistributions in binaryform must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided withthe distribution.

 THIS SOFTWARE IS PROVIDED BY THE URBAN AIRSHIP INC ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL URBAN AIRSHIP INC OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "UAStoreFrontUI.h"
#import "UAStoreFrontViewController.h"
#import "UAStoreFrontSplitViewController.h"


@implementation UAStoreFrontUI

SINGLETON_IMPLEMENTATION(UAStoreFrontUI)

@synthesize rootViewController;
@synthesize productListViewController;
@synthesize isiPad;
@synthesize uaWindow;
@synthesize originalViewController;
@synthesize isVisible;
@synthesize localizationBundle;

static BOOL runiPhoneTargetOniPad = NO;

+ (void)setRuniPhoneTargetOniPad:(BOOL)value {
    runiPhoneTargetOniPad = value;
}

-(void)dealloc {    
    RELEASE_SAFELY(rootViewController);
    RELEASE_SAFELY(productListViewController);
    RELEASE_SAFELY(uaWindow);
    RELEASE_SAFELY(originalViewController);
    RELEASE_SAFELY(alertHandler);
    RELEASE_SAFELY(localizationBundle);

    [super dealloc];
}

-(id)init {
    UALOG(@"Initialize UAStoreFrontUI.");

    if (self = [super init]) {
        NSString* path = [[[NSBundle mainBundle] resourcePath]
                          stringByAppendingPathComponent:@"UAStoreFrontLocalization.bundle"];

        self.localizationBundle = [NSBundle bundleWithPath:path];

        if ((UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) && !runiPhoneTargetOniPad) {
            isiPad = YES;
            UAStoreFrontSplitViewController *svc = [[UAStoreFrontSplitViewController alloc] init];
            productListViewController = [[svc productListViewController] retain];
            rootViewController = (UIViewController *)svc;
        
        } else {
            productListViewController = [[UAStoreFrontViewController alloc]
                                         initWithNibName:@"UAStoreFront" bundle:nil];
            
            rootViewController = [[UINavigationController alloc] initWithRootViewController:productListViewController];
        }
        
        alertHandler = [[UAStoreFrontAlertHandler alloc] init];
        isVisible = NO;
    }
    
    return self;
}

+ (void)displayStoreFront:(UIViewController *)viewController animated:(BOOL)animated {
    UAStoreFrontUI* ui = [UAStoreFrontUI shared];
    ui->animated = animated;

    // reset the view navigation to the product list
    [ui.productListViewController.navigationController popToRootViewControllerAnimated:NO];

    if (!ui.isiPad) {
		if ([viewController respondsToSelector:@selector(presentViewController:animated:completion:)]) {
			[viewController presentViewController:ui.rootViewController animated:animated completion:^{}];
		} else {
			[viewController presentModalViewController:ui.rootViewController animated:animated];
		}
    } else {
		ui.originalViewController = [UIApplication sharedApplication].delegate.window.rootViewController;
		UIViewAnimationOptions options = UIViewAnimationOptionTransitionFlipFromRight;
		if (UIInterfaceOrientationIsLandscape(ui.originalViewController.interfaceOrientation)) {
			options = UIViewAnimationOptionTransitionFlipFromBottom;
		}
		options |= UIViewAnimationOptionLayoutSubviews;
		
		UIWindow *window = [UIApplication sharedApplication].delegate.window;
		window.rootViewController = ui.rootViewController;
		window.rootViewController = ui.originalViewController;
		
		[UIView transitionWithView:window duration:0.5 options:options animations:^{
			[UIApplication sharedApplication].delegate.window.rootViewController = ui.rootViewController;
		} completion:^(BOOL finished){
		}];
    }
    ui->isVisible = YES;
}

+ (void)displayStoreFront:(UIViewController *)viewController withProductID:(NSString *)productID animated:(BOOL)animated {
    UAStoreFrontUI* ui = [UAStoreFrontUI shared];
    [UAStoreFrontUI displayStoreFront:viewController animated:animated];
    [ui.productListViewController loadProduct:productID];
}

+ (void)quitStoreFront {
    UAStoreFrontUI* ui = [UAStoreFrontUI shared];

    if (ui.isiPad) {
        // if iPad
    }

    UIViewController *presentingViewController = nil;
    if ([ui.rootViewController respondsToSelector:@selector(presentingViewController)]) {
        presentingViewController = [ui.rootViewController presentingViewController]; //iOS5 method
    } else {
        presentingViewController = ui.rootViewController.parentViewController;// <= 4.x
    }
    
    if (presentingViewController != nil) {
        // for iPhone/iPod displayStoreFront:animated:
		if ([ui.rootViewController respondsToSelector:@selector(dismissViewControllerAnimated:completion:)]) {
			[ui.rootViewController dismissViewControllerAnimated:ui->animated completion:^{}];
		} else {
			[ui.rootViewController dismissModalViewControllerAnimated:ui->animated];
		}
    } else if (ui.originalViewController) {
		UIViewAnimationOptions *options = UIViewAnimationOptionTransitionFlipFromLeft;
		if (UIInterfaceOrientationIsLandscape(ui.rootViewController.interfaceOrientation)) {
			options = UIViewAnimationOptionTransitionFlipFromTop;
		}
		UIWindow *window = [UIApplication sharedApplication].delegate.window;
		window.rootViewController = ui.originalViewController;
		window.rootViewController = ui.rootViewController;
		
		[UIView transitionWithView:window duration:0.5 options:options animations:^{
			[UIApplication sharedApplication].delegate.window.rootViewController = ui.originalViewController;
			ui.originalViewController = nil;
		} completion:^(BOOL finished){
		}];
    } else {
        // For other circumstances. e.g custom showing rootViewController or changed the showing code of StoreFront
        UALOG(@"UAStoreFrontUI rootViewController appears to be customized. Add your own quit logic here");
    }

    ui->isVisible = NO;
}

+ (id<UAStoreFrontAlertProtocol>)getAlertHandler {
    UAStoreFrontUI* ui = [UAStoreFrontUI shared];
    
    return ui->alertHandler;
}

@end
